---
created: 2018-03-24T21:29:38+01:00
modified: 2024-08-28T07:23:37+02:00
---

# Escalade

---

## 2024-08-18 Valcroissant (Die)

Avec Jean-Mathieu MONTMAGNON, Paul CHRISTOPHLE, Mélanie GROS, Théo LAMBERT, Christophe HARLEZ, Antoine TAUPIN et Camille BELIN.

### Secteur du _Pilier_ (ou de l'éperon)

_Suivez le guide_ L1 (5c)
: En tête. Jusqu'à un relais intermédiaire non-chaîné, 20m.

_Suivez le guide_ L1' (5c)
: En moulinette. Jusqu'au relais de couenne, 20m.

_Suivez le guide_ L2 (5c)
: En tête à vue. Belle longeur sur du rocher magnifique, courte. Relais sur 2 plaquettes.

_Suivez le guide_ L3 (5a)
: En moulinette. Un pas au début, puis suivre l'arête couchée. Relais au pied d'un ressaut (1 piton + 1 plaquette).

_Suivez le guide_ L4 (5b)
: En tête à vue. Remonter le pilier (peu de points) puis marche jusqu'au dernier ressaut (pas de relais)

_Suivez le guide_ L5 (5b)
: En moulinette. Attaquer le ressaut légèrement côté ouest puis revenir dans une dalle à l'est. Aller jusqu'au dernier relais, celui duquel part une corde fixe pour rejoindre le relais de rappel.

### Secteur _La dalle_

_La Fesse à piton_ (6a+)
: En tête à vue. Pas évidente, surtout avec une poulie ^^

_Lune de miel_ (5c)
: En tête à vue. Facile.

---

## 2024-08-08 La Taupinière (Tamée)

Avec Christophe HARLEZ.

### Secteur _Machu Picchu_

_El Camino de los Incas_ (5a)
: En tête à vue. Facile.

_Creps salée_ (5b+)
: En tête à vue. Un pas un peu surprenant au milieu.

_Bat guiguette_ (5c+)
: En tête à vue. Facile.

_Terra incognita_ (5b+)
: En tête à vue.

_Lierre de rien_ (5c)
: En tête à vue. Début difficile.

_Mousse Attack_ (6a+)
: En tête, deux pauses. Une cheminée, puis une sortie pas évidente par la gauche.

### Secteur _Cirque oublié_

_Larg pa le trou_ (6a)
: En tête.

---

## 2024-07-17 La Payre (Le Pouzin)

Avec Mélanie GROS, dans l'après-midi.

_Échappées belles_ (5c ?)
: En tête à vue. Dièdre facile, équipé sur plaquettes à gauche deux _Les dessous chic' d'Isab_.

_Sveltitude boudinesque_ (5c+)
: En tête, A0. Magnifique fissure bien psycho !

_Concerto pour les gnolus_ (6a)
: En tête, A0, puis travail en moulinette. Avec le dièdre.

_Subtile_ (6b+ ?)
: Travail en moulinette. Tout est dans le nom ! Peut-être même 6c...

_Hé, passe-moi le pâté !_ (6b)
: En moulinette, enchaînée. Crux tout en haut.

---

## 2024-07-13 Pas de l'escalier (Tamée)

Avec Manon GABARRET, dans l'après-midi.

### Secteur Sous la route

_Howare Yourcelf_ (5c)
: En tête à vue.

_Ma Flanie Bob_ (6a+)
: En tête à vue. Morpho.

_Déviation_ (6a)
: En tête à vue. Un peu encombrée de végétation. Sortie peu évidente.

---

## 2024-07-11 Rocher Baron (Saint-Martin-de-Queyrières)

Avec Jean-Philippe LEBRAT.

Grande voie _Joli coup Baron_ (AD 5a>4c P1), en réversible.
Belle grimpe très facile sur quartzite !

---

## 2024-07-03 Gorges de la Combe d'Oyans (Rochefort-Samson)

Avec Nordine ARAR, Fatima ATIGUI, Anne-Sophie PETIT, Mélina ÉCHIVARD, Romain AVISSE, Gabriel ULLIEL, Valentin DEPREZ, Quentin MARCHAND et Sophie, en soirée.

### Secteur Rantanplan

_Version découverte_ (5a)
: En tête. Courte.

_Le pilier des nains_ (5b)
: En tête à vue. Intéressante mais courte.

_La corde au cul_ (5b)
: En tête à vue. Courte et peu intéressante, dure dans la cotation.

_Pas de géant_ (5a)
: En tête à vue. Courte, intéressante.

---

## 2024-05-12 La Graville (Saou)

Avec Manon GABARRET.

### Secteur Nord

_L'Y_ (4c)
: En tête à vue. Voie très intéressante, typée "montagne". Dure dans la cotation.

_La Vèbre_ (5b)
: En tête.

_Directe de la Vèbre_ (5c)
: En tête.

_Monalisa_ (5c)
: En tête.

_Walkyrie_ (5b)
: En tête.

_Muguet_ (5b)
: En tête à vue. C'est très cher côté !

_Crispinette_ (5b)
: En tête.

_La fissure_ (5c)
: En tête. Un peu engagée.

---

## 2024-04-14 Falaise d'Anse (Omblèze)

Avec Paul CHRISTOPHLE.

### Secteur _Dernier empereur_

_La Georgette_ L1 (5c P1)
: En moulinette. Premier point un peu haut dans une cheminée, le crux est juste après. Puis sortir par la gauche, relais au-dessus du relais chaîné de couenne.

_La Georgette_ L2 (6a+ P1+)
: En moulinette, A0. Fissure/cheminée, avec un pas délicat à mi-longueur.

_La Georgette_ L3 (6a+ P1+)
: En moulinette, A0. Dalle à gouttes d'eau, puis fissure végétale pénible.

_Honorable Li Khen_ (6a+)
: En moulinette. Premier point très haut, le crux est juste après.

---

## 2024-04-12 Aiguille de la Tour (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Romain AVISSE et Alexandre CHARRA.

Grande voie _La Soupe aux cailloux_.

_La Soupe aux cailloux_ L1 (5c)
: En moulinette. Il faut chercher un peu, mais pas de difficulté. Belle longueur en dalle.

_La Soupe aux cailloux_ L2 (5c)
: En moulinette. Courte mais très belle et impressionnante !

_La Soupe aux cailloux_ L3 (6a+)
: En tête à vue. Dièdre, dalle et une traversée sous un toit, longueur magnifique !

_La Soupe aux cailloux_ L4 (6b)
: En tête à vue. Dalle, bombé et Dülfer, encore une longueur magnifique !

_La Soupe aux cailloux_ L5 (5a)
: En moulinette. Longueur de transition.

_La Soupe aux cailloux_ L6 (6c)
: En moulinette, enchaînée ! Longue envolée dans une dalle toute en finesse, plusieurs pas de 6b+/6c dont certains en 6b obligatoire.

Durée : 3h40 dans la voie, 45min de rappels.

---

## 2024-04-01 Mur du Son (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Romain AVISSE, Gabriel ULLIEL, Florence GRINAND, Fatima ATIGUI et Mélina ÉCHIVARD.

_Le cri du papillon_ (6a)
: En tête.

_Ultrason_ (6a+)
: En tête.

_Prise de bec_ (6b)
: En tête à vue.

_Le blues du loup_ (6c)
: En tête flash au premier essai. Pas de dalle très fin au début, puis deux murs très verticaux assez délicats. Voie très exigeante dans la cotation.

_La vie en rose_ (6a+)
: En tête.

_Tsunamis spleen_ (6c)
: En tête. Facile dans la cotation, sympathique.

_Ding ding dong_ (6a)
: En tête, une chute. Un bon pas de 6c au milieu !

_Fantasy climatique_ (6a+)
: En tête.

_Clack-son_ (6a+)
: En tête.

---

## 2024-03-29 Rocher de Saint-Julien (Buis-les-Baronnies)

Avec Manon GABARRET.

Avec un vent violent (rafales à 100km/h !).

### Secteur _L'espadon_

_Le Gastronome_ L1 (5c)
: En tête à vue.

_Le Gastronome_ L2 (5c)
: En moulinette.

_Le Gastronome_ L3 (5c)
: En tête à vue. Départ en traversée pas évident, mais bien protégé. Courte longueur.

_Le Gastronome_ L4+5 (5c, 5b)
: En moulinette.

### Secteur _Les Guêpes_

_Les Guêpes_ L1 (5b)
: En moulinette.

_Les Guêpes_ L2 bis (5c+)
: En tête à vue. Facile et esthétique.

_Les Guêpes_ L3 (5b)
: En moulinette.

---

## 2024-03-24 Aiguilles de Bénévise (Bénévise)

Organisé par CAF Théo LAMBERT.  
Avec Paul CHRISTOPHLE et Jean-Mathieu MONTMAGNON.

Grande voie _À Thor et en travers_, toute en traversée (9 longueurs horizontales et 2 longueurs verticales pour sortir).

_À Thor et en travers_ L1+L2 (3a, 5c)
: En tête. Un passage A0 dans L2 avec une chasse d'eau en place.

_À Thor et en travers_ L3+L4+L5+L6 (4c, 3c, 5a)
: En moulinette. Corde tendue.

_À Thor et en travers_ L7+L8 (5a, 4a)
: En tête à vue.

_À Thor et en travers_ L9 (5a)
: En moulinette.

_À Thor et en travers_ L10 (5c A0)
: En tête.

_À Thor et en travers_ L11 (4c)
: En moulinette.

---

## 2024-03-22 Gorges de Saint-Moirans (Chastel-Arnaud)

Avec Manon GABARRET.

### Secteur _Dent du Cafiste_

_Vert ou Jaune_ (5a)
: En tête à vue. Plutôt 5b+/5c.

_Nom non communiqué_ (5b)
: En tête à vue. Toute en traversée, plutôt 5b+/5c.

_Dansez !_ (5b)
: En tête. Plutôt 5c+/6a, début physique puis dalle toute en finesse.

_Nono ou ouioui_ (4c)
: En tête à vue. Sympathique dièdre facile.

_Faim du jour_ (6a)
: En tête. Magnifique pilier ! On peut sortir au sommet en poursuivant au-dessus du relais.

_L'Oeuvre au noir_ L1 (6a)
: En tête. Très jolie longueur. Ne pas partir trop vite à droite au début !

---

## 2023-11-26 La Ceyte (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Diane TOURET, Chloé Maxence, Florent FACON, Edith VERNAY, Nicole HAXAIRE, Dominique DAVID, Gabriel ULLIEL, Véronique LIOTIER et Marie-France DUMAS.

### Secteur _Soleil_

_Pulsion primitive_ (5c)
: En tête.

_Pars_ (5b)
: En moulinette. Rappel de 50m indispensable.

### Secteur _Tête en l'air_

_Scène de panique tranquille_ (6a+)
: En tête à vue. Ignoble et difficile !

---

## 2023-11-19 Rocher des Aures (Roche-Saint-Secret-Béconne)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Manon GABARRET, Sarah GREMILLON, Romain AVISSE, Lorraine LETRANCHANT, Sylvaine VALETTE, Jean-Mathieu MONTMAGNON, Marianne LACOSTE, Delphine SACHOT et Vincent POILEVEY.

Voie non nommée, à gauche de _Bilibili_ (5b+)
: En tête à vue. Physique !

_Bilibili_ (5b+)
: En tête à vue. Toujours physique

_Manon_ L1 (5b)
: En tête à vue. Très jolie et facile.

_Lotantique 1_ (6a+)
: En tête à vue. Pas de difficultés techniques, mais longue et continue, exigeante physiquement.

_Bilame_ (5c)
: En tête. Jolie, assez facile.

_Spitomania_ (6b)
: En tête à vue. Un pas vraiment pas évident à vue, le reste dans le même esprit que _Lotantique_: peu technique mais physiquement exigeante.

---

## 2023-11-04 GV/TA (Saint Victoire)

Organisé par CAF Paul CHRISTOPHLE et Daniel CONSTANT.  
Avec Mélanie GROS, Lorraine LETRANCHANT et Fatima ATIGUI.

### Secteur _Baou des Vespres_

_Le Jardin Suspendu_ L1 (3)
: En tête à vue.

_Le Jardin Suspendu_ L2 (5a)
: En moulinette.

_Le Jardin Suspendu_ L3 (5a)
: En tête à vue. Très jolie longueur !

_Le Jardin Suspendu_ L4 (5a)
: En moulinette. Traversée fine, mais quelques pitons en place.

_Le Jardin Suspendu_ L5 (5a)
: En tête à vue. Fissure-cheminée bien ouverte, sous la pluie c'est pas évident !

_Le Jardin Suspendu_ L6 (5a)
: En moulinette.

### Secteur _Subéroque_

_Arête Trouée_ L1 (3c)
: En moulinette

_Arête Trouée_ L2 (5c)
: En tête à vue. Grande longueur, relais confortable sur un arbre au niveau d'une terrasse. Peut se couper en deux au niveaux de deux pitons dans une dalle sur le fil du pilier.

_Arête Trouée_ L3 (6a)
: En moulinette. Pas évidente, relais sur un piton un peu caché + friends au pied du trou

_Arête Trouée_ L4 (5c)
: En tête à vue. Variante en contournant le trou par la gauche, puis mur en 5c jusqu'à une petite terrasse que l'on atteint avec un pas de désescalade.

_Arête Trouée_ L5 (6a)
: En moulinette. Dalle en 5c/6a avec pitons, pas évidente...

_Arête Trouée_ L6+7 (4c, 3)
: Corde tendue, facile.
: En réversible, les longueurs ne sont pas clairement définies)

---

## 2023-11-01 Les Rangs de Mars - Suzette Flipo et Ricky Banlieue (Pont-de-Barret)

Avec Manon GABARRET.

_Les vaccins_ (4c)
: En tête à vue. Pas évidente !

_Histoires drôles_ (4c)
: En tête à vue. Sympathique et intéressante.

_Ne laisse rien_ (5c)
: En tête à vue.

---

## 2023-10-29 Rocher des Abeilles (Soyans)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Fatima ATIGUI, Florent FACON, Alexis ROVELLI, Alison AUGIER, Edith VERNAY, Dominique DAVID, Tristan DESTRAIT, Florence GRINAND et Melina ECHIVARD.

_Moniebah_ (5c)
: En tête.

_Le laminoir de feu_ (6a+)
: En tête.

_Mosaïque_ (5c)
: En tête.

_Scorpions rebelles_ (5c)
: En tête.

---

## 2023-10-28 Petite Cournouse (Châtelus)

Avec Théo LAMBERT.

_Classique du Gendarme_ L1 (5c)
: En moulinette.

_Classique du Gendarme_ L2 (5b+)
: En tête à vue. Pas évidente à protéger !

_Classique du Gendarme_ L3 (5b)
: En moulinette.

_Classique du Gendarme_ L4 (5b)
: En tête à vue. Facile.

_Classique du Gendarme_ L5 (6a)
: En moulinette. Un pas de 6a bien protégé, le reste 5b.

_Classique du Gendarme_ L6 (6a)
: En tête à vue. Une cheminée raide qui se protège bien (C4 #0.3-#3).

_Classique du Gendarme_ L7 (5c)
: En moulinette. Pas si facile encore !

---

## 2023-10-15 Les Cabanes (St-Maurice-en-Chalencon)

Sortie d'initiation au TA organisée par CAF Paul CHRISTOPHLE.
Avec Alexandre CHARRA, Jean-Mathieu MONTMAGNON, Florence GRINAND, Fatima ATIGUI, ...

### Secteur _X Bis_

_Every body_ (5c)
: En tête à vue, sur friends et câblés uniquement. Idéale pour découvrir la grimpe en fissure et le TA.

_À cheval_ (5c)
: En tête à vue. Intéressante si l'on reste dans la fissure.

_Danse avec les écureils_ (5c)
: En moulinette. Grimpe en fissure en bas, un bombé délicat puis facile.

_Son bleu_ (6a)
: En moulinette. Tout est dans le pas du départ.

### Secteur _K Net_

_Lisse comme un c.._ (6a)
: En tête, sur friends uniquement.

_Flicophobie_ (6c)
: En moulinette. Enchaînée !

---

## 2023-10-13 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Mélanie GROS, à l'après-midi.

### Secteur _Spigolo_

_Spigolo_ L1 (4c)
: En tête à vue. Pas évidente, peu de points.

_Spigolo_ L2 (5b)
: En moulinette. Sur le fil, facile, aérienne et élégante.

_Spigolo_ L3 (5c)
: En tête à vue. Belle longueur, un pas un peu délicat pour sortir du dièdre.

Sortie par l'arête, très aérien et esthétique mais pas de point.

### Secteur _Les Trois P_

_Jardin suspendu_ L1 (5b)
: En moulinette. Un peu déroutante.

_Voie Françoise_ L2 (4b)
: En tête. Traversée facile vers la gauche, une plaquette inox et un gros piton (c'est tout !).

_Voie Françoise_ L3 (5b)
: En moulinette. Remonter une fracture évidente à droite, 3 pitons seulement.

_Voie Françoise_ L4 (5c)
: En tête. Longueur bien équipée (ça change !), crux facile avec rétablissement à gauche du trou.

---

## 2023-09-25 Gorges du Nan (Malleval-en-Vercors)

Avec Mélanie GROS.

_Pastrou_ L1 (5a)
: En moulinette. Longueur peu engageante, en cheminée patinée, mais facile.

_Pastrou_ L2 (3b)
: En tête, A0. Rocher un peu péteux, pas moyen de bien protéger...

_Pastrou_ L3 (5c)
: En moulinette. Belle longueur !

_Pastrou_ L4 (5a)
: En tête, A0. Grosse ambiance, toute en traversée.

_Pastrou_ L5 (4c)
: En moulinette. Encore une belle longueur, peu de points mais facile à protéger.

_Pastrou_ L6 (3b)
: En tête à vue. Sortie par une courte cheminée.

---

## 2023-09-20 Falaise d'Anse (Omblèze)

Avec Mélanie GROS, Lorraine LETRANCHANT, Isabelle VILBERT et Jean-Marc MOURGUE.

### Secteur Bicolore

_Le Pilier du Gag_ (5c)
: En tête à vue. Facile, mais les points sont un peu loin.

_La clé du Nirvana_ (6a)
: En tête à vue. Très jolie, un peu engagée.

_Savannakumba_ (6a)
: En tête à vue. Un poil plus dure, dans la dalle là-haut.

_Café sur terrasse_ (6a+)
: En tête à vue. Crux délicat sur gouttes d'eau. Sortie par une cheminée facile.

_Bicolore_ (6b+)
: En moulinette, enchaînée au 2e essai. Le crux se situe au milieu de la voie : deux petites prises à tenir. Belle mais physique, quand même !

---

## 2023-09-17 Beaumirail (Le Pouzin)

Avec Mélanie GROS.

### Secteur Mistral Gagnant

Des voies pas évidentes, mais à l'équipement sur plaquettes irréprochable.

_Des cités exilées_ (5c+)
: En tête à vue. Dans la fissure, un mouvement pour sortir.

_Contre vents et marées_ (5c)
: En tête à vue.

_La voix du nord_ (5c)
: En tête, 2 chutes puis A0. La cotation doit probablement être révisée à la hausse depuis la casse de prises. Le 6a à gauche est plus facile !

_L'aigri bouilleur_ (6a)
: En tête à vue. Courte mais exigeante, avec un début à bras, puis une fissure délicate et une sortie qui demande de la concentration. Bon chantier pour les gens qui ne l'enchaînent pas !

_Le chant barde ment_ (6b)
: En tête, repérage jusqu'à la 3e dégaine seulement. A l'air ignoble.

### Secteur Falaise

Des voies difficiles équipées n'importe comment : premier point à 4m+ du sol !

_Le coeur gros_ (5b)
: En tête à vue. Facile sauf le départ. Attention au risque de retour au sol !

_Pas de pieds_ (6a+)
: En moulinette, A0. Très difficile et exposée. Début fin mais intéressant.

_Le Phil amant_ (6a+)
: En moulinette, repérage jusqu'à la 3e dégaine seulement. De plus en plus facile vers le haut.

---

## 2023-09-03 La Graville (Saou)

Organisé par CAF Thibaud BACKENSTRASS.
Avec Alexandre CHARRA, Yann BERLEMONT, Claire SELLIER, Nicole HAXAIRE, Marion BREGAND, Agnès CAPITANI et Armelle THOUVENOT

### Secteur Nord

_Dix ans déjà_ (6a+)
: En tête. Splendide ! Un pas délicat pour sortir de la fissure.

_Alertez les bébés_ (5c)
: En tête. Un gros surplomb à prendre par la droite, le reste facile.

_Toto_ (4a)
: En tête à vue.

_Cool_ (3b)
: En tête à vue.

_Le plan_ (4b)
: En tête.

_Directe de la Vèbre_ (5c)
: En tête.

### Secteur Est

_Méphistophélès_ (6a+)
: En tête. Une dalle bien lisse à contourner par la gauche.

_Le diable en rit encore_ (6b+)
: En tête à vue. Début bien physique, puis sortie très fine en dalle. Voie pas évidente.

---

## 2023-08-23 La Cime du Mas (La-Chapelle-en-Vercors)

Avec Manon GABARRET

### Secteur principal

_À Bloc_ (5b)
: En tête à vue. Un pas morpho au milieu.

_Zip_ (5c)
: En tête.

_Détendez-vous_ L1 (5b)
: En tête. Magnifique voie !

_Voie des Trous_ (6a+)
: En tête. Crux sur trous assez délicat, puis une sortie fine et lisse.

### Secteur _Gros Bacs_

_Afin que tous.tes se meuvent_ L1+L2 (5a, 5b)
: En tête à vue. 40m, le début de L2 est poussiéreux et un peu végétal.

_Afin que tous.tes se meuvent_ L3 (5b)
: En moulinette. Succession de bacs. Départ un peu morpho, puis belle traversée ascendante à droite. Courte longueur.

_Afin que tous.tes se meuvent_ L4 (5b)
: En tête à vue. Succession de bacs. Un crochet à droite, puis longue traversée ascendante à gauche avec deux pas un peu morpho.

_Afin que tous.tes se meuvent_ L5+L6 (5b, 4b)
: En moulinette. Un pas déversant mais bien prisu, puis ça se couche, de plus en plus facile jusqu'au sommet.

---

## 2023-08-10 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Christophe HARLEZ en soirée.

### Secteur _Le Bec_

_In the wake of Posséidon_ L1+L2 (6a)
: En tête.

_Nouvelle génération_ L1+L2+L3 (6b)
: En tête, A0+. L3 est courte, mais fout sacrément la trouille. Bien utiliser la colonnette à gauche !

_JC m'a sauvé_ (6a)
: En tête.

---

## 2023-07-20 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Manon GABARRET en soirée.

### Secteur _Rasoir_

_Rasoir direct_ (5a)
: En tête.

### Secteur _Le Bec_

_Zelda cotée_ (6a+)
: En tête.

_JC m'a sauvé_ (6a)
: En tête.

_Le pilier des Anciens_ L1 (5c)
: En moulinette.

---

## 2023-07-18 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Mélanie GROS, Mathieu MAISONNEUVE et Sarah GRÉMILLON, en soirée.

### Secteur _Les Gorges Rive gauche_

Deux voies non-nommées en face des _Cheminées_, en tête à vue.

### Secteur _Les Gorges Rive droite_

_Les Cheminées L1_ (5a)
: En moulinette.

_Les Cheminées_ L2 (5c)
: En moulinette.

### Secteur _Le Bec_

_La 3e fois était la bonne_ (6b+)
: En tête, une pause. Magnifique longueur, accessible ! À enchaîner.

---

## 2023-07-11 Barbières (Barbières)

Avec Manon GABARRET, en soirée.

### Secteur _Les Dièdres_

_Le vol du Snake_ (5c+)
: En tête. Un début exceptionnellement teigneux qui se contourne bien par la gauche (ou tout droit en A0), puis plus facile et joli.

_La couenne des bleus_ (5b)
: En tête à vue. Très patinée et très courte.

_Lézard somnambule_ (5b+)
: En tête à vue. Intéressante mais courte.

### Secteur _Les Buis_

_Mauvaise mine_ (5c+)
: En tête à vue. Physique au départ, puis facile après le réta.


---

## 2023-07-04 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Manon GABARRET, en soirée.

### Secteur _Le Bec_

_Pao's Line_ (5b)
: En tête. Juste à gauche du _Pilier oublié_, sur le fil du pilier.

2 voies non-nommées à gauche de _Zelda cotée_ (5b)
: En tête. Ouverture B. Hogrel.

_In the wake of Posséidon_ L1+L2 (6a)
: En tête. Un pas de 6a dans L2, le reste facile.


---

## 2023-05-20 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Mélanie GROS.

### Secteur _Les Gorges_

_Le pilier des Anciens_ L1 (5c)
: En tête, A0. Relais chaîné.

_Le pilier des Anciens_ L2 (5a)
: En moulinette. Relais chaîné à droite d'une gorge.

_Le pilier des Anciens_ L3 (4b)
: En moulinette.

_Le pilier des Anciens_ L4 (5b)
: En tête à vue. Belle longueur ! Relais chaîné.

_Le pilier des Anciens_ L5 (4b)
: En moulinette. Relais chaîné.

_Le pilier des Anciens_ L6 (4c)
: En tête à vue. Relais chaîné.

_Le pilier oublié_ L7 (6a > 5b+ A0)
: En tête, A0. Le pas de 6a est fin et morpho, pas homogène avec le reste de la voie. Relais chaîné au sommet.

---

## 2023-05-04 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Grégoire NONOCHIAN.

### Secteur _Les Gorges_

_Les Cheminées_ L1 (5a)
: En tête, A0.

_Les Cheminées_ L2 (5c)
: En moulinette.

_Les Cheminées_ L3 (5b)
: En tête à vue.

_Les Cheminées_ L4 (3c)
: En moulinette.

_Les Cheminées_ L5 (3c)
: En tête à vue.

_Les Cheminées_ L6 (5a)
: En moulinette.

_Les Cheminées_ L7 (6a>5b A0+)
: En tête, A0.

_Les Cheminées_ L8 (5a)
: En moulinette. Magnifique dalle de sortie !

---

## 2023-04-28 Falaise d'Anse (Omblèze)

Avec Manon GABARRET.

### Secteur _Dopage_

_Monsieur Propre_ L1 (5b)
: En tête. Facile.

_Le nom de Dieu_ (6a)
: En tête.

### Secteur _Cacaboum_

_Mylène, je t'aime_ (5c+)
: En tête à vue.

_Tac (gauche)_ (6a+)
: En tête à vue. Déversante sur gros bac, un réta peu évident à la sortie.

### Secteur _Karine_
_Empreinte digitale_ (5c)
: En tête à vue. Jolie, attention à l'itinéraire.

---

## 2023-04-23 Aiguille de la Tour (Saou)

Avec Manon GABARRET.

_La foire aux nichons_ (5c)
: En tête à vue.

_Wesley le Loup fugueur_ (5c)
: En tête à vue. Très jolie !

_Tonbombadilom_ (6a)
: En tête à vue. Jolie, passage sur gouttes d'eau.

_Le Saharaoui_ L1 (5c)
: En tête à vue. 45m.

_Le Saharaoui_ L2 (5c)
: En moulinette. Dülfer, fissure et dièdre au programme ! Belle longueur soutenue !

_Le Saharaoui_ L3 (5b)
: En tête à vue. Grimpe en fissure et oppositions, plutôt 5c. Courte, jolie traversée pour sortir.

---

## 2023-04-09 Mur du Son (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Romain AVISSE, Anne-Sophie PETIT, Fatima MASSOUD, Valentin DEPREZ et Frédéric LACHERAY.

_Saoûffle chaud_ (6a)
: En tête.

_The Sound of silence_ (6b)
: En tête à vue. Crux constitué d'une succession de prises très fines, bien polies et à doigts, probablement 6c.

_La java des promesses_ (6b)
: En tête à vue. Un pas morpho en 6b, le reste 5c.

_Boum !_ (6a+)
: En tête.

_Mosaïque_ (5c)
: En tête.

_Synclinal Mach_ (6b+)
: En tête à vue. Crux fin à prendre par la gauche en milieu de voie.

_Supersonique_ (7a)
: En moulinette, travail. La voie s'équipe du bas en artif avec une dégaîne panic. Crux sur deux dégaines au milieu de la voie, le reste facile. Crux incompris : prises trop petites, assez de pieds mais aucune main ?

---

## 2023-01-01 Rocher des Abeilles (Soyans)

Avec Manon GABARRET.

_Ma brave dame_ (5b)
: En tête.

_Couleur Café_ (5b)
: En tête.

_Frisson d'avril_ (5c)
: En tête.

_Moniebah_ (5c)
: En tête.

_Mosaïque_ (5c)
: En tête.

_Scorpions rebelles_ (5c)
: En tête.

---

## 2022-11-27 Mur du Son (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Manon GABARRET, Mathieu MAISONNEUVE, Anne-Sophie PETIT et Christophe HARLEZ.

_Crescendo_ (5b)
: En tête.

_Saoûffle chaud_ (6a)
: En tête. Homogène.

_À voie basse_ (5c+)
: En tête.

_Boum!_ (6a+)
: En tête.

_Tsunami spleen_ (6c)
: En tête. Crux en finesse dans la dalle du bas, puis plus facile. Sortie déversante sur bonnes prises (à trouver).

_Ding Ding Dong_ (6a > 6c ?)
: En tête à vue. Premier tiers en 5b, puis un passage où des prises clés ont cassé, très fin en dalle, probablement 6c ou 6b bloc. Sortie facile mais impressionnante.

---

## 2022-11-25 La Ceyte (Saou)

Avec Manon GABARRET.

### Secteur _Soleil_

_La fabuleuse histoire de Mr Septembre Rose_ L1 (6a)
: En tête. Progressive et esthétique, bonne voie de chauffe.

_Quoi de neuf docteur ?_ (6a)
: En tête. Crux à mi-longueur, fin peu évidente.

_Hold Tight_ (6a+)
: En tête. Très belle, un peu de recherche d'itinéraire dans le haut.

_Solstice_ L1 (5b)
: En moulinette

_Solstice_ L2+L3 (4c, 2a)
: En tête à vue. Terrain à chamoix, blocs instables et itinéraire peu évident. Quelques points au début jusqu'à R2, puis une vire part sur la droite quelques mètres sous le relais.

_Solstice_ L4 gauche (5c)
En moulinette. Rocher moyen, escalade facile. Relais sur une terrasse au niveau d'un grand arbre.

_Solstice_ L5 (6a)
En tête à vue. Traversée franche vers la droite, puis sortie à gauche dans des rochers délités (1 piton + arbres). Relais chaîné. Rappels de descente foireux !

---

## 2022-11-20 La Ceyte (Saou)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Marck TO, Romain AVISSE, Patrick LAMBERT et Noëlle COULET, Muriel GRASSET, Nicole HAXAIRE, Marion BREGAND, Jacklyn et Denis MAISONNEUVE, Jacques CLAIR et Léonie CONSTANT.

### Secteur _Soleil_

_Pulsion primitive_ (5c)
: En tête à vue. Jolie, dans le même esprit que ses voisines.

_Viol collectif de 18h01_ (5c)
: En tête. 5c jusqu'au relais intermédiaire.

_Objectif Terre_ (5c)
: En tête. Facile, plutôt 5b. En traversée vers la gauche.

_Solstice_ (5b)
: En tête. Plutôt 5a, rocher délité dans le haut. Longueur de 45m, attention au tirage.

---

## 2022-11-13 Presles (Presles)

Avec Alexandre CHARRA, Grégoire NONOCHIAN et Fatima ATIGUI.

### Secteur _Buis_

_La voie des Buis_ L1 (4b)
: En tête à vue. Au relais intermédiaire, continuer à grimper au-dessus et traverser à gauche après, ne pas prendre la vire horizontale à gauche.

_La voie des Buis_ L2 (4c+)
: En moulinette.

_La voie des Buis_ L3 (5c)
: En tête à vue. Très belle longueur !

_La voie des Buis_ L4 (4b)
: En moulinette. On arrive sur la vire médiane, coin photogénique.

_La voie des Buis_ L5 (5b)
: En tête à vue. Jolie longueur.

_La voie des Buis_ L6 (5c+)
: En moulinette. Une cheminée à ramoner, passer le sac entre les jambes sous les pieds.

_La voie des Buis_ L7 (5c)
: En tête à vue. C'est bien du 5c, pas du 4c comme sur certains topos. Suivre les points.

_La voie des Buis_ L8 (4b)
: En moulinette.

Horaire : environ 4h, en prenant son temps et en s'attendant aux relais.

---

## 2022-11-06 Rocher de Saint-Julien (Buis-les-Baronnies)

Organisé par CAF Thibaud BACKENSTRASS et Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Patrick LAMBERT et Noëlle COULET, Alexandre CHARRA, Florence GRINAND et Fatima ATIGUI.

### Secteur _Les Guêpes_

_Les initiateurs_ L1 (6a+)
: En tête à vue. Crux à la fin de la longueur, à négocier par la gauche.

_Les initiateurs_ L2 (6a+)
: En tête, A0. Début de longueur compliqué, à naviguer de gauche à droite et de droite à gauche. Sortie sympathique jusqu'au sommet.

### Secteur _La Grotte_

_La Grotte_ L1 (5b)
: En tête à vue. Dalle très facile, puis un pas délicat, puis passage ludique dans la grotte. Relais de gauche après la grotte.

_La Grotte_ L2 (5c+)
: En moulinette. Dièdre et fissure sur bonnes prises.

_La Grotte_ L3 (6a)
: En tête à vue. Magnifique traversée toute en finesse, tantôt en ascendance ou en descente.

_La Grotte_ L4 (5c)
: En moulinette. Superbe longueur sur le fil du pilier.


---

## 2022-11-05 Aiguille de Buis (Buis-les-Baronnies)

Organisé par CAF Thibaud BACKENSTRASS et Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Patrick LAMBERT et Noëlle COULET, Alexandre CHARRA, Florence GRINAND et Fatima ATIGUI.

### Secteur _Cassiopée_

_Vagabond_ L1+L2 (5c)
: En moulinette. Un bel enchaînement de 50m, homogène et abordable. La deuxième partie est très intéressante.

_Vagabond_ L3 (6a)
: En tête à vue. Une rupture par rapport au style de L1 et L2 : succession de bombés avec des passages blocs.

### Secteur _Pilier_

_Tire fort ça coince_ L1 (6a+)
: En tête, une chute à la sortie. Magnifique longueur ! Début sur grosses écailles, puis une dalle qui demande de la réflexion. Sortie par un pas bloc délicat sans connaître les prises, plutôt à droite du point.

_Label du coin_ L1 (6a)
: En tête à vue. De la belle grimpe dans un magnifique rocher jaune, sur prises variées (dalle, colonettes, dièdre, ...) !

---

## 2022-10-30 Falaise d'Anse (Omblèze)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Gael VIEZ, Matthias et Romain CANTELLI, Paul CHRISTOPHLE, Fatima ATIGUI, Manon GABARRET, Nicole HAXAIRE, Grégoire et Nadine NONOCHIAN et Sylvaine VALETTE.

### Secteur _Lutin Malin_

_Mélodie Nelson_ L1+L2 (6a)
: En tête. Des blocs branlants dans le haut.

### Secteur _Tutévu_

_Shut Your Mouth_ L1+L2 (6b+)
: En tête à vue ! Magnifique longueur, crux dans la dalle au milieu de L2 (bi-doigt inversé et réglettes à droite).

_L'aphone des falaises_ L1+L2 (6c)
: En tête, 3 pauses. Un mouvement très fin avec peu de pieds à mi-longueur pour basculer à gauche, puis physique.

_Pâques aux Rabans_ L1 (6b)
: En tête. Des prises clés deviennent branlantes, un peu plus physique qu'avant.

---

## 2022-10-28 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par Jérémy LOMBARD.  
Avec Yvon CHAPUS (ami de Jérémy).
Sortie sur l'après-midi.

### Secteur _Combe Arnaud_

_Le violeur de Bagdad_ (6a+)
: En tête. Voie d'échauffement, exigeante mais belle ! Sortie du dièdre par le haut.

_Roquefort Saucisson_ (6c)
: En tête à vue ! Plutôt facile, bien prisue mais technique. Départ dans un dièdre malcommode, puis traversée fine sous un toit.

_Ascenseur pour l'échafaud_ L1 (7a)
: En moulinette. Facile jusqu'à l'avant-dernier point, puis gros mouvements pas réussis. L2 en 8a, quand même...


---

## 2022-10-16 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Romain AVISSE, Fatima ATIGUI, Alexandre CHARRA, Yvette HEBRARD, Nicole HAXAIRE, Muriel GRASSET, Patrick, Noelle & Théo LAMBERT, Gaëtan VAN THEEMST, Sylvaine VALETTE.

### Secteur _K Net_

_BN_ (5c)
: En tête à vue.

_Cosmos_ (5c)
: En tête. Pas si évidente.

_OCB_ (6a)
: En tête.

_Harry fait du zèle_ droite (6c)
: En tête, une chute. Manque de concentration dans la fin, c'est pas tout à fait gagné.

_CD07_ (6a)
: En tête.

_Laura_ (6c)
: En tête, travail.

_Flicophobie_ (6c)
: En tête & en moulinette, travail.

---

## 2022-10-15 Pierrot Beach (Presles)

Avec Jérémy LOMBART, Alexandre CHARRA et Floriane (Densité).

Secteur de couenne exigeant, les cotations ne sont pas cadeau mais on est à Presles. Grimpe souvent déversante et physique, peu de pieds francs.

Il fait chaud : 23°C et pas de vent !

### Secteur _Olivebrius_

_Merry Mary_ (6a)
: En tête à vue. Départ dans _Olivebrius_ (5c patiné), puis on bifurque vite à gauche sur des plaquettes. D'abord physique, puis psycho, on arrive enfin dans une magnifique dalle à gouttes d'eau ! Bien traverser à gauche sans monter trop tôt.

_Bichon futé_ (6b+)
: En moulinette, travail. Comme deuxième voie, ça chauffe et ça casse. Travail en moulinette : dalle fine sous la vire médiane, puis gros mouvements avec peu de pieds au milieu et retour dans une dalle fine avant d'arriver sur les gouttes d'eau. Très exigeante dans l'ensemble, mais abordable après travail.

### Secteur _Mur Bleu_

_Areuh briant_ (7a)
: En moulinette, A0+ sans aller jusqu'en haut. Incompréhensible !

---

## 2022-10-09 Presles (Presles)

Avec Grégoire NONOCHIAN.

### Secteur _Daladom_

_Gazogum_ L1 (6a)
: En tête, A0. En traversée vers la droite, on se retrouve très vite plein gaz. Attention à ne pas trop monter !

_Gazogum_ L2 (5b)
: En moulinette. Un bombé à franchir un peu sur la gauche, puis facile vers le haut (points espacés).

_Gazogum_ L3 (5c)
: En tête à vue. En traversée vers la droite, toujours plein gaz.

_Gazogum_ L4 (5c)
: En moulinette. Longueur majeure, il y a toujours une prise qui tombe sous la main ou le pied !

_Gazogum_ L5 (4c)
: En tête à vue. Un petit dièdre déroutant, puis facile sur l'arête.

Horaire : 1h30 à peine dans la voie. La plus grande longueur doit faire 35m.

---

## 2022-10-01 Traversée des Dentelles de Montmirail (Gigondas)

Organisé par Thibaud BACKENSTRASS.  
Avec Fatima ATIGUI, Florence GRINAND et Nordine ARAR.

### Traversée des _Florets_

Belle escalade, très bien équipée.

_Traversée des Florets_ L1 (5b)
: En tête.

_Traversée des Florets_ L2 (5a)
: En tête. Belle longueur.

_Traversée des Florets_ L3 (4c)
: En moulinette.

_Traversée des Florets_ L4 (4c)
: En tête.

_Traversée des Florets_ L5 (3b)
: En moulinette.

_Traversée des Florets_ L6 (5b)
: En tête. Belle longueur.

_Traversée des Florets_ L7 (5a)
: En moulinette.

_Traversée des Florets_ L8+L9 (4b, 3b)
: En tête.

Sortie par le rappel de la brèche du _Turc_ en versant sud.

[Topo C2C](https://www.camptocamp.org/routes/425766/fr/dentelles-de-montmirail-chaine-de-gigondas-traversee-florets-turc-pousterle)

---

## 2022-09-25 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Fatima ATIGUI, Pascale RENAUDET, Nicole HAXAIRE, Marion BRÉGAND, Jacklyn et Denis MAISONNEUVE, Loïc CHARRAS.

### Secteur _Le Bec_

_Éros Ion Positif_ (7a, 5c)
: En tête. Enfin enchaînée !

_JC m'a sauvé_ L1 (6a+)
: En tête. Ouverture B. Hogrel, à droite de Éros Ion Positif.

_In the wake of Posséidon_ L1+L2 (6a)
: En tête à vue. Un pas de 6a dans L2, le reste facile.


---

## 2022-09-23 La Réserve (Pont-en-Royans)

Avec Grégoire NONOCHIAN.

### Secteur _Le Triangle de la Réserve_

_Qui veut des godiveaux_ L1 (5b)
: En moulinette.

_Qui veut des godiveaux_ L2 (5c)
: En tête à vue. Plutôt 5b ?

_Qui veut des godiveaux_ L3 (6a)
: En moulinette. Un pas de 6a, possible en A0.

_Qui veut des godiveaux_ L4 (4a)
: En tête à vue.

_Qui veut des godiveaux_ L5 (4b)
: En moulinette.

---

## 2022-09-18 Mer de glace (Villevocance)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Grégoire et Nadine NONOCHIAN, Théo LAMBERT, Alexandre CHARRA, Marck TO, Romain AVISSE, Frédéric et Loïc CHARRAS.

### Secteur _Grotte_
_Faut qu'on_ (5b+)
: En tête. Un pas pour arriver au relais.

_Chou-rave_ (6a)
: En tête à vue. Un gros pas bloc au départ, puis une dalle technique mais assez facile.

_La grande Duduche_ (6b)
: En tête. Deux pas blocs, un au début (traverser à droite) et le plus difficile en haut (sortie par la droite).

_Grottal_ (6a)
: En tête à vue. Une dalle avec un pas en gainage, puis un sympathique dièdre facile.

_Y'a qu'à_ (5b)
: En tête, sur coinceurs et friends. Toujours aussi belle !

### Secteur _La boîte aux lettres_

_Que dalle_ (6a+)
: En tête. C'est fin... très fin même !

### Secteur _Le Blaireau_

_Fais ta prière blaireau_ (6b)
: Un gros mouvement psycho au début, puis facile.

---

## 2022-09-10 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Fatima ATIGUI.

### Secteur _Les Gorges_

_Le Chemin des Dames_ L1 (4b)
: En moulinette.

_Le Chemin des Dames_ L2 (5b)
: En tête.

_Le Chemin des Dames_ L3 (4b)
: En moulinette.

_Le Chemin des Dames_ L4 (4b)
: En tête.

_Le Chemin des Dames_ L5 (5c>4b/A0)
: En tête.

_Le Chemin des Dames_ L6 (5a)
: En moulinette.


---

## 2022-08-26 Vallon de la Fauge (Villard-de-Lans)

Avec Mélanie GROS, Édouard REYNAUD et Mylène (une amie de Mélanie).

### Secteur _Fauge 3 Initiation_

_Ninouille_ (4a)
: En tête à vue. Voie d'initiation intéressante.

### Secteur _Fauge 3_

_Pas facile_ (5c+)
: En tête à vue. Gros mouvements sur gros bacs, et quelques pas d'adhérence. Facile.

_Lady Bona_ (6a+)
: En tête à vue. Crux en première partie de longueur, et encore un pas plus haut. Belle continuité.

_Petite fée de décembre_ (6a)
: En tête à vue. Sympathique, pas de grosse difficulté.

_Jebel Vercors_ (6a+)
: En tête à vue. Crux entre 3e et 5e dégaine, le reste facile. Joli.

---

## 2022-08-04 Tourniol (Barbières)

Avec Christophe HARLEZ.

Seule option de grimpe s'offrant à nous en ce début août caniculaire : 38°C à Valence et vent du Sud !

Voie historique. Rocher globalement moyen, des passages très friables et d'autres, trop rares, en bon rocher.

_La Seigneur-Valençot_ L1 (6c)
: En moulinette. Deux bombés délicats, en A0/+.

_La Seigneur-Valençot_ L2 (5c)
: En tête, A0. Exigeante.

_La Seigneur-Valençot_ L3 (5b)
: En moulinette. Escalade à l'ancienne.

_La Seigneur-Valençot_ L4 (4b)
: En tête. Longueur végétale, points espacés.

_La Seigneur-Valençot_ L5 (5a)
: En moulinette.

Horaire : départ 09h du parking, sommet à 13h.

---

## 2022-07-31 La Cime du Mas (La-Chapelle-en-Vercors)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Muriel GRASSET, Arnaud DE DINECHIN et Gaël MARCHAND.

### Secteur _Gros Bacs_

_Tichodrôme_ L1 (5b+)
: En tête à vue. Très facile sur bonnes prises.

_Le Merle bleu_ L1 (5c+)
: En tête. Très facile sur bonnes prises.

### Secteur principal

_Zip_ (5c)
: En tête à vue. Sortie un peu fine, lisse et très patinée.

_Voie des Trous_ (6a+)
: En tête. Crux sur trous assez délicat, puis toujours une sortie fine et lisse.

_Détendez-vous_ L1 (5b)
: En tête à vue. Magnifique voie !

_L'échec_ (6b+)
: En tête au premier essai. Les mouvements me sont revenus quand j'étais dedans ! Pas évident, très patiné et fin.

_À la niche_ (6c)
: En tête après travail. Très jolie, mais cotation "commerciale".

---

## 2022-07-10 Serre Châtelard (St-Laurent-en-Royans)

Avec Alexandre CHARRA.

Voie pour la liste de courses du moniteur grandes voies (TD, 140m).

_Opéra Vertaco_ L1 (6b+)
: En tête, A0. La longueur la plus difficile, on est direct dans l'ambiance.

_Opéra Vertaco_ L2 (6a)
: En moulinette. Dièdre-fissure assez mentale. Prévoir 2 C4 #1 pour compléter.

_Opéra Vertaco_ L3 (6b)
: En moulinette, A0. Un pas 6b qui passe en A0, puis un pas 6a obligatoire où il ne faut pas se la coller...

_Opéra Vertaco_ L4 (6a)
: En tête, A0. Un pas fin à prendre par la gauche, puis ça grimpe dans du joli rocher, courte longueur.

_Opéra Vertaco_ L5 (6b)
: En moulinette. Magnifique longueur en traversée sur du beau rocher orange !


---

## 2022-07-02 Aiguilles de l'Argentière (Belledonne)

Avec Christophe HARLEZ.

### Secteur _Aiguille de la Combe_

_Déboires et convictions_ L1 (6a)
: En tête, A0. Un départ physique qui surprend et qui met dans l'ambiance, puis plus facile.

_Déboires et convictions_ L2 (5b+)
: En moulinette. 50 m, c'est long !

_Déboires et convictions_ L3 (5c)
: En tête à vue.

_Déboires et convictions_ L4 (5b)
: En moulinette. Un pas vaut bien 5c...

_Déboires et convictions_ L5 (4b)
: En tête à vue. Arête facile.

Descente par la face ouest et la brèche de la Combe.

Horaire :

 * 08h50 départ du col du Glandon
 * 10h50 pied de la voie, attaque vers 11h
 * 12h00 R1
 * 13h05 R2
 * 13h45 R3
 * 14h45 sommet, pause repas
 * 16h15 retour au pied
 * 18h00 retour au col


---

## 2022-07-01 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Élina REYNAUD et Mélanie GROS.

Initiation grandes voies.

### Secteur _Les Gorges_

_Le Chemin des Dames_ L1 (4b)
: En tête à vue. Courte dalle (2p) puis marche. Ignorer un premier relais chaîné, faire relais sur un arbre au pied de la dalle.

_Le Chemin des Dames_ L2 (5b)
: En moulinette. Court dièdre en 5b peu agréable (1p), puis 4b et petit mur de 5a (1p). Relais chaîné.

_Le Chemin des Dames_ L3 (4b)
: En moulinette. Petit pilier prisu, relais aérien sur une terrasse à gauche, à relier.

_Le Chemin des Dames_ L4 (4b)
: En moulinette. Dalle, puis sur le fil du pilier.

_Le Chemin des Dames_ L5 (5c>4b/A0)
: En moulinette, en libre. Joli mur raide, facile (2p+friends).

_Le Chemin des Dames_ L6 (5a)
: En moulinette. 5a, puis 3b sur le pilier. Attention au tirage !


---

## 2022-06-21 Le Veyou (Les Trois Becs)

Avec Manu IBARRA, Grégoire NONOCHIAN et André SOUVIGNET.

Voie pour la liste de courses du moniteur grandes voies (TD-, 200m).

### Secteur _Le Veyou_

_Petit écolier_ L1 (5a)
: En moulinette.

_Petit écolier_ L2 (5c/6a)
: En tête.

_Petit écolier_ L3 (5b/c)
: En moulinette.

_Petit écolier_ L4 (5b)
: En tête. Une succession de strates magnifiques !

_Petit écolier_ L5 (5b)
: En moulinette. Magnifique dalle à silex.

_Petit écolier_ L6 (5a)
: En tête. Dièdre.

_Petit écolier_ L7 (5c)
: En moulinette. Dièdre encore.

_Petit écolier_ L8 (5c)
: En tête. Dièdre toujours, athlétique.

Horaire : 8h à Crest, 10h15 à l'attaque, 14h30 au sommet.

---

## 2022-06-14 Barbières (Barbières)

Avec Alexandre CHARRA, en soirée.

### Secteur _Les Dièdres_

_Le vol du Snake_ (5c+)
: En tête, A0. Un début exceptionnellement teigneux, puis plus facile et joli.

_À droite pour les malins_ (6a+)
: En tête à vue. Rési, le crux se contourne par... la droite !

_Michel Bis_ (6c+)
: Travail. Un gros chantier...


---

## 2022-05-24 La Goule (Châteaubourg)

Avec Jérémy LOMBARD et Alexandre CHARRA, en soirée.

_Le dièdre_ (5a)
: En tête.

_Barquette 3 grattons_ (6a+)
: En tête. Enfin enchaînée !

_Speedy Gonzales_ L1 (6c+)
: En tête, une pause. On ne sait toujours pas s'il faut passer le crux par la gauche ou par la droite...

_Speedy Gonzales_ L2 (7a)
: En moulinette. Deux essais dans le début de L2, lunaire. Un enchaînement réalisé jusqu'au bac, puis un bidoigt très très loin...

---

## 2022-05-22 La Graville (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Grégoire et Nadine NONOCHIAN, Alexandre CHARRA, Jérémy LOMBARD, Cécile CHANSARD, Nancy MARION, Fatima ATIGUI, Rose-Marie FOUAILLY et Marie CHEVALLEY.

_Riclès_ (3b)
: En tête à vue.

_Crispinette_ (5b)
: En tête.

_Sauver l'amour_ (6b)
: En tête, une chute. Lecture exigeante, voie physique. Le crux consiste à passer le premier réta, avec une boîte aux lettres loin, dégueu, à prendre main gauche.

---

## 2022-05-21 La Taupinière (Tamée)

Avec Jérémy LOMBARD et Nordine ARAR.

### Secteur _Cirque oublié_

_Trop de kouign à man_ (5c)
: En tête à vue. Sympathique, bon rocher, très bien équipée.

_Larg pa le trou_ (6a)
: En tête à vue. Très jolie, un bac au-dessus du trou.

_Ma zezer_ (6c)
: Enchaînée à vue en moulinette, puis en tête, une chute à la sortie. Belle longueur, beau rocher, facile à lire.

_Le trou rutilant_ (6b)
: En tête, une chute. Crux peu évident en milieu de longueur (morpho, pied droit très loin et pied gauche à monter très haut). Sortie sur plat peu évidente.

_Deux mariages pour une pandémie_ (6a+)
: En moulinette. Un pas franchement bloc pour passer le toit, puis facile.

---

## 2022-05-11 La Goule (Châteaubourg)

Avec Christophe HARLEZ, en soirée.

_Le dièdre_ (5a)
: En tête. Bienvenue dans une nouvelle dimension : la Goule.

_Gâteau ressucité_ (6a)
: En tête, une pause, puis travail en moulinette. Un crux teigneux.

---

## 2022-05-07 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Fatima ATIGUI, Cécile CHANSARD et Johan WONG GAR HENG.

Initiation grandes voies.

### Secteur _Les Trois P_

_Jardin suspendu_ L1 (5b)
: En tête à vue. Un peu déroutante.

_Voie Françoise_ L2 (4b)
: En moulinette. Traversée facile vers la gauche, une plaquette inox et un gros piton (c'est tout !).

_Voie Françoise_ L3 (5b)
: En tête à vue. Remonter une fracture évidente à droite, 3 pitons seulement.

_Voie Françoise_ L4 (5c)
: En moulinette. Longueur bien équipée (ça change !), crux facile avec rétablissement à gauche du trou.

---

## 2022-05-05 Gorges de Saint-Moirans - Rive gauche (Chastel-Arnaud)

Avec Manu IBARRA, Grégoire NONOCHIAN et André SOUVIGNET.

Nettoyage et purge de la dalle d'initiation.

---

## 2022-05-01 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Grégoire NONOCHIAN, Nordine ARAR, Jacques CLAIR, Catherine MALHERBE, Cécile CHANSARD, Marianne LACOSTE, Rose-Marie FOUAILLY et Nancy MARION.

### Secteur _K Net_

_Le blues de l'ouvreur_ (5c)
: En tête.

_Quarantaine rugissante_ (5b)
: En tête.

_OCB_ (6a)
: En tête.

_Soleil immonde_ (6b)
: En tête.

_Harry fait du zèle_ droite (6c)
: En tête.

_Harry fait du zèle_ gauche (6c)
: En moulinette.

---

## 2022-04-27 Presles (Presles)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE et Pascale RENAUDET.

Initiation grandes voies.

### Secteur _Éliane_

_Avec vue sur la mer_ L1 (5a)
: En tête à vue.

_Avec vue sur la mer_ L2 (4b)
: En moulinette.

_Bim Bam Boum_ L1 (4c)
: En tête à vue.

_Bim Bam Boum_ L2 (4c)
: En moulinette.

_Bim Bam Boum_ L3 (5c)
: En tête à vue. Un pas physique.

---

## 2022-04-23 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Jérémy LOMBARD.

Grimpe de la dernière chance avant un week-end de pluies...

### Secteur _Le Bec_

_Zelda cotée_ (6a+)
: En tête à vue. 5b en prenant le pilier à droite, 6a+ en restant un peu à gauche du pilier. Très bel itinéraire et belle sortie.

_Nouvelle génération_ (6b)
: En tête, A0 et pauses. Petite partie 6b psycho mais abordable : à enchaîner !

_La gouttière oubliée_ (6c)
: En tête à vue. Sortir par la 7a+ à gauche, le fil du pilier n'est plus en 6c suite à des casses de prises ?

### Secteur _Rasoir_

_Le nouveau monde_ (7a)
: En moulinette. Très dur et physique !

---

## 2022-04-18 Les Rangs de Mars - Suzette Flipo et Ricky Banlieue (Pont-de-Barret)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Christophe MERCIER, Sarah GRÉMILLON et Christophe HARLEZ.

_Radio Bagdad_ (5a)
: En tête. Facile mais psycho.

_Loxodrôme_ (5b)
: En tête à vue. Un rétablissement physique et peu prisu.

_Coin de rue_ (5c)
: En tête à vue. Un vrai plaisir !

_Fifty-fifty_ (6a)
: En tête à vue. Belle voie, très léger dévers en ascendance vers la droite. Sortie physique sur plats à gauche, ou bien avec l'écaille à droite.

_Les trous dans le réel_ (5c)
: En tête. Sortie fine à droite.

_Fermé pour ne pas mordre_ (6a-)
: En tête. La dalle de sortie n'est pas évidente...

_Suzette flipo_ (6a+)
: En tête, 1 pause et 2 chutes. Un départ physique, puis une belle continuité jusqu'à une sortie difficile.

---

## 2022-04-17 Gorges de la combe d'Oyans (Rochefort-Samson)

Avec Jérémy LOMBARD.

### Secteur _Le Bec_

_JC m'a sauvé_ L1+L2 (6a+)
: En tête. Ouverture B. Hogrel, à droite de Éros Ion Positif.

_Éros Ion Positif_ (7a)
: En tête, une pause. Facile, quelques pas de 7 au début, le reste en 5.

_Pépin Fabule_ (7a)
: En moulinette, travail. Crux sur quelques mouvements en haut de la dalle noire, traversée d>g délicate avec très peu de prises.

---

## 2022-04-10 La Ceyte (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Nordine ARAR, Nancy MARION, Cécile CHANSARD, Marianne LACOSTE, Marion BRÉGAND, Miguel GOMEZ, Ludivine MARCON et Catherine MALHERBE.

### Secteur _Soleil_

_Solstice_ (5b)
: En tête à vue. Facile, mais rappel de 50m indispensable.

_Le pilier du soleil_ L1 (5b)
: En tête.

_Lézard émietté_ L1 (5b)
: En tête à vue. Voie engagée : 5 points sur 35m !

---

## 2022-04-03 Rochers de la Condamine (Saint-Ferréol-Trente-Pas)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Nordine ARAR, Alexandre CHARRA, Denis & Jacklyn MAISONNEUVE, Mathieu MAISONNEUVE, Françoise COURTIER, Mélanie GROS, Édouard REYNAUD, Nicole HAXAIRE, Pascale RENAUDET, Fatima ATIGUI et Christophe MERCIER.

### Secteur 1 : _Vol de flèches_

_Géronimo_ (6a)
: En tête.

_Faites le clown, pas le clone_ (5b)
: En tête à vue. Un petit pas au départ.

### Secteur 2 : _Les P'tits Indiens de la roche_

_Totem_ (5c+)
: En tête.

---

## 2022-03-27 Falaise d'Anse (Omblèze)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Nadine NONOCHIAN, Marion BREGAND, Johan WONG GAR HENG, Christophe MERCIER, Fatima ATIGUI, Marianne LACOSTE, Yvette HEBRARD, Nancy MARION, Pauline LESAGE, Muriel GRASSET et Alain BRET.

### Secteur _Lutin Malin_

_Harricana_ (5a)
: En tête.

_Mélodie Nelson_ (6a)
: En tête.

### Secteur _Tutévu_

_L'Train Blues_ (5c)
: En tête.

_L'aphone des falaises_ (6a)
: En tête.

_Raining Stones_ L1+L2 (6a)
: En tête.

---

## 2022-03-23 Rocher des Abeilles (Soyans)

Organisé par Sylvaine VALETTE.

_Les deux mains prises_ (5c)
: En tête.

_Ma brave dame_ (5b)
: En tête.

_Le drapeau de la colère_ (5c)
: En tête.

_Origine_ (6a+)
: En tête.

_Le pied de biche en folie_ (6a+)
: En moulinette. Début expo.

_Ambiguë Amitié_ (6a)
: En tête.

_Le laminoir de feu_ (6a+)
: En tête.

---

## 2022-03-20 Gorges de Saint-Moirans - Rive gauche (Chastel-Arnaud)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Grégoire NONOCHIAN, Nicolas MOURIN, Alexandre CHARRA, Denis & Jacklyn MAISONNEUVE, Mathieu MAISONNEUVE, Delphine & Nathan SALLES, Muriel GRASSET, Françoise COURTIER, Mélanie GROS, Édouard REYNAUD, Nicole HAXAIRE, Pascale RENAUDET, Fatima ATIGUI et Christophe MERCIER.

### Secteur _Tilleul_

_Le bel Ayatollah_ (5a)
: En tête à vue.

_La ballade à Jimmy_ L1+L2 (6b)
: En tête à vue.

_Suppositoire effervescent_ L1 (6a)
: En tête à vue. Un enchaînement de 4 mouvements obligatoires au milieu de la voie, le reste 5c.

---

## 2022-03-19 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par Jérémy LOMBARD.  
Avec Alexandre CHARRA, Yvon CHAPUS (ami de Jérémy) et Floriane (Densité).
Sortie sur l'après-midi.

### Secteur _Combe Arnaud_
_Mélina_ (6a)
: En tête flash. Très psycho, assez difficile. Départ pied droit.

_Le violeur de Bagdad_ (6a+)
: En tête. Magnifique voie ! Début par la droite sous le toit, en s'aidant du gros bloc au pied de la voie. Sortie du dièdre par la droite (pieds loin à droite, dans la 6b) ou plus technique par le haut. Fin de la voie en dalle facile mais points espacés.

_La pluie des mangues_ (6c)
: En tête après travail. Voie très belle mais difficile, lecture délicate, prises et mouvements obligatoires pour la plupart des pieds et toutes les mains dans la moitié inférieure de la voie. Le haut est facile, mais il faut rester lucide et assez en forme.

---

## 2022-03-05 Falaise de la Barre - Mur du Vent (Saou)

Organisé par Nordine ARAR.  
Avec Jérémy LOMBARD.

Un site un peu délicat, le début des voies faciles est teigneux et parfois exposé. Ce n'est pas du tout le même style d'escalade qu'au mur du Son tout proche. Certaines sorties ressemblent à la falaise d'Eson à Pont-de-Barret.

_Mistralet_ (5c)
: En tête à vue.

_Auro_ (6a+)
: En tête à vue. Début teigneux, puis exigeant en lecture (not. 3e dégaine, cliper avec une règlette bien haut plutôt que de rester dans le trou). Sortie sur plats.

_Placements nébuleux_ (6b)
: En tête au 2e essai. Un enchaînement délicat dans la partie basse, le reste assez facile.

_Sédimentation artistique_ (6b+)
: En moulinette, travail, enchainé la 2e fois. Beau chantier ! Crux en milieu de voie avec un enchainement de prises obligatoires avec quelques inverses. Lecture et cheminement difficiles.

_Foehn renversant_ (6a+)
: En tête à vue. Facile, un dièdre avec quelques prises à la solidité douteuse... Sortie un peu physique.

---

## 2022-03-04 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par Jérémy LOMBARD.  
Avec Alexandre CHARRA, Yvon CHAPUS et Benoit (amis de Jérémy).
Sortie sur l'après-midi.

### Secteur _Rantanplan_
_Version découverte_ (5a)
: En tête à vue. Courte, facile et sympathique.

_Sortie de terre_ (5c)
: En tête à vue. Facile, peu intéressante.

### Secteur _Combe Arnaud_

_Le violeur de Bagdad_ (6a+)
: En tête après travail. Magnifique voie ! Début par la droite sous le toit, en s'aidant du gros bloc au pied de la voie. Sortie du dièdre par la droite (pieds loin à droite, dans la 6b) ou plus technique par le haut. Fin de la voie en dalle facile mais points espacés.

---

## 2021-11-21 Les Calanques - L'Oule (Cassis)

Organisé par CAF Daniel CONSTANT.  
Avec Diane TOURET, Julie DZIALOSZYNSKI, Cyrille VUIDEL, Alain SALA, Alexandre CHARRA et Yann BERLEMONT.

_Ave Maria ou Ex Voto_ L1 (5c)
: En moulinette.

Sortie par la vire des Écureils et la brèche de Castel Vieil en raison de la pluie.

---

## 2021-11-20 Les Calanques - Sormiou (Marseille)

Organisé par CAF Daniel CONSTANT.  
Avec Diane TOURET, Julie DZIALOSZYNSKI, Cyrille VUIDEL, Alain SALA, Alexandre CHARRA et Yann BERLEMONT.

### Secteur _Rumpe Cuou_

Belles voies assez courtes (60-150m) sur un magnifique calcaire blanc. Attention, on y transpire même fin novembre ! Prévoir assez d'eau...

_Les pieds dans le plat_ L1 (5b)
: En tête. Le plus dur est de décoller.

_Les pieds dans le plat_ L2 (5c)
: En tête. Très facile.

_Les pieds dans le plat_ L3 (5c)
: En tête. Cheminée, puis dalle à droite.

_Le paradigme perdu_ L1 (5b)
: En tête. Facile.

_Le paradigme perdu_ L2 (5b)
: En moulinette. Rampe inclinée un peu déroutante, mais prisue.

_Le paradigme perdu_ L3 (6a)
: En tête. Rampe inclinée délicate, sortie complètement sur la gauche.

---

## 2021-11-14 Rocher des Aures (Roche-Saint-Secret-Béconne)

Avec Nicolas MOURIN, Jean-Mathieu MONTMAGNON, Jérémy LOMBARD et Floriane (Densité).

_Le vol des six rondelles_ (6a)
: En tête, 1 pause. Un crux teigneux en haut de voie.

_Corne de bois_ (6a)
: En tête, 1 pause. Début un peu engagé, puis un pas morpho avec peu de pieds à hauteur de l'arbre.

_Le chemin des lames_ (5c)
: En tête à vue. Jolie, un rétablissement un peu délicat à la 4e dégaine.

_Bilame_ (5c)
: En tête à vue. Jolie, assez facile.

_La tentation du biscuit_ (6b+)
: En tête à vue. Début facile par la gauche, puis traversée g->d en inverse et sortie exigeante.

_Le miroir de l'eau_ (7a+)
: En moulinette, plusieurs pauses. Première moitié facile (6a+ ?) et très joli sur prises évidentes. Toutes les difficultés sont concentrées dans le haut : quelques gros mouvements, deux enchaînements délicats sur prises fines. Globalement abordable après travail ?

---

## 2021-11-12 Presles (Presles)

Avec Alexandre CHARRA.

Voie pour la liste de courses du moniteur grandes voies (TD, 200m).

### Secteur _Fond du cirque_

_Absolue_ L1 (6a)
: En moulinette.

_Absolue_ L2 (5c+)
: En tête à vue.

_Absolue_ L3 (6a)
: En moulinette.

_Absolue_ L4 (6b)
: En tête, une chute. Passage du surplomb délicat, peu de pieds.

_Absolue_ L5 (6b)
: En moulinette. Bombé en 6b, peu de pieds mais des bonnes mains à droite.

_Absolue_ L6 (6a)
: En tête, A0. Cheminée en coincement, puis traversée fine, passée en A0.

_Absolue_ L7 (4)
: En moulinette.

Horaire : 9h50 parking du gite du Charmeil, 11h05 pied de la voie, 11h30 attaque, 16h30 sortie.

---

## 2021-11-07 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Théo LAMBERT, Grégoire NONOCHIAN, Sylvaine VALETTE, Nordine ARAR, Claire SELLIER, Yann BERLEMONT, Pascale RENAUDET, Marie-Aimée DURANT, Marion BRÉGAND, Victoria KAREL, Garance REY et Ronald RADU.

### Secteur _K Net_

_6 K nette_ (6a)
: En tête.

_Quarantaine rugissante_ (5b)
: En tête à vue.

_Soleil immonde_ (6b)
: En tête.

_Harry fait du zèle_ droite (6c)
: En tête.

_Harry fait du zèle_ gauche (6c)
: En moulinette.

_Flicophobie_ (6c)
: En tête avec une pause, puis travail en moulinette.

---

## 2021-10-24 Falaise d'Anse (Omblèze)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Théo LAMBERT, Pascale RENAUDET, Victoria KAREL, Nicole HAXAIRE, Muriel GRASSET, Claire SELLIER et Jacques CLAIR.

### Secteur _Coeur à carreau_
_Melting Potes_ (4c)
: En tête à vue. Très facile, bien comme première voie en tête pour les débutants.

_La volte des Vertugadins_ (6a)
: En tête à vue. Plutôt 5c, pas intéressante.

### Secteur _Lutin Malin_

_Harricana_ (5a)
: En tête.

---

## 2021-10-17 La Ceyte (Saou)

Organisé par CAF Sylvaine VALETTE, en remplacement d'une journée d'initiation GV prévue par Grégoire NONOCHIAN.  
Avec Alexandre CHARRA, Florence GRINANT, Élina REYNAUD et Olivier ANGONIN.

### Secteur _Soleil_

_Christophe_ (5c)
: En tête à vue. Très jolie voie ! Un piège sous le relais, le reste facile.

_Viol collectif de 18h01_ (6a)
: En tête à vue. Belle voie toute en finesse, crux en haut.

_Hold Tight_ (6a+)
: En tête. Belle voie, la difficulté réside dans l'engagement et le cheminement. Prévoir friends pour ajouter un point au début et éventuellement dans le haut.

_La fabuleuse histoire de Mr Septembre Rose_ L1 (6a)
: En tête à vue. Facile, le 6a se situe dans la 2e moitié (dalle), 35m.

_La fabuleuse histoire de Mr Septembre Rose_ L2 (6a)
: En tête, une chute au début. Un surplomb à négocier bien à gauche, puis facile, 20m.

### Secteur _Tête en l'air_

_Je suis qu'un grain de poussière, de fil en aiguille_ (6a+)
: En tête à vue. Première longueur d'une grande voie facile, bien équipée, quelques pas demandent un peu d'attention. Corde de 80m un peu juste.

---

## 2021-10-16 Falaise d'Anse (Omblèze)

Avec Christophe HARLEZ.

### Secteur _Mandarine_

_Triphospattes_ (5c)
: En tête à vue. Courte, mais pas tout à fait triviale.

_Adénosine_ (6a+)
: En tête, une pause. Lecture peu évidente, voie continue sur des prises fines et sculptées. Pas forcément à refaire.

_La bande à Velcrot_ (6a+)
: En tête à vue. Jolie longueur ! Crux à mi-longueur mentalement exigeant, puis facile.

_Clémentine_ (6b)
: En tête à vue. Quelques mouvements physiques pour passer le surplomb, le reste 5c+/6a. Voie magnifique sur un calcaire orangé, malheureusement victime de son succès (patine).

### Secteur _Riette Beurre_

_Le géant de papier_ (6b+)
: En tête, 4 pauses. Très belle longueur, alternant entre dalle et passages légèrement déversants. De bonnes prises assez évidentes tout du long. Un 5c+/6a avec trois passages 6a+/6b : la dalle du bas (fine), le surplomb (athlétique) et la sortie (délicate et mentale).

---

## 2021-10-03 La Graville (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Grégoire NONOCHIAN, Sylvaine VALETTE, Nicolas MOURIN, Nordine ARAR, Nicole HAXAIRE, Pascale RENAUDET, Claire SELLIER, Florence GRINANT et Lorraine LETRANCHANT.

_L'escalope et la salade_ (6a+)
: En tête à vue. Un gros réta puis facile. Sortie à droite.

_Dune_ (4c)
: En tête.

_Que dalle_ (6a)
: En tête à vue. Magnifique !

_Denise_ (5c+)
: En tête à vue. Physique avec un crux à mi-longueur, puis plus fin.

_La Blaconnaise_ (5c)
: En tête à vue. Rester concentré pour la dalle de sortie.

_Plaisir du soir_ (5c)
: En tête à vue. Très jolie et abordable.

_Alertez les bébés_ (5c)
: En tête à vue. Un gros surplomb à prendre par la droite, le reste facile.

---

## 2021-10-02 Falaise d'Eson (Pont-de-Barret)

Avec Christophe HARLEZ.

### Secteur _Tapis Volant_

_Le phaco, la chauve-souris et le Kronenbourg_ (6a)
: En tête. Jolie, mais pas si facile.

_Nostalgie du désert_ (6c)
: En moulinette, 2/3 de la voie.

### Secteur _Les fesses de J.F._

_Le bal des casses couilles_ (5c+)
: En tête.

_Gratos pour les blondes_ (6a+)
: En tête à vue.

_Les fesses de J.F._ (5c+)
: En tête à vue.

### Secteur _Les rois de la bartasse_

_Le pilard_ (6a)
: En tête.

### Secteur _L'art_

_Art Assez !_ (6b+)
: En tête, 4 pauses. Voie continue, beaucoup de mental et assez physique.

---

## 2021-09-19 Falaise d'Eson (Pont-de-Barret)

Avec Christophe HARLEZ.

### Secteur _Sécateur et Pied de Biche_

_Putain de caillasse_ (5b+)
: En tête à vue. Facile mais déjà physique.

_Oh ! Oh ! Oh ! Joli coco !_ (6a+)
: En tête, 1 pause. Début en 5b/c, puis 6a+ physique. Un piège juste sous le relais.

_La solitude de l'équipeur_ (6b)
: En tête, 1 pause. Début 5c, puis 6b(+). Traversée d>g un peu technique en haut de voie (inversée sous la dégaine à prendre MG).

### Secteur _Papa 6b_

_On m'appelle Papa 6b_ (6b)
: En tête, 1 pause. Magnifique voie physique et homogène. Un rétablissement peu évident en haut de voie, sans trop bonnes mains.

---

## 2021-09-12 Mer de glace (Villevocance)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE et Jean-Mathieu MONTMAGNON.

### Secteur _Grotte_
_Au plaisir des dames_ (5b)
: En tête.

_Faut qu'on_ (5b+)
: En tête.

_La grande Duduche_ (6b)
: En tête à vue. Deux pas blocs, un au début (traverser à droite) et le plus difficile en haut.

_La Jeanne_ (5c)
: En tête. Sympathique et homogène.

### Secteur _La boîte aux lettres_

_La boîte aux lettres_ (5a)
: En tête à vue. Facile en dépit des apparences.

_Que dalle_ (6a+)
: En tête à vue. C'est fin... très fin même !

---

## 2021-08-28 Tourniol (Barbières)

Avec Christophe HARLEZ.

Rocher moyen (mauvais en bas), équipement espacé. La grimpe est très exigeante, physiquement mais surtout mentalement.

Voie pour la liste de courses du moniteur grandes voies (TD+, 150m).

_Ce Bel Été_ L1 (6a)
: En moulinette. Un bombé assez physique. Attention à toutes les prises !

_Ce Bel Été_ L2 (6b)
: En tête, A0. Un départ en dalle déversante très physique (A0), puis une vire ascendante toute en finesse. Relais caché au pied d'une cheminée.

_Le Dièdre_ L3 (5c)
: En moulinette. Longueur de transition, on rejoint la voie du Dièdre. Le rocher change du tout au tout, il devient plus patiné mais moins péteux.

_Le Dièdre_ L4+L5 (4c, 5c)
: En tête à vue. Longueur à l'ancienne, parfois végétale, pitons assez espacés.

_Le Dièdre_ L6 (6b)
: En moulinette, A0. Remonter la fin de la cheminée (un pas 6a), puis bifurquer sur la dalle à gauche (un pas 6a). La traversée vers la gauche dans la dalle est cosmique : pas de prise de main, et un bombé tout en adhérence pour les pieds. On passe à tire-clou, ce n'est pas du 6b...

_Le Dièdre_ L7 (3a)
: En tête à vue. Deux pitons et puis sortis !

Horaire : 4h dans la voie.

---

## 2021-08-25 La Goule (Châteaubourg)

Avec Christophe HARLEZ, en soirée.

_Le dièdre_ (5a)
: En tête. Ça ne rentre décidément pas...

_Zéro à la question_ droite (6b+)
: En tête, D2 pas propre. Magnifique ! Un enchaînement à trouver pour le début : inversée MD, diagonale à droite de D2 MG, puis bloc épaule MD et MG bonne prise dans la fissure.

_La petite couenne_ (6a)
: En tête, enchaînée. Passage entre D1 et D2 en dynamique morpho : MG sur l'écaille à gauche, MD sur le cube, puis MG dans la fissure de sortie (nettoyée !).

---

## 2021-08-15 Rochers de Gonson (Rencurel)

Avec Théo LAMBERT.

Sympathique secteur de grandes voies faciles, grimpe majoritairement en dalle avec grosses prises éloignées et beaucoup d'adhérences de pieds.

Le secteur compte 5 voies seulement, mieux vaut être seuls ! Les voies sont bien protégées, plaquettes abondantes (jusqu'à 18 en 45m !).

_D'un extrême à l'autre_ L1 (5b)
: En moulinette.

_D'un extrême à l'autre_ L2 (5c+)
: En tête à vue. Un début en dalle déversante, puis très vite une cheminée exigeante.

_D'un extrême à l'autre_ L3+L4 (5b)
: En moulinette.

_D'un extrême à l'autre_ L5 (5b)
: En tête à vue.

_Armagonson_ L3 (5c)
: En moulinette.

_Armagonson_ L4+L5 (5b+)
: En tête à vue. Joli enchaînement.

---

## 2021-08-02 Presles (Presles)

Avec Christophe HARLEZ.

### Secteur _Éliane_

_Voix d'Éliane_ L1 (5c)
: En tête, A0.

_Voix d'Éliane_ L2 (5c)
: En moulinette.

_Voix d'Éliane_ L3 (5b+)
: En tête, A0.

_Voix d'Éliane_ L4 (6a)
: En moulinette.

_Voix d'Éliane_ L5 (5b)
: En tête.

_Voix d'Éliane_ L6 (2)
: En moulinette.

_Voix d'Éliane_ L7 (5b)
: En tête.

_Voix d'Éliane_ L8 (5c)
: En moulinette.

---

## 2021-07-23 La Goule (Châteaubourg)

Avec Christophe HARLEZ, en soirée.

_Le dièdre_ (5a)
: En tête. Bienvenue dans une nouvelle dimension : la Goule.

_Gâteau réssucité_ (5c+)
: En tête. Début sur gros trous, puis un crux dont la méthode m'échappe à chaque fois : rester dans la dalle de droite, sans essayer d'utiliser la fissure.

_Zéro à la question_ droite (6b+)
: En tête, D2 pas propre. Magnifique !

_Barquette 3 grattons_ (6a+)
: En tête, une pause. Le bas est toujours aussi physique et le haut toujours aussi mental...

---

## 2021-07-20 La Payre (Le Pouzin)

Avec Christophe HARLEZ, en soirée.

_Goût d'aphteur noun'_ (6a)
: En tête, enchaînée au 2e essai. Jolie mais fine, ça met dans l'ambiance !

_Putain d'exam_ (6a+)
: En tête à vue. Traversée physique vers la gauche, puis garder les idées claires pour une sortie à bien lire.

_À la soupe_ (6a+)
: En tête, A0. Infaisable jusqu'à D3, puis sortie à gauche facile ? Ou bien on n'a rien compris...

---

## 2021-07-07 La Goule (Châteaubourg)

Avec Christophe HARLEZ, en soirée.

_Le dièdre_ (5a)
: En tête. Bienvenue dans une nouvelle dimension : la Goule.

_Le retour du comcombre_ droite (6b+)
: En tête à vue. Miraculeusement enchaînée, une bonne dose d'aléatoire en fin de voie !

_Fin de série_ (6b)
: En tête, enchaînée. Sortie par la gauche, pas dans la voie.

_La petite couenne_ (6a)
: En moulinette, travail. Un abominable enchaînement bloc entre D1 et D2.

---

## 2021-07-02 La Goule (Châteaubourg)

Avec Christophe HARLEZ, en soirée.

_Le dièdre_ (5a)
: En tête. Bienvenue dans une nouvelle dimension : la Goule.

_Gâteau ressucité_ (6a)
: En moulinette, travail. Un crux teigneux.

_Speedy Gonzales_ (6c+)
: En tête, travail. Les points sont loins ! Relais non-chaîné.

---

## 2021-06-16 La Goule (Châteaubourg)

Avec Christophe HARLEZ, en soirée.

_Le dièdre_ (5a)
: En tête. Bienvenue dans une nouvelle dimension : la Goule.

_Barquette 3 grattons_ (6a+)
: En tête, une pause. Départ physique, le reste peu évident.

_Zéro à la question_ droite (6b+)
: En tête, une pause. Départ à court-circuiter par la droite, puis aller dans la grotte et sortir en traversant à droite.

---

## 2021-06-05 Buoux (Buoux)

Organisé par Patrice DELHOUME.  
Avec Gaelle GOURDOL, Lorraine LETRANCHANT, Isabelle VILBERT, Jean-Marc et Gaétan MOURGUE, Jean-Louis DIGON, Alexandre CHARRAS, Thierry STOULS.

### Secteur _Pubis_

_La confiture aux cochons_ (5c)
: En tête à vue. Magnifique, homogène.

_Porcinet_ (5b)
: En tête à vue. Belle, un petit bombé à passer puis facile.

_Grande Veine Bleue_ (6a)
: En tête flash. Physique, léger dévers sur bonnes prises. Une prise peu évidente à trouver pour la main gauche...

_Poeme givré_ (5c, 6b)
: En moulinette, travail. Incompréhensible.

_Bagdad Café_ (6b)
: En moulinette, travail. Très difficile de décoller ! De plus en plus facile vers le haut, mais cheminement assez dur à lire.

_L'Utérus_ (6c)
: En moulinette, travail. Un A/R dans la 7a à droite très morpho, le reste facile.

### Secteur _Mur de Zappa_

_Bal des lazes_ (6a)
: En tête à vue. Belle dalle, toute en finesse.

### Secteur _Excalibur_

_Le pilier déglingué_ (5c)
: En tête à vue. Facile malgré un départ physique.

_La fakir_ (6a+)
: En moulinette. Une fissure un peu patinée et poisseuse.

_Bleu tomate_ (6b)
: En moulinette, travail x2. Un passage sur bombé avec des bigoigts assez difficile (prendre plutôt sur la droite), le reste facile.

### Secteur _Scorpion_

_Imitation granit_ (6a)
: En tête à vue. Un mouvement assez physique en Dülfer au milieu, rester lucide pour la sortie ! Traverser à gauche sur une dégaine, puis droit vers le haut (ne pas prendre le relais de gauche).

---

## 2021-05-13 Mur du Son (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Jean-Mathieu MONTMAGNON et Marianne LACOSTE.

_Ultrason_ (6a+)
: En tête.

_Le cri du papillon_ (6a)
: En tête.

_Crescendo_ (5b)
: En tête.

_Boum!_ (6a+)
: En tête.

_La vie en rose_ (6a+)
: En tête à vue. Très jolie, pas de difficulté.

_ÔM!_ (6a+)
: En tête, non-enchaînée. Sympathique, mais à faire par temps sec : une résurgence sur le passage clé...

---

## 2021-05-09 La Graville (Saou)

Avec Julie DZIALOSZYNSKI.

_Belu_ (3b)
: En tête à vue.

_Walkyrie_ (5b)
: En tête.

_Muguet_ (5b)
: En tête à vue. C'est très cher côté !

_Cayenne_ (5c)
: En tête à vue. Un pas à la sortie.

_La Vèbre_ (5b)
: En tête.

_Furieuse_ (5c)
: En tête. Fissure évidente, enchaînement de pas blocs. En croyant faire _10 ans déjà_ mais qui traverse plus à droite.

---

## 2021-05-08 Sucettes de Borne (Glandage)

Organisé par CAF Théo LAMBERT.  
Avec Diane TOURET, Jean-Mathieu MONTMAGNON.

Magnifique site au fond du département de la Drôme, très isolé. Venir idéalement pour deux jours, camping sauvage ou gite au village de Borne. Attention, les cotations sont parfois sévères...

### Secteur 1 : _Moaï_

_Voie normale_ L1 (5a)
: En moulinette. Facile, mais attention au tirage.

_Voie normale_ L2 (5b)
: En tête à vue. Très aérien, sur le fil de l'arête. Si la corde est assez longue, on peut sauter R2.

_Voie normale_ L3 (5b+)
: En tête. Traversée sans difficulté.

_Voie normale_ L4 (5b)
: En moulinette. Surprenante mais facile.

Descente en rappel dans _Tu me prends de court_ et _Popo Star_ (7a...)

### Secteur 4 : _Le Rasoir_

_Esprit tordu_ L1 (5a)
: En moulinette.

_Esprit tordu_ L2 (6a)
: En tête à vue. Un pas.

_Babaorum_ (5c)
: En tête à vue.

_Panoramix_ (6a)
: En moulinette. Pas évidente du tout, il faut de la lecture et du mental !

---

## 2021-04-10 Roche Pertus (Cornas)

Avec Julie DZIALOSZYNSKI.

### Secteur _Nord_

_Bébé Requin_ (4c)
: En tête à vue. Cheminée facile.

_La tentation de St-Meumeu_ (5a)
: En tête. Sortie par la droite.

_Protection rapprochée_ (4c, 5c)
: En tête.

_Brie comte Robert_ (5b+)
: En tête.

_L'entrejambe_ (5a+)
: En tête après travail. Toute la difficulté est au début, bien à gauche puis bien à droite.

_Am stram gram_ (5c+)
: En moulinette.

### Secteur _Sud_

_La promenade du fil à plomb_ (4c)
: En tête à vue. Pas de relais.

Voie non-nommée à gauche de _La promenade du fil à plomb_ (4c)
: En tête à vue. Pas de point.

---

## 2021-04-02 La Graville (Saou)

Avec Sylvaine VALETTE.

_Utopia_ (4b)
: En tête.

_Dune_ (4c)
: En tête.

_Arrakis_ (4c)
: En tête à vue. Un bon 5 si on reste bien au milieu dans la dalle, sans utiliser la fissure de droite.

_Crispinette_ (5b)
: En tête à vue.

_La fissure_ (5c)
: En tête. Un peu engagée.

_Méphistophélès_ (6a+)
: En tête à vue. Une dalle bien lisse à contourner par la gauche.

_Tilt_ (6a)
: En tête à vue. Cheminement peu évident dans la partie supérieure.

_Les potes à Perrault et à Grisly_ (5c+)
: En tête à vue. Bloc !

---

## 2021-03-28 Falaise d'Anse (Omblèze)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Marianne LACOSTE, Marion BREGAND, Jacklyn et Denis MAISONNEUVE, Ugo COTEAUX, Hélène ZAJACZKOWSKI, Marie-Aimée DURAND, Jean TROUILLER, Fatima MASSOUD, ...

### Secteur _Lutin Malin_

_Au bord du trou_ (4c)
: En tête à vue. Pas si facile.

_Harricana_ (5a)
: En tête.

_Mélodie Nelson_ L1+L2 (6a)
: En tête.

---

## 2021-03-21 Gorges de Saint-Moirans (Chastel-Arnaud)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Grégoire NONOCHIAN, Fatima MASSOUD, Fatima ATIGUI, Nordine ARAR, Alexandre CHARRA, Yann BERLEMONT.

### Secteur _Dent du Cafiste_

_Dansez !_ (5b)
: En tête au 2e essai. Début physique, puis dalle toute en finesse... dure dure comme voie de chauffe !

_Faim du jour_ (6a)
: En tête à vue. Un vrai 6a continu et exigeant tout du long, mais c'est magnifique !

_L'Oeuvre au noir_ L1+L2 (6a+)
: En moulinette. Un pas physique pour passer le toit.

---

## 2021-03-13 Les Rangs de Mars - Suzette Flipo et Ricky Banlieue (Pont-de-Barret)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Florance GRINAND, Sylvaine VALETTE, Jérémy LOMBARD, Grégoire NONOCHIAN.

_OK canaill_ (5b)
: En tête à vue. Bien pour commencer.

_Boum_ (5b)
: En tête à vue. Délicat et exposé au départ.

_Sister Moon_ (6a)
: En tête. Un pas très physique au milieu.

_Fragile_ (6a)
: En tête. Jolie et homogène.

_Signes de vie_ (6b)
: En tête à vue. Facile.

_Evans_ (7a+)
: Travail en moulinette. Toujours aussi explosive.

---

## 2021-02-21 La Ceyte (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Théo LAMBERT, Fatima ATIGUI.

### Secteur _Soleil_

_Check-up total_ (5b)
: En tête à vue.

_Objectif terre_ (5c)
: En tête à vue. Toute en traversée.

_Quoi de neuf docteur_ (6a)
: En tête à vue. Quelques pas fins.

_Hold tight_ (6a+)
: En tête à vue. Une belle voie, avec des bonnes prises tout du long. La difficulté est dans la recherche de l'itinéraire.

_Pars_ (5b)
: En tête à vue. Magnifique et facile, mais il faut un rappel 2x50m.

_Le pilier du soleil_ L1+L2 (5c)
: En tête à vue, corde tendue. Une unique longueur de 65m, grandiose ! Descente en rappel (2x50m).

Attention aux chutes de pierre dans les rappels du _Pilier du soleil_ !

---

## 2021-02-20 Aiguille de la Tour (Saou)

Avec Théo LAMBERT.

_Arête Sud_ L1 (3b)
: Suivre un cable fixe.

_Arête Sud_ L2 (5a)
: En moulinette. Cheminée équipée.

_Arête Sud_ L3 (5b)
: En tête, équipée.

_Arête Sud_ L4 (5b)
: En moulinette. TA.

_Arête Sud_ L5 (5c)
: En moulinette. TA.

_Arête Sud_ L6 (5c)
: En tête à vue, magnifique dalle à réglettes.

_Arête Sud_ L7 (2)
: En moulinette. Rampe ascendante et traversée difficile à protéger.

_Arête Sud_ L8 (4c)
: En moulinette. Sortie dans du rocher délité envahi par la végétation.

_Arête Sud_ L9 (3-4)
: Course d'arête jusqu'au sommet.

Descente : 3 rappels végétatifs.

---

## 2021-02-06 Rocher des Abeilles (Soyans)

Organisé par Jean-Louis DIGON.  
Avec Yann BERLEMONT.

_Moniebah_ (5c)
: En tête.

_Mosaïque_ (5c)
: En tête. Attention, sortie à gauche sur le relais de _Ambiguë Amitié_.

_Ambiguë Amitié_ (6a)
: En tête.

_Le laminoir de feu_ (6a+)
: En moulinette.

_Scorpions rebelles_ (5c)
: En tête.

_Le relief de la vie_ (6c)
: En moulinette. Un enchaînement dans la dalle à bien connaître.

---

## 2020-12-16 Falaise du Chat Gourmand (Rompon)

Avec Jérémy LOMBARD et Clément PICHON.

### Secteur _Face Sud_

_Jamais deux sans toi_ (5c)
: En tête à vue. Déroutante, bombé de sortie à prendre par la droite.

_Aimer est un verbe irréfléchi_ (5c)
: En tête à vue. Début par la gauche, fin peu évidente.

### Secteur _Face Ouest_

_Ravie sente_ (5a)
: En tête à vue.

_Une grande histoire d'amour_ (6a)
: En tête, A0. Attention, ça parpine beaucoup ! Il est courant d'avoir les prises en main.

_12°5 avant la sieste_ (6a)
: En tête à vue. Facile, jolie et courte.

_Marri, on l'est_ (6a)
: En tête, A0. Une belle voie toute en traversée, le début est instable (à droite) et difficile à protéger.

---

## 2020-12-05 Falaise du Chat Gourmand (Rompon)

Avec Jérémy LOMBARD, Clément PICHON, Gabriel ULLIEL, Nordine ARAR, Lorraine LETRANCHANT et Arnaud DE DINECHIN.

Voies techniques et déroutantes dans un grand et étonnant lapiaz calcaire d'une vingtaine de mètres de haut. Attention aux cotations, soyez modestes !

### Secteur _L'Ours_

_Les colles des fans_ II, III, V et VII (5a-5c)
: En tête à vue. Des voies d'initiation, pas si faciles que ça.

_Quelle face prendre_ (5b)
: En tête à vue. Dièdre morpho.

_Pacific Street_ (5c)
: En tête flash. Très jolie arrête, départ fin sur la droite (quelques prises à bien repérer) ou bourrin dans la fissure à gauche. Sortie en Dülfer.

_La peau de l'ours noir_ (6a+)
: En tête, pas enchaîné. Début difficile.

_Blanche main et les 7 voies_ (7a)
: En moulinette, travail. Plusieurs mouvements très difficiles et physiques.

---

## 2020-11-28 Roche Pertus (Cornas)

Avec Jérémy LOMBARD et Clément PICHON.

### Secteur _Nord_

_Cool Ibarra_ (6a)
: En tête. Il faut bien lire et ne pas paniquer, surtout à la reprise :-).

_La petite dalle_ (5c)
: En tête. Facile.

_No comment_ (5b)
: En tête. Facile sur l'arête.

_A l'ombre du concombre_ (6b+)
: En moulinette, quasi enchaînée. Sur la traversée d->g de fin, la verticale est à prendre main gauche pour pouvoir croiser main droite sur une goutte d'eau juste au-dessus.

_Persiste et signe_ (7a)
: En moulinette, travail. Un début facile (6b max) jusqu'au premier point, puis une succession de prises et de placements très difficiles, pas réussis. Sortie facile.

---

## 2020-10-25 Falaise d'Anse (Omblèze)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Émilie SEVERAC, Muriel GRASSET, Mariane LACOSTE, Julien SAISON, François MIALHE, Marion BONACORSI, Stéphane BAUDE, Fatima ATIGUI, Fatima MASSOUD.

### Secteur _Lutin Malin_

_Pour Cloclot_ (4c)
: En tête.

_Mélodie Nelson_ L1+L2 (6a)
: En tête.

### Secteur _Tutévu_

_L'aphone des falaises_ L1 (6a)
: En tête.

_Nage Indienne_ L1+L2 (6b+)
: En tête. C'est fin mais ça s'enchaîne bien !

---

## 2020-10-17 Orgon Le Canal (Orgon)

Organisé par Patrice DELHOUME.  
Avec Gaelle MEUZARD, Thierry & Clément STOULS, Jean-Marc et Gaétan MOURGUE, Jean-Louis DIGON, Sylvaine VALETTE, Michel ROBERT, Lorraine LETRANCHANT, Isabelle VILBERT, Alexandre CHARRAS.

### Secteur _Sous les arbres_

Non nommée n°1 (5c+)
: En tête à vue. Voie en surplomb dans un dièdre, bonnes prises tout du long

_Power plate_ (6b+)
: En tête après 2 essais en moulinette. Voie magnifique mais continue et exigeante. Une rampe à remonter de gauche à droite, pour aller chercher une pincette à côté de la dégaine main droite, puis traversée du bombé vers la droite en adhérence de pieds.

_Sergent la varice_ (6a+)
: En tête après un essai en moulinette. Voie continue. Prendre bien à droite au départ avant de revenir dans la voie à la 4e dégaine.

_La fourmilière s'agite_ (5a)
: En tête à vue.

_Bon pied bon oeil_ (5b)
: En tête à vue.

_Bac et alors ?_ (5c)
: En tête à vue.

_La sauce piquante_ (5c+)
: En tête à vue. Pas bloc avec mousquetonnages délicats dans le bas de la voie, le reste facile.

_Test tube baby_ (6a)
: En tête. Morpho.

Non nommée n°14 (6a+)
: En tête à vue.

Non nommée n°15 (5c)
: En tête à vue.

---

## 2020-10-11 Roc de Tras Castel (St-Jean-de-Buèges)

Organisé par CAF Daniel CONSTANT.  
Avec Jean-Mathieu MONTMAGNON, Jérémy LOMBARD, Alain SALA, Henry-Louis PENANT, Binh NGUYEN, Marion BONACORSI.

### Secteur _Pilier du Château_
_La fosse aux serments_ L1 (5b)
: En tête. Un pas de 6a avant le relais.

_La fosse aux serments_ L2 (5c)
: En tête. Quelques pas de 6a, il s'agit en fait de L2+L3 de _L'ombre du chasseur_.

### Secteur _Face Sud_

Très jolie grimpe, en dalle majoritairement. Calcaire gréseux qui ne patine pas beaucoup.

_Xénon le xénophobe_ (5c)
: En tête.

_Oh, les beaux jours_ (6a)
: En moulinette. Un pas assez fin.

---

## 2020-10-10 Thaurac - Site de la Falaise de la Grotte (St-Bauzille-de-Putois)

Organisé par CAF Daniel CONSTANT.  
Avec Jean-Mathieu MONTMAGNON, Jérémy LOMBARD, Alain SALA, Henry-Louis PENANT, Binh NGUYEN, Marion BONACORSI.

### Secteur _Bisous dans le cou_

_Voie du Tournesol_ L1 (5b)
: En tête.

_Voie du Tournesol_ L2 (4b)
: En moulinette.

_Voie du Tournesol_ L3 (6a)
: En tête. Sortie à gauche de la voie officielle, sur la plus à droite des 3 voies équipées de plaquettes.

_Voie de l'Instituteur_ L1 (5c)
: En tête. Un pas au début, le reste 5a.

_Voie de l'Instituteur_ L2 (5c)
: En tête. Belle longueur homogène et continue.

---

## 2020-10-08 Falaise d'Anse (Omblèze)

Avec Jérémy LOMBARD et Clément PICHON.

### Secteur _Dopage_

_Monsieur Propre_ L1+L2 (5b, 6a)
: En tête.

_Le nom de Dieu_ (6a)
: En tête.

_Stratopause_ (6b)
: En tête à vue. Traversée fine vers la droite à la 4e dégaine, puis passage conti entre 5e et 9e dégaine, prendre bien le temps sur les repos.

_Virage Dangereux_ (6b)
: En tête, plusieurs repos. Début facile, crux très fin et psychologique sous le relais.

### Secteur _Psychanalyse_

_Lâche toi_ (6b)
: En tête, 3 repos. Début physique et déversant, puis de plus en plus facile. Dans le style du secteur _Nuit Blanche_.

_Les Frères Terribles_ (6a+)
: En moulinette.

---

## 2020-10-04 Roche Pertus (Cornas)

Avec Jérémy LOMBARD.

### Secteur _Nord_

_Protection rapprochée_ (5c)
: En tête. Le crux est en haut.

_Numéro 13_ (6b)
: En moulinette, travail. Début physiquement impossible, milieu facile, puis fin jolie et ample.

_À l'ombre du concombre_ (6b+)
: En moulinette, travail. Un beau projet, départ bien à gauche et des prises obligatoires tout le long.

_No comment_ (5b)
: En tête. Facile sur l'arrête.

_La petite dalle_ (5c)
: En tête à vue. Facile.

_Cool Ibarra_ (6a)
: En tête, une pause. Gros mouvements.

---

## 2020-09-27 Traversée des Dentelles de Montmirail (Gigondas)

Organisé par CAF Théo LAMBERT.  
Avec Nancy MARION, Mathieu MOLLARET, Antoine BILLEAU et Jean-Mathieu MONTMAGNON.

### Traversée des _Florets_

Belle escalade, très bien équipée.

_Traversée des Florets_ L1 (5b)
: En tête.

_Traversée des Florets_ L2+L3 (5a, 4c)
: En tête. Belle longueur.

_Traversée des Florets_ L4 (4c)
: En moulinette.

_Traversée des Florets_ L5 (3b)
: En tête.

_Traversée des Florets_ L6 (5b)
: En tête. Belle longueur.

_Traversée des Florets_ L7 (5a)
: En moulinette.

_Traversée des Florets_ L8+L9 (4b, 3b)
: En moulinette.

### Traversée du _Turc_

Course d'arrête moins équipée (parfois TA), moins de grimpe.

_Traversée du Turc_ (4c)
: En tête avec points posés. Montée au sommet du roc du Turc par l'arrête W non équipée.

Réchappe au col de la Voie des Vires par un unique rappel de 50m versant N.

[Topo C2C](https://www.camptocamp.org/routes/425766/fr/dentelles-de-montmirail-chaine-de-gigondas-traversee-florets-turc-pousterle)

---

## 2020-09-19 La Graville (Saou)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Lorraine LETRANCHANT, André BESSON, Nicole HAXAIRE, Loïc CHARRAS, Sixtine BROSSARD-RUFFEY, Muriel GARNIER, Line BOUVIER et Mariane LACOSTE.

_Utopia_ (4b)
: En tête à vue.

_Anticaca_ (6a)
: En moulinette. Pas si facile si on reste bien au milieu dans la dalle.

_Walkyrie_ (5b)
: En tête à vue.

_La Vèbre_ (5b)
: En tête.

---

## 2020-09-05 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Théo LAMBERT.  
Avec Jean-Mathieu MONTMAGNON, Florence GRINANT, Marvin DUTROT, Véronique LOTTIER, Patrick & Noelle LAMBERT.

### Secteur _K Net_

_E=MC2_ (5c)
: En tête.

_X-o_ (5c)
: En tête.

_Cosmos_ (5c)
: En tête.

_E.t III_ (4b)
: En tête.

_Kid_ (5b)
: En tête.

_OCB_ (6a)
: En tête.

_Harry fait du zèle_ (6c)
: En moulinette.

---

## 2020-06-19 Roche Pertus (Cornas)

Organisé par Lorraine LETRANCHANT, en soirée.  
Avec Jean-Mathieu MONTMAGNON, Isabelle VILBERT, Bernard ARNUTI, Émilie SEVERAC.

Voies équipées à l'ancienne avec des points parfois très espacés et mal placés, sur un calcaire à trous assez à très patiné.

[Topo Rothenbusch](http://rothenbusch.free.fr/e07500/sorties/cornas/topocornas.jpg)

### Secteur _Nord_

_La tentation de St Meumeu_ (5a)
: En tête à vue.

_Brie comte Robert_ (5b+)
: En tête à vue.

_Am stram gram_ (5c+)
: En moulinette. Un enchaînement pas évident à trouver dans la dalle du début avec une traversée d->g, le reste facile.

_No comment_ (5b)
: En tête à vue.

---

## 2020-03-15 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Thibaud BACKENSTRASS.  
Avec Sylvaine VALETTE, Bernard ARNUTI, Alexandre CHARRA, Lionel BLANC, Théo LAMBERT.

### Secteur _Les Fourmis_

_Rouge gorge_ (5c)
: En tête à vue.

_Sale matinée_ (5b)
: En tête à vue.

_Porte jaretelles_ (5a)
: En tête à vue.

_La décadence_ (5a)
: En tête à vue.

_Soleil glacé_ (5c+)
: En tête à vue. Dans la dalle, le reste 5a.

_Un pas si dur_ (6a+)
: En tête à vue. Le crux est en bas, la réglette est une main gauche et il y a une verticale main droite.

_C'est con l'quai d'une gare_ (6a+)
: En tête, travail. Le crux est en bas au niveau du réta, une grosse verticale main droite (bonne aux deux tiers) et une petite réglette main gauche à prendre en pince, il faut monter le pied sur la prise évidente sous le toit.

_Riz au bis_ (6a+)
: En tête, une chute. Quelques bonnes fissures verticales, mais il faut les tenir et pas être trop grand. Attention aux vols !

### Secteur _K Net_

_Laura_ (6c)
: En moulinette, travail. Le 6c reste bien dans la dalle, 4 premiers points faciles puis une traversée d->g peu évidente. Possibilité de biscuiter dans le dièdre à gauche, 5c max.

_Cosmos_ (5c)
: En tête. Entraînement au placement de friends (pas évident sans gros modèle !).

---

## 2020-03-08 La Graville/Aiguille de la Tour (Saoû)

Dans le cadre du stage initiateur SNE organisé par le CAF de Lyon (Michel HUSSON).

### La Graville

_Dune_ (4c)
: En tête à vue.

_La fissure_ (5c)
: En tête à vue. Un peu engagée.

### Aiguille de la Tour

_Vitamine A_ L1 (5c)
: En tête à vue. Pas si facile...

---

## 2020-03-07 Rocher des abeilles (Soyans)

Dans le cadre du stage initiateur SNE organisé par le CAF de Lyon (Michel HUSSON).

Grimpe dans le secteur _Demi-lune_.

---

## 2020-03-01 Virieu-le-Grand (Virieu-le-Grand)

Dans le cadre du stage initiateur SNE organisé par le CAF de Lyon (Alain MARDOIAN).

### Secteur _Rémifasol_

_Rando_ (5a)
: En tête à vue. Traversée évidente.

_Terre Promise_ (5c)
: En tête à vue. Une escalade toute en oppositions.

_Gérard et Choupinette_ (6b+)
: En tête, une chute. Une première partie pas évidente légèrement déversante, et une sortie dans une dalle à ne pas rater !

_Fausse Note_ (6a+)
: En moulinette. Étrange voie, on a l'impression de ne jamais être au bon endroit.

---

## 2020-02-29 Roche de la Narse (Argis)

Dans le cadre du stage initiateur SNE organisé par le CAF de Lyon (Alain MARDOIAN).

### Secteur _Initiation_

Voie non-nommée numéro 3 (5+)
: En tête à vue.

### Secteur _La Grotte_

_M'Roc ou l'Envie de Forcer_ (6b)
: En tête à vue. Jolie voie facile.

_Climb Up Narse ou Le dernier vol_ (6a)
: En tête à vue.

_Hold Up ou Unfinished Business_ (7a)
: En tête, une chute. Un 7a commercial, biscuiter bien à gauche en première partie de voie.

---

## 2020-02-22 Falaise des Embruscalles (Claret)

Organisé par Patrice DELHOUME.  
Avec Sylvaine VALETTE, Lorraine LETRANCHANT, Michel ROBERT, Alexandre CHARRAT, Gaelle GOURDOL.

Site globalement très exigeant, grimpe engagée mais pas exposée dans des voies longues, souvent déversantes, mais très belles dès le 6a/6b. Une belle leçon d'humilité : il faut maîtriser le niveau pour réussir à Claret \; d'ailleurs certaines voies sont les plus dures du monde dans leur cotation.

### Secteur _Escalator_

Extrémité gauche de la falaise, des voies plus faciles dans des styles très variés.

_Miss Pascale_ (5c+)
: En tête à vue. Jolie voie variée, pas très dépaysante par rapport à nos habitudes drômoises.

_Martin Luther King_ (6b+)
: En moulinette, 1 repos. Voie sur plats dans un magnifique calcaire jaune, exigeante au début, facile à la fin, peu de repos.

_Escalator en panne_ (5b+)
: En tête à vue. La plus dure du monde, très patinée et peu esthétique, il y a moyen de se faire peur...

_Dièdre non-nommé_ (6a)
: En tête à vue. Un seul pas difficile à l'entrée du dièdre, il faut monter les pieds très haut et y croire... le reste facile, dans un style à l'ancienne.

_Sisyphe_ (6c)
: Enchaînée en tête au 2e essai. Trois pas délicats, mais c'est un 6c donné pour Claret !

### Secteur _King of Bongo_

_Margot_ (6a)
: En tête à vue. Jolie voie, sans difficultés, une fin à trous et à gouttes d'eau.

### Secteur _Anastasie_

_Coït ou Double_ (6a)
: En tête à vue. Une belle voie très exigeante et continue, départ avec un tronc d'arbre.

? <à droite de Coït ou Double> (6b)
: En moulinette. Départ dans la 6a pour ne pas se fatiguer, puis une traversée vers la droite à ne pas louper. Belle longueur !

### Secteur _Soweto_

_Cayenne_ (6a+)
: En moulinette. Un début athlétique, un très bon repos, puis une dalle bien fissurée.

_Red Red Wine_ (6b)
: En tête, avec une chute et sortie flash. Très belle continuité, une belle réglette au milieu d'une dalle grise passe presque inaperçue au milieu de la longueur...

### Secteur _Mascarade_

_The End_ (5c+)
: En tête, 2 premiers points perchés. La plus dure du monde, un début exagérément difficile et patiné, une sortie dans un beau dièdre.

---

## 2020-02-16 Falaise de la Barre - Mur du Son (Saou)

Organisé par Lorraine LETRANCHANT.  
Avec Sébastien TOURNAIRE, Tiphaine FALLIGAN, Marina PINSARD, Gabriel ULLIEL, Géraldine GROSJACQUES.

_Le son du mur_ (5c+)
: En tête à vue.

_Le cri des pierres_ (5c)
: En tête à vue. Jolie.

_Les murmurs ont des oreilles_ (6a)
: En tête à vue. Jolie.

_Fantaisie climatique_ (6a+)
: En tête à vue, avec dégaine panic. Mousquetonnage 5e dégaine délicat, peu de pieds mais une relance sur un bon réglette \; traversée d->g pour la sortie.

_Respire_ (6b-)
: En tête, A0. Traversée ultra-fine à la 3e dégaine, casses de prises, pas en 6b+/6c, le reste facile et peu intéressant.

_Chut_ (6b)
: En tête à vue. Jolie, un court passage bloc pour se rétablir dans la dalle à gauche sur deux réglettes.

_Clak-son_ (6a+)
: En tête à vue. Jolie.

---

## 2020-01-19 Rochers de la Condamine (Saint-Ferréol-Trente-Pas)

Organisé par Jean-Louis DIGON.  
Avec Sylvaine VALETTE, Lorraine LETRANCHANT.

Secteur 1 : _Vol de flèches_

_Géronimo_ (6a)
: En tête. Très beau pilier, un rien exposé.

_Tête en l'air et idée claire_ (6a)
: En tête. Beau dièdre.

_2001_ (6b)
: En tête à vue. Un toit, puis un dièdre assez technique.

_Le monde ne suffit pas_ (6a+)
: En tête à vue. Peu intéressante.

_Confidences verticales_ (5c+)
: En tête à vue. Courte.

_Les bras arc en ciel_ (5a+)
: En tête à vue.

_Voluptée retrouvée_ (6b)
: Travail en moulinette. Magnifique voie, les difficultés sont en haut : prises clés à repérer, parfois très à gauche.

---

## 2019-12-15 Falaise de la Barre - Mur du Son (Saou)

Organisé par CAF Jean-Louis DIGON.  
Avec Isabelle VILBERT, Lorraine LETRANCHANT, Perrine SERRE, Diane TOURET, Alexandre CHARRA, Jean-Marc MOURGUE, Nicole HAXAIRE, Véronique LIOTIER, Nicolas MOURIN, ...

_Crescendo_ (5b)
: En tête.

_Ultrason_ (6a+)
: En tête.

_Saoûpir_ (6a+)
: En tête.

_Décibel_ (6b)
: En tête. Dalle peu évidente au début, passer à gauche, puis surplomb à contourner légèrement par la gauche, puis reste sur gros bacs, physique.

_Écho_ (6b)
: Travail en moulinette. Casse d'une prise dans Onde de chocs qui servait au début de la voie : passer à droite chercher le bac dans _Tsunami Spleen_. 2 options pour la sortie : prises très fines et nombreuses à droite, ou bonnes réglettes à repérer avant sur la gauche.

_Tsunami Spleen_ (6c)
: En tête. Très jolie, facile sauf les 3 premiers points.

_Boum!_ (6a+)
: En moulinette, pieds nus !

---

## 2019-11-16 Beaume Rousse (Buis-les-Baronnies)

Organisé par Patrice DELHOUME.  
Avec Jean-Louis DIGON, Sylvaine VALETTE, Lorraine LETRANCHANT, Isabelle VILBERT.

### Secteur _Le Bouclier_

Beau calcaire gris, mais assez patiné.

_Le dièdre_ (5c)
: En tête. Plutôt 5b, jolie.

_Pleine Lune_ (6a)
: Très patinée.

_Coin Coin_ L1+L2 (5c+)
: En tête.

_Au Bout_ L1 (6a+)
: En tête.

_Couleur d'Ambruns_ (6a+)
: En tête.

_Milky Way_ (5c+)
: En tête.

### Secteur _Face Sud_

Des voies assez difficiles dans un beau calcaire jaune.

_La caraille gauche_ (6b)
: Enchaînée en tête au 2e essai.

### Secteur _Face Est_

_Qualif à la place du kalife_ (6b)
: En tête. Un ancien 6a+, très joli.

_Les milles et une buis_ (6a)
: En tête. Mouillée...

_Les ailes du vizir_ (6c+)
: Enchaînée en moulinette !

---

## 2019-11-10 Les Rangs de Mars - Suzette Flipo et Ricky Banlieue (Pont-de-Barret)

Organisé par Jean-Louis DIGON.  
Site d'escalade "du bas", voies de 20m belles et peu patinées malgré leur âge. Attention aux blocs mouvants dans les voies faciles !

_Vous êtes jolie_ (5a)
: En tête. Escalade à l'ancienne.

_Moldau_ (5c)
: En tête. Intéressant, un peu physique.

_Radio Bagdad_ (5a)
: En tête. Escalade à l'ancienne.

_La vie de ma mère_ (6a)
: En tête. Quelques mouvements à trouver et à enchaîner.

_Fermé pour ne pas mordre_ (6a-)
: En tête. Tout est dans le début.

_Les trous dans le réel_ (5c)
: En tête. Jolie.

_Un espace pour revivre_ (5c)
: En moulinette. Très physique.

_Evans_ (7a+)
: En tête, A0 avec perche. Départ très soutenu.

_Équipée sauvage_ (5a)
: En tête.

---

## 2019-10-27 Glaciaire de Font d'Urle (Bouvante)

Organisé par Patrice DELHOUME.  
Avec Jean-Louis DIGON, Jean-Marc et Gaetan MOURGUE, Isabelle VILBERT...

Voies courtes mais physiques, type bloc.

_Équinoxe_ (5c)
: En tête.

_Duel_ (6b)
: En tête.

_Déchirure_ (6b+)
: En moulinette.

_Délirium_ (6b)
: En tête. Magnifique !

_Sanglante_ (6a+)
: En tête.

_Incantation_ (6c+)
: En moulinette. Un mouvement hyper physique au milieu, le reste passe bien.

_The Wall_ (6c)
: En moulinette. Gros dévers sur gros bacs.

---

## 2019-10-20 La Goule (Châteaubourg)

Organisé par Patrice DELHOUME.  
Avec Sylvaine VALETTE, Lorraine LETRANCHANT, Michel ROBERT.

_Le dièdre_ (5a)
: En tête.

_La petite couenne_ (6a+)
: En tête. Un enchaînement de pas blocs difficiles jusqu'à la 2e dégaine, les topos la donnent à tort en 5c+.

_Fin de série_ (6b)
: En tête, flash.

_Gâteau ressucité_ (5c+)
: En moulinette. une voie originale avec une moitié sur bacs et le haut en dalle.

---

## 2019-10-19 Chaulet (Berrias et Casteljau)

Organisé par Patrice DELHOUME.  
Avec Sylvaine VALETTE, Lorraine LETRANCHANT, Michel ROBERT.

### Secteur _Beaume Escure_, 1ère grotte

Non-nommé sur le mur aval de la grotte, en traversée vers la gauche (6a)
: En moulinette.

### Secteur _Beaume Escure_, 2e grotte

Cheminée au fond à gauche de la grotte (6a)
: En moulinette.

Non-nommé à droite du 6a (5c)
: En tête.

Non-nommé à droite de la 5c (5a?)
: En tête.

Non-nommé tout de suite à gauche de l'entrée, très belles colonettes en haut (5a?)
: En tête.

Non-nommé tout de suite à droite de l'entrée (5b?)
: En moulinette.

---

## 2019-10-05 Aiguilles de Bénévise (Bénévise)

Organisé par CAF Sylvaine VALETTE.

Grande Voie _Pilier de l'Impromptu_, accessible après 1h d'approche au départ de Bénévise. Un spit est visible depuis le pied de la voie. Les relais sont tous visibles au dernier moment.

_Pilier de l'Impromptu_ L1 (6a, 5b)
: En moulinette. Relais sur arbre à gauche ou sur 2 points sanglés à droite, seulement 3 points sur 40m.

_Pilier de l'Impromptu_ L2 (5a)
: En tête. Longueur en traversée vers la gauche, 15m, 3 points.

_Pilier de l'Impromptu_ L3 (5b)
: En tête. Physique mais bien protégée, 35m, 5 points.

_Pilier de l'Impromptu_ L4 (5b)
: En tête. 15m, 2 pitons rapprochés et 1 lunule au début.

Descente en rappel sur les relais de la voie.

---

## 2019-09-29 La Graville (Saou)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Patrick, Noëlle et Théo LAMBERT, Bernard ARNUTI, Baptiste CHEYNEL, Thierry STOULS, Nicole HAXAIRE, ...

_Le plan_ (4b)
: En tête.

_Monalisa_ (5c)
: En tête.

_La Vèbre_ (5b)
: En tête.

_Directe de la Vèbre_ (5c)
: En tête.

_Furieuse_ (5c)
: En tête. Fissure évidente, enchaînement de pas blocs.

_10 ans déjà !_ (6a+)
: En tête. Splendide mais fine et peu évidente, croise _Furieuse_ au départ.

_Variante de Pigeon Vol_ (7b)
: Quelques essais en moulinette, on ne va pas bien haut...

---

## 2019-09-21 Falaise d'Anse (Omblèze)

Organisé par CAF Jean-Louis DIGON.  
Avec Sylvaine VALETTE, Diane TOURET, Florence GRINANT, Nicolas MOURIN, Patrick & Noelle LAMBERT, Julie PERRET, Jean-Rémy CHARENTUS, ...

### Secteur _Lutin Malin_

_Pour Cloclot_ (4c)
: En tête.

_Harricana_ (5a)
: En tête.

_Mélodie Nelson_ L1+L2 (6a)
: En tête.

### Secteur _Tutévu_

_Pâques aux Rabans_ (6b)
: En tête. Facile, ne pas oublier le bac dans la dalle grise main droite.

_L'aphone des falaises_ (6a)
: En tête.

---

## 2019-09-08 Valcroissant (Die)

Organisé par CAF Jean-Louis DIGON.  
Avec Alexandre CHARRA, Bernard ARNUTI, Sylvaine VALETTE, Nordine ARAR, Nicolas MOURIN, Lionel BLANC, Christine.

### Secteur du _Pilier_ (ou de l'éperon)

_Les bucherons de l'espace_ L1 (3b)
: En tête. Plutôt du 5.

_Suivez le guide_ L1 (5c)
: En tête. Ignorer relais non-chaîné à mi-hauteur, 2e moitié plus gazeuse.

_L'éperon_ (6a)
: En tête. Relais non-chaîné, un pas athlétique au début.

### Secteur _La dalle_

_La marque jaune_ (5c)
: En tête.

---

## 2019-08-19 Les Actinidias (Berrias et Casteljau)

Organisé par Patrice DELHOUME.

_Bretelle d'accès_ (5c)
: En tête.

_Le laurier_ (6a+)
: En tête, pauses. Très physique.

_Mutation_ (7a)
: En moulinette, A0, travail. Très jolie et accessible.

_Tout l'effet d'une petite fée_ (7a)
: En moulinette, A0, travail. Assez physique.

Grandes voies dans la grotte au-dessus de _Bretelle d'accès_. Intéressant et ludique (spéléo), 5c/A0 max.

---

## 2019-08-17 Valcroissant (Die)

Organisé par Bernard ARNUTI.  
Avec Jean-Mathieu MONTMAGNON, Perrine SERRE.

### Secteur du _Pilier_ (ou de l'éperon)

_Méli-mélo_ gauche (5c)
: En tête. Ne pas prendre le relais de _Force 6_ mais continuer sur le 2e relais, début peu intéressant.

_Méli-mélo_ droite (3b)
: En tête. Voie terreuse, plutôt en 5 qu'en 3.

_Pimprenelle et Nicolas_ (3b)
: En tête. 2 variantes, trop courtes.

_Les bucherons de l'espace_ L1 (3b)
: En tête. Plutôt du 5.

### Secteur _La dalle_

_Les charentaises à Papy_ (5b)
: En tête.

_Croissant de lune_ (6a+)
: En tête. Vertical sur la fin.

_L'envers du décors_ (5b)
: En tête.

---

## 2019-08-10 Pointe de Palavar (Ailefroide)

Organisé par CAF Sylvaine VALETTE.  
Avec Bernard ARNUTI, Jean-Mathieu MONTMAGNON, Théo LAMBERT, Anne-Claire BENARD.

### Secteur _Les étoiles_

Une belle dalle comme on aime dans ce genre de massif : inclinée à 60-80°, de très bons pieds (mais il faut y croire !) et presque pas de mains. Les voies les plus intéressantes sont situées au milieu de la dalle.

_Cala Luna_ (4a)
: En tête. Début exposé.

_Piste aux étoiles_ (4c)
: En tête.

_Saturne_ (5b)
: En tête. Facile et homogène.

_Mars_ (5b)
: En tête. 4 pts en 5b+ le reste 5a.

_Soleil_ (5c)
: En tête. Inintéressante.

_Sarkostar_ (5c)
: En tête. Magnifique, 2 pts pas évidents au début.

_Rigel_ (5c)
: En moulinette.

_Véga_ (6a)
: En tête à vue. Magnifique, superbe dalle avec bonnes adhérences de pieds.

---

## 2019-08-09 Pointe de Palavar (Ailefroide)

Organisé par CAF Sylvaine VALETTE.  
Avec Bernard ARNUTI, Jean-Mathieu MONTMAGNON.

Objectif de la journée : la grande voie _Little Palavar_. Le départ est dur à trouver : bien partir du pied de la pointe de Palavar (sentier depuis la route, 200m sous le parking de départ randonnées) et remonter en longeant la paroi. On devine un "R" bleu à demi effacé au pied de la voie.

_Little Palavar_ L1 (5c)
: En tête, A0. 2 pas blocs.

_Little Palavar_ L2 (5b)
: En moulinette. Dalle sans mains.

_Little Palavar_ L3 (3c)
: En tête. Traversée vers la gauche.

L'horaire et la chaleur nous font abandonner. Sortie par les rappels de _Fleurs Bleues_ depuis R3.

---

## 2019-08-06 Les Traverses et la Vignette (Les Vigneaux)

Organisé par CAF Sylvaine VALETTE.  
Avec Bernard ARNUTI, Jean-Mathieu MONTMAGNON, Théo LAMBERT, Grégoire NONOCHIAN, Anne-Claire.

### Secteur _Lézaroïde_, face SW

Rocher calcaire plissé très inhabituel, semblable à du bois.

_Aérobic_ (5b)
: En tête.

_Bronzing_ (5b)
: En tête.

_Plein Sud_ (5c)
: En tête.

_Lézaroïde_ (5c)
: En tête.

_Dinosorus_ (5c)
: En tête. Magnifique !

_Délirium direct_ (6b>5c A0)
: En tête, A0.

_Dernière cavale_ (6a)
: En tête.

---

## 2019-08-05 Contreforts de la Draye (Ailefroide)

Organisé par CAF Sylvaine VALETTE.  
Avec Bernard ARNUTI, Jean-Mathieu MONTMAGNON.

_Spit on Cup_ L1 (4b)
: En moulinette.

_Spit on Cup_ L2 (5a, 3c)
: En tête.

_Spit on Cup_ L3 (5b, 4a)
: En moulinette.

_Spit on Cup_ L4 (3a)
: En tête. Ignorer R4, prendre R4bis après 25m de marche.

_Spit on Cup_ L5 (4c, 5b)
: En moulinette. Magnifique.

_Spit on Cup_ L6 (3a, 4c)
: En tête.

_Spit on Cup_ L7 (2c)
: En moulinette.

Descente : vires, puis rappels (10+50m)

---

## 2019-08-04 La Draye (Ailefroide)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Théo LAMBERT, Bernard ARNUTI, Jean-Mathieu MONTMAGNON...

### Secteur _École droite_

_Cool Raoul_ (5b)
: En tête.

_D'la dalle_ (5c)
: En tête.

_L'écaille_ (5b)
: En tête.

_Pourquoi pas_ (5c)
: En tête.

_Directos_ (6a)
: En tête, A0. Un pas A0 au début.

_La dalle à Robin_ (6a+)
: En moulinette.

_La Dülfer_ (5b)
: En tête.

---

## 2019-07-03 Barbières (Barbières)

Organisé par Sylvaine VALETTE.  
Avec Morgan DUMAS.

### Secteur _Les Buis_

_Le père tape dur_ (5c)
: En tête.

_Envole-toit_ L1+L2 (6a)
: En tête.

_La reprise_ L1+L2 (5c)
: En tête.

### Secteur _La Grotte_

_Prend moi sec_ (6a)
: En tête.

---

## 2019-06-19 Barbières (Barbières)

Organisé par Jean-Marc (AKKA, FFME), en soirée.  
Avec Gaëlle MEUZARD, ...

### Secteur _Les Buis_

_Le père tape dur_ (5c)
: En tête.

_Pas de bras, pas de caillette_ (6a)
: En tête. Quelques prises à repérer au début.

_La reprise_ L1+L2 (5c)
: En tête.

### Secteur _La Grotte_

_Seul au monde_ (6b+)
: En tête, A0 sur la traversée, plusieurs repos.

_Prend moi sec_ (6a)
: En tête.

_Lierre et les mordus_ (5b)
: En tête.

---

## 2019-06-05 La Taupinière (Tamée)

Organisé par Jean-Marc (AKKA, FFME), en soirée.  
Avec Gaëlle MEUZARD, ...

Le site a été complété par des voies faciles qui ne figurent pas dans le topo papier, cf. [topo sur Oblyk](https://www.oblyk.org/crags/998/tamee-la-taupiniere/routes).

### Secteur _Bangster_

_Clo clo rico_ (6a+)
: En tête.

_Marie Boulangère_ (6a)
: En tête.

_Le Syndrôme du terrassier_ L1 (6b+)
: En tête après travail. Magnifique pilier !

### Secteur _Mur central_

_Du courant dans l'eau_ L1 (7a+)
: En moulinette, A0+.

---

## 2019-05-26 Pennes-le-Sec (Pennes-le-Sec)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Jean-Mathieu MONTMAGNON, Sylvaine VALETTE, Louis-Michel GREGOIRE & famille, Antoine BILLAU, Diane TOURNET, Bernard ARNUTI, Gaelle MEUZARD...

### Secteur _Initiation_

Les L3 sont méchantes.

Voie n°7 L1+L2 (4b)
: En tête. C'est la voie la plus à droite.

Voie n°7 L3 (6b)
: En tête A0 avec la perche, puis en moulinette.

### Secteur _La Tour du Pennes_

Secteur caractérisé par des voies avec de nombreuses prises apparentes, mais la plupart sont moins bonnes qu'il n'y parait.

_Suivez le Prof_ (5c)
: En moulinette.

_Le pet de Pennes_ (6a)
: En tête.

_JYC_ (6a+)
: En tête. Assez difficile.

---

## 2019-05-23 La Cime du Mas (La-Chapelle-en-Vercors)

Organisé par Jean-Louis DIGON.  
Avec Patrice DELHOUME.

### Secteur _Gros Bacs_
_Le Merle Bleu_ L1 (5c)
: En tête.

### Secteur _Principal_

_Voie des trous_ (6a+)
: En tête.

_L'échec_ (6b+)
: En tête après travail. Quelques pas peu évidents.

_À la niche_ (6c)
: En tête flash. Très jolie, 2 pieds-mains avec déroulés assez physiques.

_La tentatioin_ (7c+)
: En moulinette. Vraiment hard core...

---

## 2019-05-16 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par Jean-Louis DIGON.  
Avec Patrice DELHOUME.  
Rencontre avec Bernard HOGREL, ouvreur à Rochefort et à Ansage.

### Secteur _Rasoir_
_Minimur Maximum_ (6b)
: En moulinette. Difficile.

### Secteur _Le Bec_

_JC m'a sauvé_ (6a+)
: En tête. Ouverture B. Hogrel, à droite de Éros Ion Positif.

Non nommée à droite de _JC m'a sauvé_ (5b)
: En tête. Ouverture B. Hogrel.

_Affreux Dit Zac_ (7a)
: En moulinette.

_Éros Ion Positif_ (7a)
: En moulinette, travail.

---

## 2019-05-07 Pas de l'Escalier (Tamée (Oriol-en-Royans))

Organisé par CAF Jean-Louis DIGON.  
Avec Patrice DELHOUME.

### Secteur _Sous la route_

_Faut pas boire avant_ (5a)
: En tête.

_Astral voyager_ (5b+)
: En tête. Magnifique.

_Ré-Tamée_ (5c+)
: En tête. Physique mais splendide.

_Fantasia chez les ploucs_ (5c)
: En moulinette. Un pas au début.

_L'absence_ (6b+)
: En tête après travail. Très jolie, mais il faut connaître les mouvements des 4 premiers points par coeur.

_Voie Lactée_ (6c)
: En moulinette. Une traversée g->d très difficile juste après le trou, et un rétablissement délicat avant la sortie.

---

## 2019-05-04 Gorges de la Saint-Beaume (Saint-Montan)

Organisé par CAF Jean-Louis DIGON.  
Avec Sylvaine VALETTE, Grégoire et Nadine NONOCHIAN, Perrine SERRE, Antoine BILLAU, Pauline LESAGE, Gaelle MEUZARD, ...

### Secteur _Le Jardin_

_Sonde d'un jour_ (5c)
: En tête.

### Secteur _Le dièdre_

_Le dièdre de la terreur_ (5b)
: En tête. Intéressante.

_Galactica_ (5b)
: En tête. Intéressante.

_Les plaisanteries les plus courtes_ (3c)
: En tête. Plutôt du 5 ?

_Le picodon fumant_ (6a+)
: En moulinette.

_La petite dalle_ (6a)
: En moulinette.

### Secteur _Central_

_Promenade romantique_ (5b)
: En tête.

_Paturage_ (4b)
: En tête.

---

## 2019-05-01 Falaise d'Anse (Omblèze)

Organisé par Lionel BLANC.  
Avec Jean-Louis DIGON, Isabelle VILBERT, Bruno MAILLARD, Anais PAYEN.

### Secteur _Nuit Blanche_

_Ça débloque_ (5c)
: En tête.

_Nulle part ailleurs_ (6a)
: En tête. Traverser complètement à droite sous le surplomb, les prises de pieds sont là.

_L'arche de Zoé_ (6a+)
: En tête.

_Culture de Lotentics_ (6a+)
: En tête. Magnifique.

_Dust in the Wind_ (6b+)
: En tête, 2 pauses. Rester légèrement sur la droite pour passer la 2è dalle, mais repérer un pied gauche à hauteur du point en avance !

_Ouf je l'ai_ (6c)
: En moulinette. Après la fissure horizontale, continuer tout droit sans se laisser entraîner sur la gauche \; le crux est dans la dalle grise du haut de la voie, 3 points délicats.

---

## 2019-04-27 Carrières de Chomérac (Chomérac)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Jean-Louis DIGON, Lionel BLANC, Perrine SERRE, Antoine BILLAU, Pauline LESAGE, Loïc...

Voies généralement sous côtées sur un rocher calcaire très particulier.

### Secteur _La bascule_

_Amid' mefes_ (5b)
: En tête.

_Marcel ben hur_ (5b)
: En tête.

_Honoré vapété_ (5b)
: En tête. Crux après la 3e dégaîne, exploiter la faille à gauche et l'allonge...

### Secteur _Le Chêne vert_

_Ardécho merveillou pais_ (4b)
: En tête.

_R. 16 for ever_ (5c)
: En tête.

_Mercredi traction_ (6a)
: En tête flash. Pas physique déversant au début, mais les prises sont là.

_Que dalle qui m'aille_ (6a)
: En tête.

---

## 2019-04-14 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Grégoire NONOCHIAN. 
Avec Bernard ARNUTI, Perrine SERRE, Gaëlle MEUZARD, Florence GRINAND, Isabelle VILBERT, Antoine BILLAU, Loïc...

### Secteur _K Net_

_E.t III_ (4b)
: En tête.

_X-o_ (5c)
: En tête.

_Lisse comme un c..._ (6a)
: En tête.

_Kid_ (5b)
: En tête.

_CD 07_ (6a)
: En tête. Un pas bloc au début avec une traversée droite-gauche.

_OCB_ (6a)
: En tête.

_Soleil immonde_ (6b)
: En tête.

_6 K nette_ (6a)
: En tête.

_Le blues de l'ouvreur_ (5c)
: En tête.

_Les terrassiers_ (5a)
: En tête.

---

## 2019-04-06 Gorges de Saint Moirans (Chastel-Arnaud)

Organisé par CAF Jean-Louis DIGON.  
Avec Sylvaine VALETTE, Lorraine LETRANCHANT, Perrine SERRE, Loïc, ...

### Secteur _Initiation_

Voies non-nommées (3c-4c)
: En tête dans les 4 voies de droite.

### Secteur _Tilleul_

_La Tête à Toto_ L1+L2 (5c)
: En tête.

_La ballade à Jimmy_ L1+L2 (5c)
: En moulinette.

_Ah! Urinélla!!_ L1 (5c)
: En tête.

---

## 2019-03-31 Falaise d'Anse (Omblèze)

Organisé par CAF Grégoire NONOCHIAN.  
Avec Sylvaine VALETTE, Nicolas MOURIN, Romain ROLLAND, Jean-Mathieu MONTMAGNON, Lionel BLANC, Sarah ROBIN, Guillaume TABLET, ...

### Secteur _Tutévu_

_L'Train blues_ (5c)
: En tête.

_L'opportuniste_ (5b)
: En tête.

_Pâques au rabans_ (6b)
: En tête. Tout sauf un 6b.

_L'aphone des falaises_ (6a)
: En tête. Magnifique !

_Raining Stones_ L1+L2 (6a)
: En tête.

_Nage indienne_ L1+L2 (6b+)
: En moulinette. C'est continu mais ça s'enchaîne.

---

## 2019-03-30 Falaise d'Anse (Omblèze)

Organisé par Jean-Louis DIGON.  
Avec Lorraine LETRANCHANT.

### Secteur _Nuit Blanche_

_Ça débloque_ (5c)
: En tête. Magnifique caillou.

_Nulle part ailleurs_ (6a)
: En tête flash. Traverser complètement à droite sous le surplomb, les prises de pieds sont là.

_Glucose toujours_ (6a+)
: En tête. Une dalle peu évidente au début.

_L'arche de Zoé_ (6a+)
: En tête.

_Dust in the Wind_ (6b+)
: En moulinette. Une dalle peu évidente, suivie par une seconde à traverser de g à d.

---

## 2019-03-23 Falaise de la Barre - Mur du Son (Saoû)

Organisé par CAF Jean-Michel BROGUIÈRE.  
Avec Florence GRINAND, Anaïs PAYEN, Gaëlle MEUZARD.

_Crescendo_ (5b)
: En tête.

_Le cri du papillon_ (6a)
: En tête.

_Ultrason_ (6a+)
: En tête.

_À voix basse_ (5c+)
: En tête.

_Echo_ (6b)
: En tête.

---

## 2019-03-17 Rocher des Abeilles (Soyans)

Organisé par CAF Sylvaine VALETTE.

_Couleur Café_ (5b)
: En tête.

_Frissons d'avril_ (5b)
: En tête.

_Moniebah_ (5c)
: En tête.

_Ambigue Amitié_ (6a)
: En tête.

_Le drapeau de la colère_ (5c)
: En tête.

_Ma brave dame_ (5b)
: En tête.

_Les deux mains prises_ (5b)
: En tête.

_Origine_ (6a+)
: En tête.

_Le pied de biche en folie_ (6a+)
: En moulinette.

---

## 2019-03-16 Falaise d'Anse (Omblèze)

Organisé par Thibaud BACKENSTRASS.  
Avec Patrick, Noëlle et Théo LAMBERT.

### Secteur _Lutin Malin_

_Pour Cloclo_ (4c)
: En tête.

_Mélodie Nelson_ L1+L2 (6a)
: En tête.

_Harricana_ (5a)
: En tête.

_Lutin Malin_ L1+L2 (6b+)
: En moulinette. Un pas très délicat au milieu de L2.

_La Lune Ulule_ (5a)
: En tête.

### Secteur _Tutévu_

_Shut Your Mouth_ (5b)
: En tête.

_L'train blues_ (5c)
: En tête.

---

## 2019-03-09 Falaise de la Barre - Mur du Son (Saoû)

Organisé par Thibaud BACKENSTRASS.  
Avec Sébastien TOURNAIRE, Julie DZIALOSZYNSKI.

_Tempo_ (5a)
: En tête.

_Crescendo_ (5b)
: En tête.

_Le cri du papillon_ (6a)
: En tête.

_Ultrason_ (6a+)
: En tête.

_À voix basse_ (5c+)
: En tête.

_Saoûffle chaud_ (6a)
: En tête.

_Boum!_ (6a+)
: En tête.

_Saoûpir_ (6a)
: En tête. Rester sous la fissure à droite et revenir après le 3e point.

_Echo_ (6b)
: En tête. 8e dégaine hard core.

---

## 2019-03-02 Falaise d'Eson (Pont-de-Barret)

Organisé par Lorraine LETRANCHANT.  
Avec Sébastien TOURNAIRE, Théo LAMBERT, Julie DZIALOSZYNSKI, Anne-Claire BERNARD.

### Secteur _Les Fesses_

_Alaska Rock Gym_ (5c)
: En tête.

_Le bal des casse-couilles_ (5c+)
: En tête.

_L'étoile du berger_ (5c+)
: En tête.

### Secteur _Les rois de la bartasse_

_Dura lex Silex_ (5c)
: En tête.

_Le pilard_ (6a)
: En tête. Magnifique !

### Secteur _Tapis Volant_

_Le phaco, la chauve-souris et le kronenbourg_ (6a)
: En tête.

_Nostalgie du désert_ (6c)
: En moulinette, travail.

---

## 2019-02-23 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par Lionel BLANC.  
Avec Julie DZIALOSZYNSKI.

_Le pilier oublié_ L1 (5c)
: En moulinette. Relais chaîné.

_Le pilier oublié_ L2 (4c)
: En moulinette. Relais chaîné.

_Le pilier oublié_ L3 (5a)
: En moulinette.

_Le pilier oublié_ L4 (4c)
: En moulinette. Relais chaîné.

_Le pilier oublié_ L5 (4b)
: En tête.

_Le pilier oublié_ L6 (6a)
: En tête. Un pas au début. relais chaîné.

_Le pilier oublié_ L7 (3b)
: En tête. Relais chaîné au sommet.

Horaire : ~3h 45min dans la voie.

---

## 2019-02-09 Rochers de la Condamine (Saint-Ferréol-Trente-Pas)

Organisé par CAF Jean-Louis DIGON.  
Avec Lorraine LETRANCHANT, Thierry STOULS.

_Pieds Agiles_ (5a)
: En tête.

_Totem_ (5c)
: En tête.

_Océan de sable_ (6a)
: En tête.

_Des larmes sous le soleil_ L1+L2 (6b)
: En tête. Un bac jaune après le surplomb.

_Un thé au Sahara_ (6a+)
: En tête.

_Dune de miel_ (5c)
: En tête. Physique.

---

## 2018-12-01 Rochers des Abeilles (Soyans)

Organisé par CAF Michel ROBERT.  
Avec Jean-Louis DIGON, Jean-Mathieu MONTMAGNON, Sylvaine VALETTE, Grégoire NONOCHIAN...

_Moniebah_ (5c)
: En tête.

_Mosaïque_ (5c)
: En tête.

_Couleur Café_ (5b)
: En tête.

_Ambigue Amitie_ (6a)
: En moulinette.

_Chasseurs de miel_ (6a+)
: En tête.

_Le drapeau de la colère_ (5c)
: En tête.

_Ma brave dame_ (5b)
: En tête.

---

## 2018-11-24 La Graville (Saoû)

Organisé par CAF Jean-Louis DIGON.  
Avec Bernard ARNUTI, Jean-Mathieu MONTMAGNON, Elina REYNAUD, ...

_La bleue_ (3b)
: En tête.

_Le plan_ (4b)
: En tête.

_Monalisa_ (5c)
: En tête.

_La vèbre_ (5b)
: En tête.

_La voie du dièdre_ (4c)
: En tête.

_Ouistiti Zodrôme_ (5c)
: En tête.

---

## 2018-11-03 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Michel ROBERT.  
Avec Bernard ARNUTI, ...

### Secteur _K NET_

_X-o_ (5c)
: En tête.

_E=mc2_ (5c)
: En tête.

_CD07_ (6a)
: En tête. Un pas bloc au début.

_Le blues de l'ouvreur_ (5c)
: En tête.

_Les terrassiers_ (5a)
: En tête.

_Lisse comme un c..._ (6a)
: En tête.

_OCB_ (6a)
: En moulinette.

_Soleil Immonde_ (6b)
: En moulinette.

_Merci Léon_ (5b)
: En tête.

---

## 2018-10-21 Falaise d'Anse (Omblèze)

Organisé par CAF Sylvaine VALETTE.  
Avec ?

### Secteur _Tutévu_

_Shut Your Mouth_ (5b)
: En tête.

_L'opportuniste_ (5b)
: En tête.

_L'train blues_ (5c)
: En tête.

_Pâques au rabans_ (6b)
: En tête.

_L'aphone des falaises_ (6a)
: En tête.

_Raining stones_ L1+L2 (6a)
: En tête.

_Nage indienne_ L1 (4c)
: En tête.

---

## 2018-09-30 Gorges de la combe d'Oyans (Rochefort-Samson)

Organisé par Lionel BLANC.  
Avec Sylvaine VALETTE.

_La folie du zou_ L1 (6a)
: En moulinette.

_La folie du zou_ L2 (5b)
: En moulinette.

_La folie du zou_ L3 (4b)
: En moulinette.

_La folie du zou_ L4 (5b)
: En moulinette.

_La folie du zou_ L5 (4c)
: En moulinette.

Horaire : ~4h dans la voie.

---

## 2018-09-09 Gorges d'Omblèze (Omblèze)

Organisé par Bernard ARNUTI.  
Avec ?

### Secteur _Luluberlu_

_Chocé hô loare_ (5b)
: En tête.

_Brise glace_ (5b)
: En tête.

_Neulice_ gauche (5c)
: En tête.

_Neulice_ droite (5c)
: En tête.

_À la mémoire de Jérôme Cavalli_ (6a)
: En tête.

---

## 2018-08-19 Gorges d'Omblèze (Omblèze)

Organisé par Lionel BLANC.  
Avec Bernard ARNUTI, Gabriel ULLIEL, ...

### Secteur _Faubourg_

_Pudding_ (5c)
: En tête.

_Zozotte_ (6a)
: En tête.

_Visualise_ (6a+)
: En moulinette.

_Je suis un héros_ (6a+)
: En moulinette.

---

## 2018-08-15 Gorges d'Omblèze (Omblèze)

Organisé par Agathe BENARD.

### Secteur _Jardiland_

_Acrobuis_ gauche (5a)
: En tête.

_Nické Larson_ (5c)
: En tête.

_Chtaforty_ (6a)
: En tête. Athlétique à la fin.

---

## 2018-05-26 Mer de glace (Villevocance)

Organisé par CAF Jean-Louis DIGON.  
Avec Lorraine LETRANCHANT, Gabriel ULLIEL...

_Derche_ (5c)
: En tête.

_Au plaisir des dames_ (5b)
: En tête.

_Faut qu'on_ (5b+)
: En tête.

_Y'a qu'a_ (5b)
: En tête. Magnifique !

_La Jeanne_ (5c)
: En tête. Peu de pieds a la fin.

_Coince Caillou_ (6a)
: En tête. Début périlleux.

---

## 2018-04-28 Falaise de la Barre - Mur du Son (Saoû)

Organisé par CAF Jean-Louis DIGON.  
Avec Michel ROBERT, ...

_Tempo_ (5a)
: En tête.

_Crescendo_ (5b)
: En tête.

_Le cri du papillon_ (6a)
: En tête.

_A voix basse_ (5c+)
: En tête.

_Saouffle Chaud_ (6a)
: En tête.

_Boum!_ (6a+)
: En tête.

_A gorge deployee_ (5b)
: En tête.

_Big Bang_ (5b)
: En tête.

_Mistral rugissant_ (5b)
: En tête.

---

## 2018-04-14 Rochers de la Condamine (Saint-Ferréol-Trente-Pas)

Organisé par CAF Jean-Louis DIGON.  
Avec ?

_Décoche_ (5a)
: En tête.

_Visage pâle_ (5c)
: En tête.

_Leste et Céleste_ (6a)
: En tête.

_Totem_ (5c+)
: En tête.

_Geronimo_ (6a)
: En tête.

---

## 2018-04-07 Les Cabanes (St-Maurice-en-Chalencon)

Organisé par CAF Grégoire NONOCHIAN.  
Avec ?

### Secteur _K NET_

_Le blues de l'ouvreur_ (5c)
: En tête.

_Les terrassiers_ (5a)
: En tête.

_Lisse comme un c..._ (6a)
: En tête.

_OCB_ (6a)
: En moulinette.

_Soleil Immonde_ (6b)
: En moulinette.

_Merci Léon_ (5b)
: En tête.

---

## 2018-03-24 Rocher des abeilles (Soyans)

Organisé par CAF Grégoire NONOCHIAN.  
Avec ?

_Les deux mains prises_ (5b)
: En tête.

_Ma brave dame_ (5b)
: En tête.

_Le drapeau de la colère_ (5c)
: En tête.

_Scorpions_ (5c)
: En tête.
