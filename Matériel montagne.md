---
created: 2023-05-24T12:23:32+02:00
modified: 2024-07-21T20:14:33+02:00
type: Checklist
---

# Matériel montagne

## Fond de sac

- [ ] couverture de survie
- [ ] lunettes de soleil, crème solaire, stick à lèvres
- [ ] altimètre, téléphone+chargeur, topos+cartes
- [ ] petite pharmacie (aspirine/efferalgan, strap...)
- [ ] sifflet de sécurité
- [ ] lampe frontale (chargée !)
- [ ] sac poubelle
- [ ] papiers, carte CAF, argent liquide/chèques, stylo

## Matériel technique individuel

- [ ] chaussures d'alpinisme cramponnables
- [ ] bâtons de marche (optionnel)
- [ ] piolet(s) : droit ou techniques suivant course
- [ ] crampons + antibottes + sac
- [ ] guêtres
- [ ] casque
- [ ] baudrier, maillon rapide, 2 machards, longe, descendeur, mousqueton à vis directionnel
- [ ] dégaines : 2 à 6 par personne suivant course
- [ ] 2 anneaux de sangles (ou plus) avec leur mousqueton
- [ ] kit crevasse : 4-5 mousquetons, 2 broches à glace, sangle 180cm, micro-traxion

On n'a jamais trop de mousquetons !

## Suivant la course et les conditions

- [ ] chaussons d'escalade
- [ ] DVA, pelle, sonde
- [ ] corde(s)
- [ ] coinceurs et friends, décoinceur
- [ ] broches à glace, crochet à lunule
- [ ] pitons

## Vêtements

- [ ] bonnet
- [ ] foulard ou tour de cou
- [ ] bob ou casquette, selon météo
- [ ] t-shirt de rechange 
- [ ] veste polaire
- [ ] doudoune (selon météo)
- [ ] coupe-vent/gore-tex
- [ ] gants: 2 paires, à choisir selon météo
- [ ] pantalon d'alpi, pantalon imperméable et/ou collant selon météo
- [ ] chaussettes de rechange, à choisir selon météo
- [ ] sous-vêtements de rechange

## Nuit

- [ ] serviette, savon (optionnel)
- [ ] brosse à dent, dentifrice
- [ ] pyjama ou tenue pour dormir (optionnel)
- [ ] drap de soie
- [ ] affaires personnelles (boules quies, ...)
- [ ] mouchoirs

## Repas

- [ ] couteau
- [ ] gourde et/ou thermos+tasse
- [ ] encas : barres, fruits secs
- [ ] pain/crackers, fromage, saucisson, oeufs durs
- [ ] fruits, jus de fruit ou compotes
- [ ] chocolat
