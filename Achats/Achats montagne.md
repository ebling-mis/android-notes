---
created: 2020-05-14T14:29:32+02:00
modified: 2024-10-22T15:46:54+02:00
type: Checklist
---

# Achats montagne

- [ ] Friends Camalots C4 : #2, #3
- [ ] Friends Alien X : #1/3, #3/8
- [ ] Broches à glace alpi (TBD)
- [ ] Sac Millet D-Tour 30L "Saphir"
- [ ] Perche escalade "Beta Stick"
- [ ] Sac Millet Peuterey 35+10L
