---
created: 2024-08-28T07:26:34+02:00
modified: 2024-12-29T22:27:20+01:00
type: Checklist
---

# Achats autres

- [ ] Tasses
- [ ] Glacière + ice packs
- [x] Pantalon Bonobo (Straight T36 ou Slim T38, L32-34)
