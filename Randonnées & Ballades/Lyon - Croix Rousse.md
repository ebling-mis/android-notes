---
created: 2020-07-21T21:59:15+02:00
modified: 2021-08-03T11:43:27+02:00
---

# Lyon - Croix Rousse

Départ place des Terraux (accès par les métros A et C, station "Hôtel de Ville -
Louis Pradel").

Le quartier de la Croix-Rousse s'étend sur les flancs de la colline du même nom
et possède le deuxième grand réseau de traboules de Lyon. La Croix-Rousse
(éthymologiquement, la _colline qui travaille_, par opposition à Fourvière, la
_colline qui prie_) est l'ancien quartier des **canuts** : ouvriers de la soie
que des conditions de vie précaires ont amené à se révolter au 19e siècle au cri
de "vivre en travaillant ou mourir en combattant". Le terme de canut est un
dérivé de canette, une petite bobine située dans la navette qui permet de passer
le fil au métier à tisser.

### Place des Terraux

La **place des Terraux** est située sur le confluent du Rhône et de la Saône à
l'époque romaine. La ville de Lyon était alors de l'autre côté de la Saône, à
l'emplacement du Vieux Lyon. La place des Terraux abritait le village romain de
Condate ; les fossés des fortifications du 13e siècle bouchés de terre plus tard
ont donné leur nom à la place et au quartier.

La place est célèbre pour sa grande **fontaine Bartholdi**. Initialement
commandée par la ville de Bordeaux au sculpteur de la statue de la Liberté, elle
a finalement "atterri" à Lyon, à l’extrémité de la place des Terreaux, face à
l’Hôtel de Ville. Par la suite, elle a été déplacée au milieu de la place lors
de son aménagement sous Michel Noir. Elle répond en fait à la petite fontaine de
la cour du Musée des Beaux Arts, quand sa porte est ouverte. Soyez attentifs aux
naseaux des chevaux de droite : de la vapeur s'en échappe !

L'hôtel de ville, surmonté d'un beffroi, était le plus grand édifice municipal
à sa construction au milieu du 17e siècle. Dans la cour d'honneur, l'une des
statues représente le cyclope Polyphème (celui qu'Ulysse eut à combattre)...
avec trois yeux. Le sculpteur en a effet probablement d'abord fait sa statue
sans savoir que le Polyphème était un cyclope, avant de lui rajouter un oeil au
milieu du front.

Le **cloître du musée des Beaux-Arts** recèle un merveilleux jardin émaillé de
statues. On y accède par un portail au milieu de la place.

Se rendre au numéro **7, place des Terraux** : si la porte est ouverte, voici la
première traboule de Croix-Rousse ! Sinon, rejoindre la rue Sainte-Catherine par
la rue Sainte-Marie des Terreaux à gauche (à côté de la pharmacie). La rue
Sainte-Catherine est la rue des bars de nuit. Monter les escaliers jusqu'à une
petite place.

Prendre à droite après le magasin de bières sans entrer sur la place. Un portail,
de plus en plus souvent fermé hélas, permet d'entrer dans une traboule. On arrive
ainsi dans une cour, une des rares sur les pentes qui ait fait l’objet d’un
aménagement qui la fait ressembler à une cour italienne avec ses arbustes. A
certaines heures, il y a une lumière d’une douceur magnifique dans cette cour.
Sortir par la sortie opposée. On est 6, rue des Capucins.

Prendre à gaucheet aller tout droit jusqu'aux feux. Traverser et continuer par
la rue Sergent Blandan jusqu'à la place Sathonay.

### Place Sathonay

La place Sathonay est probablement l'une des plus belles de Lyon. Vivante toute
l'année, plus encore l'été, elle s'échappe vers le haut par un escalier qui mène
au jardin des plantes et à l'amphithéâtre des Trois Gaules. Au centre de la place,
une statue du sergent Blandan, qui s'est fait tué en Algérie durant la conquête
coloniale, mais bravement (voir sur son piédestal la phrase qu'il prononça à
ses collègues).

La mairie du 1er arrondissement est au fond de la place. Sur sa façade, une
oeuvre street-art au pochoir de l’artiste lyonnais Don Matteo, hommage au chanteur
du projet musical _Slow Joe and the Ginger Accident_. La bâtiment de la mairie du
1er était le local où l’on chouchoutait les plants avant de les transplanter dans
le jardin des plantes, avant que celui-ci ne fut déménagé au Parc de la Tête d’Or,
après deux tempêtes successives qui l’avaient dévasté.

De part et d’autre de l’escalier, admirer deux remarquables lions, fondus au Creusot,
d’où jaillit un élégant filet d’eau.

À voir également : fréquentes expositions photos dans la salle de la mairie au
milieu de l'escalier à gauche ; glacier _Sur le bout de la langue_.

### Montée de la Grande Côte

On quitte la place Sathonay par une manoeuvre délicate. Prendre la rue Poivre
au-dessus du commissariat. Aller jusqu’aux escaliers au fond de la rue, monter
jusqu’à la rue Terne. Là, traverser la rue en faisant attention, ou au besoin en
redescendant emprunter le passage clouté vers les feux. Aller jusqu’au **7, rue
Terme**. Appuyer sur le bouton portier, ça doit s’ouvrir, poussez, vous voilà
dans votre première vraie traboule. On débouche sur la **montée de la Grande
Côte**.

Cette montée très vivante qui a été entièrement refaite ces dernières années
est devenue une merveille, surtout dès que le soleil anime les façades colorées
dans des tas d’ocres différents. Nombre de petites boutiques d’arts, d’artisanat,
librairies alternatives, bars cosys, se sont installés ici. Pas de doute, on
est sur les pentes.

Des milliers d’ouvriers travaillaient ici au XIXe siècle, au domicile de maîtres
canuts qui avaient installés à leur domicile quelques métiers à tisser, les
bistanclaques (onomatopée du bruit qu’ils faisaient). Ces domiciles étaient
très haut de plafond pour permettre d’y construire les métiers à tisser. Ils
sont devenus des appartements que l’on appelle « canuts », souvent complétés
d’une mezzanine, grâce à la hauteur de plafond. Les maisons plus riches se
signalent par des fenêtres à meneaux.

Montez tout doucettement la montée de la Grande Côte en vous régalant des
alignements de façades colorées.

### Amphithéâtre des Trois-Gaules

A l’arrivée rue Burdeau, vous tournez à gauche pour redescendre quelques dizaines
de mètres puis remonter jusqu’à l’amphithéâtre des Trois Gaules. Chaque 1er août,
les représentants des 60 nations des Trois Gaules s'y réunissaient pour présenter
leurs doléances, qui étaient transmises à Rome.  Il y avait des fêtes, des joutes
de poésie, des sacrifices (et notamment celui de Blandine et ses amis chrétiens,
en 177).

Remontez maintenant l’escalier qui est sur le flanc droit de l’amphi. En dessous
de vous, vous avez le tunnel routier qui monte à la Croix-Rousse, sur l’emplacement
d’une des deux ficelles qui desservaient la colline.

### Jardin et esplanade de la Grande Côte

Vous débouchez rue des Tables Claudiennes et vous prenez à droite jusqu’à la montée
de la Grande Côte dont vous reprenez l’ascension à travers le jardin de la
Grande-Côte. Référence à l’histoire de la soie, il a été planté de muriers, lesquels
servaient à élever les vers à soie.

Après un escalier ardu à travers le jardin des pentes, vous arrivez à l’un des plus
beaux points de vue de Lyon. Cette esplanade à l’intersection de la rue des Pierres
Plantées, de la rue Jean-Baptiste Say et de la rue du Bon Pasteur n’a pas de nom.

Les arbres qui ont beaucoup grandi cachent désormais une partie du panorama. Vous
aurez une bien meilleure vue depuis le milieu de la rue des Pierres Plantées. En
face de vous, la Saône, Fourvière, le Vieux Lyon.

### Plateau de Croix Rousse

En haut de la rue des Pierres Plantées, on est face au centre névralgiques de la
Croix-Rousse : la place, le boulevard, la station de métro… Tous trois portent le
même nom.

Sur le boulevard, un gros marché chaque jour, avec des producteurs de l’ouest
lyonnais, de la vallée du Rhône et de Bresse. Des institutions lyonnaises comme
le _café Jutard_ avec ses plateaux d’huîtres le dimanche matin, le _Chantecler_,
le _café de la Soierie_, la _Grande Droguerie Lyonnaise_ où l’on trouve de tout.
Plus loin, la mairie du 4e avec une plaque commémorant les révoltes ouvrières de
1831 et 1834.

La **statue de Jacquard**, inventeur du métier à tisser éponyme, est au milieu de
la place de Croix-Rousse. Celui-ci utilisait des cartes perforées inventées par
Vaucanson, comme l’orgue de Barbarie, qui seront utilisées plus tard comme mémoires
de travail des premiers ordinateurs. Il y aura jusqu’à 30.000 de ces métiers dans
Lyon et près de 100.000 dans les environs. Tout cela constituait la « Grande
Fabrique ». Un seul ouvrier (surnommé le « canut ») suffisait à manoeuvrer ce métier,
d’où les révoltes des Canuts et les premières manifestations de destruction de
machines dans le pays. Cette invention marque l’arrivée en France de la première
révolution industrielle née de l’autre côté de la Manche.

### Place des Tapis

Rejoindre à gauche la **place des Tapis** par la rue de la Terrasse. C'est ici le
Vieux Port de Croix Rousse, aves ses terrasses aux chaises pliantes. En face, le
spectaculaire mur peint street-art sur 5 étages est renouvelé chaque année par
l'association MUR69.

À un coin, pieds dans l’eau, la statue oeuvre des sculpteurs Georges Salendre,
Da Fonseca et Hamelin. À l’autre coin, une petite statue « Le Printemps » du même
Georges Salendre, également auteur de « L’homme de pierre », place Bellecour.

Hommage à la soie et particulièrement à la sériculture, des gros objets de pierre
en forme de cocons de ver à soie sont disséminés sur la place.

Poursuivre 300m sur le Boulevard des Canuts jusqu'au numéro 36, pour venir trouver
le **mur peint des Canuts**, le plus connu et le plus spectaculaire mur peint de
Lyon.

Plus d'informations : [Lyon visite](http://www.lyon-visite.info/mur-peint-canuts-croix-rousse/)

### Gros Caillou

Rejoindre alors le Gros Caillou par le square qui porte son nom. Ce banal rocher
est l'attraction touristique du quartier. Le quaternaire l’a oublié là. À l’époque,
les glaciers des Alpes arrivaient jusqu’ici. Il s'agit d'un bloc erratique en _quartzite
triasique métamorphique_, roche compacte et dure. Il a été découvert en 1862 lorsque
l’on a construit le funiculaire de la Croix-Rousse.

Descendre par les escaliers en direction de la place Bellevue, second point de vue
remarquable de ce parcours. Une vue plongeante sur le Rhône, ses berges plus ou
moins aménagées et tout le VIe arrondissement. À gauche, le parc de la Tête d'Or,
son lac et son île.

Poursuivre à droite par la rue Bodin jusqu'à la place Colbert.

### Traboule de la Cour des Voraces

En bas de la place Colvert, au 9, rue Diderot, s'ouvre la **traboule de la Cour
des Voraces**. Sa cour date de 1840 et est dominée par un formidable escalier de
6 étages. La traboule plonge dans le ventre de l’immeuble, sinue à gauche, à
droite, débouche 3 ou 4 niveaux plus bas au 19 rue Imbert-Colomès, ou bien au
14bis montée Saint-Sébastien.

### Traboule Imbert Colomès et rue des Tables Claudiennes

Se rendre alors au **20, rue Imbert Colomès** pour une nouvelle traboule. On
ressort au 55, rue des Tables Claudiennes.

Cette succession de traboules depuis la place Colbert est, avec la Grande
traboule du Vieux Lyon, un passage dans le temps de la soie.

On se faufile le long de la fresque du théâtre _Le nombril du monde_, où
Florence Foresti a débuté. Il voisine un autre bon lieu, _Le théâtre des
Clochards célestes_. Place Chardonnet, on passe devant une cave de
jazz envoûtante, _La clef de voûte_.

On descend l’escalier typique du quartier, à deux volées, à la façade très
utilisée par les street-artistes.

### Rue Burdeau

On arrive rue Burdeau. La rue accueille une douzaine de galeries d'art
contemporain, photo et peinture. Au numéro 38, la galerie _Le Réverbère_,
créée en 1981 par Jacques Damez et Catherine Dérioz, est une des plus
anciennes en France.

Au numéro 17 se trouve un jardin contemporain fort réussi.

### Passage Thiaffait

On emprunte l’un des deux escaliers, soit celui à l’aplomb de la rue Pouteau
qu’une voiture a récemment embarqué, terminant sa course au fond, soit celui
situé entre le 30 et le 32 de la rue Burdeau. Mieux, faites les deux si vous
êtes amateur de street-art, ils sont en général très riches d’œuvres éphémères.

La quinzaine de boutiques-ateliers de ce passage constitue « Le village des
créateurs ». Elles accueillent des créateurs de mode en phase de lancement de
leurs produits.

### Escalier bleu Passage Mermet

Prendre à gauche en sortant, jusqu'à la paroisse Saint-Polycarpe. À sa droite,
le **passage Mermet**, devenu célèbre en 2019 lorsque les habitants du quartier
ont repeint en bleu le nez des marches de cet escalier créé au 19ième siècle
pour les besoins de la fabrique de la soie. Spot photographique inévitable !

À l'angle de la rue Abbé Rozier et de la rue Donné, on retrouve des oeuvres
de street-art, notamment les vinyles découpés de _Keza_. Sur le mur en face,
un « Droit dans le mur », il y en a quelques autres dans la ville.

### Traboule rue des Capucins

Descendre au dans la rue des Capucins. Au 25, remarquer la chimère à tête
de chien street-art au-dessus de la galerie d'art. Entrer dans la traboule au
**22, rue des Capucins**. Celle-ci est très caractéristique, en angle droit,
et débouche au 5 rue Coustou par un escalier d’une quinzaine de marches.

On prend la rue Romarin à gauche et, au bout, la place Croix-Paquet, la rue
qui descend à droite du jardin public, jusqu’à la rue de Thou.

### Traboule de Thou

Au **4, rue de Thou**, nouvelle traboule. Au centre, un très bel escalier
monumental carré datant du XVIIe siècle. Il appartenait au monastère des
Feuillants. Il arrive que la traboule ne soit pas accessible par le 4, faites
le tour par la rue du Griffon ou la grande rue des Feuillants.

La traboule a 2 sorties, petite rue des Feuillants ou bien rue des Moirages,
sortie que l’on vous recommande. Vous vous retrouvez dans une cour au pied
d’un escalier qui vous mène place du Griffon, juste au-dessus de l’Opéra.

### TODO

5, petite rue des Feuillants : escalier des Feuillants

### Jardin Rosa Mir Mercader

À ne pas manquer, au **87, grande rue de la Croix-Rousse** : le **jardin Rosa
Mir Mercader**. Il n'est ouvert que les samedi de 15h à 18h et du 1er avril au
30 novembre, et est assez peu connu des Lyonnais eux-mêmes. Il faut préciser
qu'il n'est indiqué à aucun endroit... C'est pourtant une visite assez rapide,
gratuite, et qui vaut le coup d'oeil ! Ce jardin privé de 400 mètres carrés a
été entièrement emménagé dans les années 1950 à partir de pierres et coquillages
par son propriétaire Jules Sénis, immigré Espagnol qui avait participé aux
chantiers de Gaudi à Barcelone. Il lui aura fallu pas moins de 25 ans pour
arriver au résultat final. Impossible de manquer le parallèle avec le palais
Idéal du facteur Cheval à Hauterives ou la chapelle du Mont Cindre dans les
Monts d'Or Lyonnais (qui ne se visite malheureusement plus...). Une petite
chapelle a même été ajoutée en l'honneur de sa mère Rosa Mir Mercader (à qui le
jardin est dédié) et à la Sainte Vierge.

Sources: [Lyon visite](http://www.lyon-visite.info/traboules-croix-rousse/),
[Les pérégrinations de Titi](http://www.wingsunfurled-web.com/fr/carnet-voyage/france/rhone-alpes/lyon.html)
