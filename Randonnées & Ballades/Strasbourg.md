---
created: 2020-07-21T22:02:08+02:00
modified: 2020-07-21T22:02:12+02:00
---

# Strasbourg

## Centre Ville

### Cathédrale

La cathédrale de Strasbourg est bâtie sur le point le plus élevé de la ville, à
142m d'altitude. À l'époque romaine, un temple occupait déjà cette place, puis
s'y sont succédés une église en bois au temps de Clovis, une église
carolingienne, une ottonienne puis une romane. Cette-dernière, d'une taille
comparable à la cathédrale actuelle, a brûlé en 1176.

La première pière de la cathédrale est posée en 1015. En 1177 commencent les
travaux de construction de la façade. La cathédrale sera de style gothique,
avec 3 portails d'entrée et une rosace de 15m de diamètre.

En 1439, les ouvriers terminent la plateforme, surplombant le parvis de 66m (à
titre de comparaison, la plateforme de l'église Notre-Dame de Paris culmine à
69m). Les bourgeois réclament des cloches pour annoncer les offices religieux.
Celles-ci seront ajoutées entre les deux tours. La construction des flèches fut
décidée la même année. La construction de la seconde flèche est rapidement
abandonnée, par manque d'argent mais aussi faute de fondations stables, la
cathédrale étant bâtie sur un sol sabloneux. Le monument culmine à 142m, ce qui
en fait l'édifice le plus haut du monde de 1625 à 1847. Aujourd'hui, elle est la
cathédrale la plus haute de France après celle de Rouen (151m). Il faut gravir
646 marches pour atteindre le sommet de la flèche.

La cathédrale est construite en grès rose des Vosges. La carrière la plus proche
se situait à 25km de Strasbourg. Les pierres plus claires correspondent à des
restaurations récentes. Toutes les sculptures de la façade sont en avant d'une
vingtaine de centimètres du mur : on parle de "dentelles de grès suspendues en
avant de la façade". Sur le **portail central**, les sculptures mettent en avant
l'amour de Dieu pour les Hommes : on y retrouve notamment la scène de la
Passion. Le tympan central retrace l'histoire de la semaine sainte depuis les
Rameaux (en bas à gauche) jusqu'à la Résurrection au matin de Pâques (tout en
haut). La plupart des statues sont cependant des copies, les originaux étant
conservés au musée de l'Oeuvre Notre-Dame. À gauche, on peut voir les
représentations des événements importants de la vie de Jésus. À droite, les
prophètes sont représentés autour du tympan qui met en scène la parabole des
vierges folles (à gauche) et des vierges sages (à droite). Le tentateur est
reconnaissable à ses crapeaux, ses serpents et ses chaussures pointues. Les
signes du zodiaque sont visibles en-dessous.

À l'extérieur, on retrouve un peu partout des bouchons métalliques : il s'agit
de repères permettant de surveiller la stabilité de la cathédrale à l'aide de
lasers. L'édifice a en effet vacillé au début du XXe siècle, mais des travaux
avaient immédiatement été entrepris pour soutenir les piliers.

Les parties les plus anciennes de la cathédrale sont le choeur, le transept et
la crypte. Elles sont les vestiges de l'ancienne cathédrale romane (XI-XIIe
siècle). Les choeur liturgique est aujourd'hui constitué du vrai choeur et de la
croisée du transept. Au début du XIIIè siècle, les croisillons nord et sud ont
été rajoutés alors que le style gothique commence à se répandre. Enfin, la nef
fut construite. Les **vitraux de la nef** sont datés du XIVe siècle. Ils
représentent les saints et saintes (partie supérieure de la nef), les empereurs
du Saint Empire Romain Germanique (en bas à gauche) et la vie de Jésus (à
droite). Tous les vitraux sont de style gothique, à l'exception notable des 3
premiers vitraux de gauche, romans.

L'orgue est situé sur la partie gauche de la nef. C'est en effet le meilleur
point accoustique de la cathédrale. Sa position au nord permet de limiter la
dilataion des tuyaux par la chaleur. Le bois de l'orgue a été revêtu de feuilles
d'or. De part et d'autre de l'orgue se trouvent deux statues en bois
articulées : un soldat jouant de la trompette et un vendeur de bretzels. Tous
deux représentent le peuple ; derrière l'orgue se trouve une cachette permettant
de faire bouger ces statues pour proférer des critiques à l'encontre de
l'Église. En 1524, la cathédrale devient protestante et les statues se "turent"
jusqu'en 1681.

La **chaire**, de style gothique, date de 1487. Elle est aussi faite en grès et
est ornée de statues du Christ, des apôtres, mais aussi d'un chien célèbre,
celui que le prédicateur Geiler emmenait toujours avec lui. Depuis, le toucher
apporte la chance, dit-on.

Nous nous dirigeons ensuite vers le **croisillon nord**. Les vitraux, de style
roman, représentent le jugement modèle du roi Salomon. La voûte sur croisée
d'ogives est conçue pour soulager les murs et reporter son poids sur le pilier
central cylindrique. Nous passons devant le choeur, de style roman également.
Son vitrail, qui représente la Vierge à l'enfant, a été offert en 1956 par le
Conseil de l'Europe. Nous atteignons le croisillon sud, qui abrite l'**horloge
astronomique** depuis 1547. Outre l'heure, elle indique les phases de la Lune,
les équations du temps, les éléments du comput ecclésiastique, la position des
planètes du système solaire et la date du jour. L'horloge astronomique
fonctionne grâce à des poids qui sont remontés tous les lundis vers 10h. Tous
les quarts d'heure, les quatre âges de la vie (l'enfant, l'adolescent, le
guerrier et le vieillard) passent devant la mort. À 12h30 (l'horloge affiche en
effet une demi-heure de retard), un coq bat des ailes et chante trois fois pour
rappeler le reniement de saint Pierre. Le jour de la semaine est indiqué sur un
char. Au centre du croisillon sud, on peut admirer le **pilier des anges**.
Comme au nord, il soutient la voûte. De bas en haut, on y retrouve les 4
évangélistes, puis 4 anges jouant de la trompette, et enfin le Christ sur un
trône en compagnie de 3 anges. Lors de sa construction, un passant aurait
prétendu que le pilier en pourrait jamais supporter le poids de la voûte et que
celle-ci s'effondrerait. Pris de court, l'ingénieur décida de le sculpter et de
l'accouder à la balustrade, en face du pilier. Aujourd'hui, il attend toujours
que la voûte s'écroule...

Source: Visite guidée de la Cathédrale, mars 2008
