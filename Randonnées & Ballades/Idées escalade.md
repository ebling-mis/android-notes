---
modified: 2024-08-28T07:17:29+02:00
modified: 2024-08-28T07:17:29+02:00
type: Checklist
---

# Idées escalade

- [ ] Défilé des Gillardes (Dévoluy)
- [ ] La Grande arabesque (Aravis)
- [ ] Presles: _Topomaniak_ ou _Chrysanthèmes_ jusqu'à la vire médiane, puis _La Grotte_
