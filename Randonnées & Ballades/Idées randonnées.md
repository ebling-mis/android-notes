---
created: 2020-07-21T21:45:05+02:00
modified: 2024-08-28T07:16:29+02:00
type: Checklist
---

# Idées randonnées

- [ ] Roc de Garnesier ***
- [ ] Toussière 1916m
- [ ] Lac du Vallon (Chantelouve)
- [ ] Lac de la Blanche (Queyras)
- [ ] St-Christophe-en-Oisans La Ville -> Miroir des Fétoules -> Tête de la Toura -> retour par télécabines Jandri Express + Vénosc
- [ ] St-Christophe-en-Oisans Plan du lac -> Lanchatra -> Monter sur les crêtes (exposé, grand ciel bleu requis) -> contourner la coche -> quand la crête se redresse et plusieurs gros rochers à droite, filer sur le versant droit jusqu'au ref. de la Muzelle -> retour direct sur Vénosc OU nuit et retour par le col du Vallon (descente exposée) et Lauvitel
