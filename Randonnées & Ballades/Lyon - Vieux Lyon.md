---
created: 2020-07-21T21:57:07+02:00
modified: 2020-07-31T13:15:54+02:00
---

# Lyon - Vieux Lyon

## Quartiers Saint-Jean et Saint-Paul

Départ place Saint-Jean (accès par le métro D, station "Vieux Lyon").

### Cathédrale Saint-Jean-Baptiste

Sur la place, un arrêt s'impose à la Cathédrale Saint-Jean-Baptiste. Cette
église a su garder une trace de chacune des époques qu'elle a traversées : des
fouilles ont mis à jour des traces de 3 édifices religieux remontant en partie
au 4e siècle. L'**influence romane** est visible dans les voûtes autour du
choeur (cheminement à l'étage). Les **vitraux** aussi ont traversé plusieurs
époques : ornés de couleurs froides au sud et chaudes au nord, certains ont été
remplacés plus récemment. La **grande rosace**, appelée _Rosace du couchant_,
mesure 12 mètres de diamètre !

La cathédrale est aussi connue pour son **horloge astronomique**, qui est l'une
des plus vieilles d'Europe (1383, au moins), et également l'une des seules à
actionner des automates. Avant son arrêt en 2013, elle sonnait plusieurs fois
par jour, avec un défilé de personnages. L'ENS y a d'ailleurs consacré
[une page sur son site](http://www.ens-lyon.fr/RELIE/Cadrans/Musee/HorlogesAstro/Lyon/Cathedrale.htm).

### La Manécanterie

Située sur le flanc sud de la cathédrale, la Manécanterie est sans doute le
plus vieil édifice du quartier (11e siècle). Elle appartenait autrefois à un
ensemble de batisses qui formait le petit cloître et servait de résidence aux
chanoines. La Manécanterie en aurait été au début le dortoir ou le réfectoire.
Par la suite, elle a abrité l'école de chant des clercs, qui lui donne son nom.
La Manécanterie abrite aujourd'hui le **trésor de la cathédrale** et se visite
(accès gratuit, Ma-Sa: 9h30-12h et 14h-18h, Me: à partir de 11h, compter 30
minutes).

[Plus d'informations sur la Manécanterie](http://lyonhistorique.fr/vieux-lyon-manecanterie/)

### Le Vieux Lyon et ses traboules

Le quartier du Vieux Lyon possède une histoire de plus de mille ans. Il a failli
disparaître après la seconde guerre mondiale lorsqu'un maire lyonnais a voulu
implanter des marinas en bord de Saône. Des associations de riverains et Malraux
lui-même ont heureusement réussi à l'arrêter et le site est maintenant classé au
patrimoine mondial de l'UNESCO.

Le Vieux Lyon est avant tout célèbre pour son réseau de traboules. Trabouler, du
latin _trans ambulare_, signifie _passer à travers_. Les traboules sont des
passages piétons à travers des cours d'immeuble qui permettent de se rendre
d'une rue à une autre. Au Moyen-Âge, les traboules du Vieux Lyon permettaient de
rejoindre rapidement les bateaux sur la Saône. Par la suite, elles ont abrité
les allées et venues clandestines des Résistants lyonnais pendant la seconde
guerre mondiale.

Sur les 500 traboules recensées à Lyon (215 dans le Vieux Lyon, 163 à
Croix-Rousse et 130 dans la Presqu'île), une quarantaine se visite gratuitement
dans le cadre d'accords passés entre la commune et les particuliers. Malgré
cela, ces passages demeurent privés.


Prendre la **rue des Antonins** (petite rue au milieu nord de la place), à
gauche du café de la Gargouille. La ruelle s'élargit et vers le milieu de sa
longueur, à droite, s'ouvre une première traboule, pas spectaculaire ni très
belle, mais typique pour le changement d'univers qu'elle apporte entre son
entrée et sa sortie. Un arbre pousse entre les pavés, on l'a laissé grandir.

On ressort au 70, rue Saint-Jean, au milieu de la foule des touristes. Prendre
à gauche.

### Maison du Chamarier

On passe la rue Sainte-Croix sur la droite pour trouver, juste après, la Maison
du Chamarier (elle héberge d'ailleurs la fameuse patisserie _À la Marquise_).
Il s'agit d'un exemple rare de maison civile de l'époque Renaissance. Sa cour
sublime a été restaurée en 2005 et laisse admirer un **puits Renaissance**
attribué à Philibert Delorme, alors qu'il revenait d'Italie à 26 ans. Parmi les
autres éléments de l'édifice, on note **l'escalier à vis** datant de la fin du
Moyen-Âge et la **_loggia_ à l'italienne** ornée d'un mur peint (au-dessus de
l'entrée de la cour).

### Maison des Avocats et rue de la Bombarde

Remonter ensuite sur quelques mètres la rue de la Bombarde pour s'arrêter
devant la facade la plus photographiée du Vieux Lyon: la Maison des Avocats
offre un style résolument... florentin ! Elle héberge le **musée des miniatures
et des décors de cinéma**.

Poursuivre un peu la montée pour admirer, au dessus de la porte du numéro 10,
une plaque avec un boute-feu qui allume une bombarde. C'est elle qui a donné
son nom à la rue.

### La Longue Traboule

Redescendre jusqu'à la rue Saint-Jean et poursuivre jusqu'au **numéro 58**.
Avec un peu de chance, la cour derrière la crêperie _Au petit glouton_ sera
ouverte. Si elle est un peu sombre et exigüe, et a un très joli puits adjacent
à la salle et à la cuisine de la crêperie.

Au 52, rue Saint-Jean, au fond d'une traboule en impasse, on trouve la
**boutique médiévale _Mandragore_**: on peut y acheter des gantelets en cotte
de mailles, des vêtements d'époque, velours et robes longues, ...

Enfin, au **54, rue Saint-Jean** se trouve la _Longue Traboule_, la plus longue
de toutes les traboules du Vieux Lyon, traversant quatre immeubles et quatre
cours. Attention à l'affluence aux heures de pointe !

On ressort au 27, rue du Boeuf. Tourner à droite.

### Rue du Boeuf

On longe les boutiques d'art, le restaurant quatre étoiles _La Tour Rose_,
avant d'arriver à la **place Neuve Saint-Jean**. À l'angle, on remarque une
statue en bois d'un boeuf sur le coin du mur.

Poursuivre jusqu'au **16, rue du Boeuf** pour trouver la _Cour de la Tour
Rose_. La somptueuse tour rose abrite un escalier à vis desservant quatre
étages et a été édifiée au 16e siècle.

À noter également, la présence d'une cour au 14, rue du Boeuf.

Revenir en arrière jusqu'à la place Neuve Saint-Jean.

### Place Neuve Saint-Jean

Entre les terrasses des restaurants, au milieu de la place, se tient une
remarquable cour ouverte avec les escaliers visibles. Cela fait penser à
_Fenêtre sur cour_ d'Hitchcock.

Plus loin, on trouve à l'angle la _Boulangerie du Palais_, réputée pour une
des spécialités de Lyon, les brioches pralinées. Il n'est pas rare de devoir y
faire la queue !

À sa droite, le bâtiment énorme est l'ancien palais de justice. Les lyonnais
l'appellent _Les 24 colonnes_ à cause des colonnes corynthiennes qui ornent sa
façade est (face à la Saône). Ici siègent encore la Cour d'appel et la Cour
d'assises du Rhône. Le palais dispose de sa propre prison pour les longs
procès. Maurras a été jugé là en 1945 et Klaus Barbie en 1987. Dans le bâtiment
qui précédait celui-ci, furent jugées des centaines de personnes durant la
Terreur, puis guillotinées place des Terreaux.

Revenir pour trouver, au **5, place Neuve Saint-Jean**, une petite traboule qui
contourne le restaurant pour ressortir au 40, rue Saint-Jean.

### Rue des Trois Maries et place de la Baleine

Poursuivre la rue Saint-Jean vers le nord (sens décroissant des numéros). Au
numéro 27, nouvelle entrée de traboule. Celle-ci permet de traverser deux cours
à galeries du 16e siècle, entièrement restaurées à l'aide d'enduits de couleur
vive. À la rentrée, il n'est pas rare d'y retrouver des étudiants qui
participent à des jeux de piste dans le Vieux Lyon.

On ressort au 6, **rue des Trois Maries**.  La rue possède une courbure en
boomerang bien caractéristique du Vieux Lyon. On s'y croirait au 16e siècle.

Au numéro 9, nouvelle traboule. Celle-ci débouche au 17, quai Romain
Rolland, mais nous reviendrons sur nos pas pour retrouver la place de la
Baleine au nord de la rue des Trois Maries.

On se retrouve au coin de la très jolie **place de la Baleine** et de la rue du
même nom. Là aussi, un panneau sculpté représente une baleine (sur la facade à
l'angle avec la rue de la Baleine). En hiver, cette place est parfois le
rendez-vous des cracheurs de feu.

### Rue Saint-Jean

Retrouver la rue Saint-Jean et revenir sur la gauche jusqu'au **28, rue
Saint-Jean** pour admirer une belle cour intérieure.

Poursuivre ensuite vers le nord (sens décroissant des numéros) jusqu'à la
**place du Gouvernement**. Attardons-nous un instant sur le bâtiment au numéro
2, à l'angle de la place. Construit au 16e siècle, il abritait l'hostellerie de
Saint-Christophe qui donnait refuge aux voyageurs. L'édifice a conservé
quelques éléments de décor gothique. Passer la grille à droite pour découvrir
l'entrée d'une traboule qui conduit à la splendide cour haute, alors située
au-dessus de l'écurie. Elle débouche au 10, quai Romain Rolland, mais nous
reviendrons sur nos pas pour reprendre la rue Saint-Jean vers le nord.

### Hôtel et musées de Gadagne

À la hauteur du 6, rue Saint-Jean, prendre la ruelle à gauche pour atteindre au
fond les musées Gadagne. Le bâtiment, hôtel particulier classé monument
historique dès 1920, abrite aujourd'hui le musée historique de la ville de
Lyon et le musée des arts de la marionnette. Sa vaste cour est à elle seule
un joyau ; son jardin en hauteur, situé au 4e étage (accessible par l'ascenseur
sans billets des musées) vaut le détour.

### Temple du Change et maison Thomassin

Revenir sur ses pas jusqu'à la rue Saint-Jean et poursuivre vers le nord.
On arrive rapidement **place du Change**: autrefois, on y changeait sa monnaie
avant de quitter le royaume de France.

Le **palais de Change**, remanié par Soufflot en 1748, est un temple de culte
protestant depuis 1803. Deux horloges couronnent sa façade : celle de droite
est l'horloge "idéale" imaginée par Soufflot, marquant jours, mois et années.

En face, la facade gothique de la **Maison Thomassin** (1493) est également
typique.

### Rue Juiverie

Monter la rue de la Loge, à droite du palais du Change, puis tourner à droite
dans la **rue Juiverie**. Avant d'entrer dans la rue, on pourra gravir les
premières marches de la montée des Carmes Déchaussés pour découvrir l'escalier
Renaissance de la maison Henri IV.

Presque toutes les façades de la rue Juiverie sont intéressantes : fenêtres à
meneaux, gargouilles vraies ou factices ajoutées par les habitants. S'y sont
installés plusieurs ateliers de troupes de théâtre, un restaurateur d'horloges
au numéro 20 (_l'Horloger de Saint-Paul_), ainsi qu'un bel atelier de sculptures
au numéro 15. Enfin, au **8, rue Juiverie** se trouve la galerie Philibert
Delorme. Une plaque dans la cour explique sa construction par un architecte
rentrant d'Italie. Une merveille de légèreté et de symbolisme qui unit deux
maisons !

### Place et église Saint-Paul

On arrive **place Saint-Paul**, centre de ce quartier qui avec Saint-Georges et
Saint-Jean constitue le Vieux Lyon. Passer devant la gare et poursuivre jusqu'à
l'église Saint-Paul, magnifiquement restaurée, dedans comme dehors. L'édifice
date du 6e siècle, et des ajouts de style gothique ont été réalisés par la
suite. Prendre quelques minutes pour admirer son clocher, sa fresque d'anges
musiciens, ses clefs de voûte et son acoustique.

À côté de l'église se trouve une place tranquille, à l'écart du barouf
touristique de Saint-Jean, la **place Gerson**, avec un café-théâtre
caractéristique du Vieux-Lyon et de l'esprit des cafés-théâtres à leur démarrage
dans les années 1970 sur les pentes de la Croix-Rousse.

Traverser la place pour retrouver les bords de la Saône et la **passerelle
Saint-Vincent**, qui permet d'accéder directement à la célèbre fresque des
Lyonnais et à Croix-Rousse.

Source: [Lyon visite](http://www.lyon-visite.info/vieux-lyon/)

---

## Quartier Saint-Georges

Ce quartier est plus réduit que les quartiers Saint-Jean et Saint-Paul, mais
présente quelques points d'intérêt. Il s'étend au sud de la place Saint Jean
(accès par le métro D, station "Vieux Lyon").

### Place de la Trinité

Rejoindre la **place de la Trinité** par l'avenue du Doyenné, puis à droite par
la rue Mourguet, et enfin à gauche par la rue Tramassac. Cette place marque le
début de la rue Saint-Georges. Le café du soleil est le plus ancien de Lyon.

En face se trouve le **théâtre du Guignol**, où des représentations sont données
certains soirs. Guignol est effectivement une marionnette inventée à Lyon au
début du 19e siècle. Laurent Mourguet, canut au chômage, devient dentiste (ou
plutôt "arracheur de dent", comme on les appelait à l'époque). Comme de nombreux
collègues, il utilisait une marionnette de Polichinelle pour distraire ses
patients. C'est de là que lui vient l'idée de créer son propre personnage :
Guignol, cheveux noués en catogan, et muni d'un bâton, la tavelle.

### Rue Saint-Georges

Au numéro 8, on trouve le magnifique **escalier traboule** : une magnifique cour
intérieure avec un escalier hélicoïdal. Il n'y a malheureusement pas de bouton
de porte, il faudra attendre qu'un habitant vous ouvre...

Prendre la rue Saint-Georges vers le sud. Au numéro 10, une petite traboule
débouche au numéro 12 en passant par une magnifique cour et des jardins
intérieurs.

Au numéro 14, on débouche après quelques marches dans une petite cour privée
accrochée au flanc de la colline.

Poursuivre dans la rue Saint-Georges jusqu'à l'**église Saint-Georges**. Son
tympan représente le combat de Saint-Georges contre le dragon.

On pourra poursuivre jusqu'au 100, rue Saint-Georges, où s'est installé le
**musée des automates**.

Source: [Les pérégrinations de Titi](http://www.wingsunfurled-web.com/fr/carnet-voyage/france/rhone-alpes/lyon.html)