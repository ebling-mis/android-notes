---
created: 2020-07-30T22:22:05+02:00
modified: 2020-07-30T22:22:51+02:00
---

# Lyon - Murs peints

Les murs peints de Lyon sont connus pour leur nombre, leur diversité et leur
beauté. Créés en grande partie par la coopérative CitéCréation, ils font
désormais partie du patrimoine de la ville. Quatre sont en bord de Saône, dont
l'immense mur des Lyonnais.

## Mur de la Cour des Loges

À deux pas de la place du Change, dans le Vieux Lyon, se trouve le mur peint de
la Cour des Loges. Depuis la place, rejoindre le bord de la Saône : le mur est
à l'angle de la rue François Vernay.

Ce mur est très habilement fait et si l'on n’est pas prévenu qu'il s’agit d'un
trompe-l'oeil, on a toute chance de se tromper et de croire qu'il s’agit d'un
véritable échafaudage. S'y ajoute une mise en abîme qui piège le cerveau : cette
bâche est-elle réelle ?

Au pied de ce mur, le **glacier Nardone** est réputé à Lyon.

## Mur des Lyonnais

C'est une des fresques à voir absolument à Lyon : 31 célébrités lyonnaises
sont représentées sur 800 mètres carrés !

Observez bien la fresque ! Vous remarquerez des effets saisissants de trompe
l'oeil : les ombres des balcons, un léger effet de cette brume qui monte aux
matins d'automne de la Saône.

À l'opposé, angle rue Pareille et rue de la Martinière, on a un mur peint
beaucoup plus discret. Il joue si bien du trompe-l'oeil qu'on ne le découvrirait
pas si, à l'une de ses fenêtres, au premier étage, il n'y avait un personnage
anachronique. Regardez tout en haut, au dernier étage, le chat à la fenêtre.
Jouez à distinguer le vrai du faux.

## Mur Tony Tollet

Remonter légèrement la rue Pareille pour venir trouver un peu plus haut le mut
peint Tony Tollet, réalisé en 2012 et conséacré à ce peintre lyonnais qui fut
élève d'Ingres et prix de Rome ne 1885.

## Mur peint des écrivains, quai de la Pêcherie

Descendre le quai Saint-Vincent vers le sud. Il devient quai de la Pêcherie
après le pont de la Feuillée.

À l'angle de la rue de la Platière et du quai de la Pêcherie se trouve le **mur
des écrivains**. Y figurent environ 300 écrivains lyonnais et des extraits de
leurs textes.

Remarquez au rez-de-chaussée de ce mur en trompe-l'oeil trois boutiques et le
facteur dans le porche. Juste à côté, une boîte aux lettres PTT, une vraie !

## D'autres murs peints à Croix Rousse

TODO: Le mur des Canuts

Source: [Lyon visite (Bords de Saône)](https://www.lyon-visite.info/murs-peints/),
[Lyon visite (Croix Rousse)](https://www.lyon-visite.info/mur-peint-canuts-croix-rousse/)
