---
created: 2021-08-01T20:41:06+02:00
modified: 2021-08-03T10:57:27+02:00
---

# Presles - Sect. Éliane - La Voix d'Éliane

## Cotations

Cotation globale : D+  
Cotation escalade : 6a (5c obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1

## Ambiance

Ce n'est pas la plus belle, mais c'est la voie facile de Presles la plus parcourue.

Ouverture : 1997, par Bernard Gravier. Les 2 dernières longueurs sont celles de la
_Belle Jardinière_, ouverte le 28 octobre 1975 par Bernerd Amy et Pierre Chapoutot.

**Attention** : un couloir d'éboulement débouche sur R3, des chutes de pierre
peuvent survenir dans les premières longueurs de la voie (dégel, chamoix...).
L2 est particulièrement exposée.

## Approche -- 30min

Depuis le plateau de Presles, redescendre par la RD292 en direction de Pont-en-Royans.
Après le premier virage à droite et avant le tunnel, prendre un bon sentier à gauche
permettant de couper le virage des Roches du Nugues. Descendre 100m, traverser la route,
couper de nouveau le lacet par un raide sentier (corde). Prendre à droite sur la route
pendant 20 m pour rejoindre à gauche un sentier (panneau).

Suivre ce sentier sur 500 m. Il descend, remonte et se sépare en deux, à gauche du
pilier SW. Le sentier de droite descend en direction de Choranche. Dix mètres avant
le croisement, suivre une vague sente (cairn et marquage bleu "VE") pour arriver au
pied de la falaise. Là, suivre à gauche un flèchage bleu ("V.Eliane") sur quelques
dizaines de mètres pour arriver au pied de la voie (corde fixe sur la fin). Marquage
bleu clair _Voix Eliane_ au pied, pas forcément très visible.

## Voie

### L1 -- 5c

Un départ patiné qui fait chauffer les bras puis plus calme. R1 chaîné.

### L2 -- 5c

Départ sur la gauche du relais. Un passage en traversée vers la droite délicat et un peu exposé car patiné. Le reste peu soutenu.
R2 chaîné et comfortable sous la vire médiane.

### L3 -- 5b+

Petit surplomb au départ. Belle longueur en dalle, un peu patinée. Certains points sont bien trop à gauche, ne pas se laisser entraîner et chercher le cheminement le plus facile (souvent plus à droite). R3 chaîné, comfortable.

### L4 -- 6a

Beau pilier raide, un peu physique. Équipement serré.  
Une petite variante plus dure (6a+) à gauche dans
le haut.

R4 chaîné.

### L5 -- 5b

Du 5b pour traverser à gauche au départ, puis du 4c. Passer le relais sur deux spits et
faire R5 confort sur le gros arbre.

### L6 -- 2

Longueur de jonction. Monter au-dessus de l'arbre sur 20m. Au sommet des cordes fixes,
traverser 20m à droite sur la petite vire pour trouver la suite (fissure-dièdre).
On sort par _La belle Jardinière_.

### L7 -- 5b

Longueur en dièdre avec de bons bacs. Relais sur une terrasse.

### L8 -- 5c

Départ sur la droite du relais (points cachés). Début en dalle facile, puis quelques pas plus soutenus.

Relais sur rocher au sol ou sur arbre plus haut (L7-L8 peuvent s'enchaîner, 40-45 m,
en gérant bien).

### Variantes

On peut remplacer L1 et L2 par les trois longueurs de _Mimosa_ (écrit en rouge au pied
de la voie, 5c+), ou d'_Un autre monde_ (juste à côté de _Mimosa_, 6a, relais communs) et
rejoindre la Voie d'Éliane en traversant sur la grande vire à droite (2 passages cordés).
Éliane est la dernière voie avant les surplombs.

## Descente

Prendre une sente évidente qui ramène au parking par une clairière (cairn). Traverser la clairière, prendre une piste et revenir au NNW pour rejoindre la D292.
