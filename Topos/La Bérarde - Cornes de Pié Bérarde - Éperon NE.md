---
created: 2022-06-25T16:14:00+02:00
modified: 2022-06-27T20:26:12+02:00
---

# La Bérarde - Cornes de Pié Bérarde - Éperon NE

## Cotations

Cotation globale : D-  
Cotation escalade : 5a (5a obligatoire)  
Engagement (échelle C2C) : III  
Équipement en place (échelle C2C) : P2

## Ambiance

Cadre grandiose et solitude (quasi) garantie pour cette escalade qui s'effectue traditionnellement à la journée depuis la Bérarde. L'itinéraire remonte un pilier NE en rive gauche d'un couloir/cheminée très encaissé aboutissant au pied de l'arête SE des Cornes. La voie se déroule sur du beau granit jaune/orange, sauf sur les 15 premiers mètres qui ont subi un éboulement. Les deux longueurs en 5 sont bien protégées (pitons).

Ouverture : 25 août 1957, par Abel Barnaud, Pierre Girod, Denise Heilman et Max Puissant.

## Approche -- 3h

De la Bérarde, prendre le sentier d'accès au vallon des Étançons (direction refuge du Châtelleret) rive gauche du torrent des Étançons. Vers 1890 m, après la passerelle sur le ruisseau de Bonnepierre, prendre à droite pour entrer dans le vallon de Bonnepierre. Suivre le sentier qui est plus ou moins marqué au début (cairns), puis qui remonte la moraine en rive droite du glacier de Bonnepierre.

Dès que la moraine devient horizontale (~2500m), descendre sur le glacier (cairn, emplacement de bivouac) et le traverser au mieux en visant l'aplomb du col de Pié Bérarde. Remonter le cône du couloir nord du col sur 100 m, puis prendre à droite pour atteindre le pied de l'éperon (névé en début de saison) : [photo depuis le glacier](https://media.camptocamp.org/c2corg-active/1410297676_1953142821BI.jpg)

**Attention !** La suite de l'approche est très exposée aux chutes de pierres.

S'engager dans le couloir à gauche de l'éperon sur 20 m (repérer un bloc coincé caractéristique).
Venir buter au pied du bloc coincé, gravir le court ressaut par la gauche (facile mais exposé, encordement à considérer) et rejoindre le léger replat formé par le bloc coincé.

Du replat, il est tentant de rejoindre le pied de l'éperon (en rochers gris et délités) au niveau d'un second replat plus haut à droite : **ne pas y aller** ! À la place, continuer dans le couloir qui présente main droite des rochers fracturés orange ; on y trouve à 4 m de hauteur un spit qui marque le début de la voie.

Photos de la voie : [depuis le pierrier terminal](https://media.camptocamp.org/c2corg-active/1472737263_311547475BI.jpg), [depuis la moraine du glacier](https://media.camptocamp.org/c2corg-active/1472737304_1849521330BI.jpg)

## Voie

### L1 -- 3b

Remonter droit au-dessus du spit dans des gradins faciles, puis rapidement traverser à droite pour rejoindre le fil de l'éperon par une petite vire (rocher moyen). Relais intermédiaire possible sur 2 pitons au pied d'un ressaut. Ne pas s'engager sur la vire à droite, mais gravir une succession de petits ressauts et virettes faciles en restant au plus près du fil. On arrive bientôt sur une large vire rocheuse qui traverse à droite.

R1 sur deux pitons au pied d'un mur noir.

### L2 -- 4b

À partir de là, le rocher devient bien meilleur. Monter droit dans le mur noir (15-20 m, 1 piton sur la droite de la dalle, superbe) et rejoindre la base de deux jolis dièdres. Possibilité d'enchaîner avec L3.

R2 sur 1 piton + 1 spit.

### L3 -- 5a, 15m

Gravir le dièdre de droite (10 m, 2 spits, 1 friend coincé, joli), et rejoindre facilement une brèche à gauche. Cette longueur peut-être enchaînée après la précédente.

R3 sur becquet à côté de la brèche à gauche.

### L4 -- 3b, corde tendue

Remonter le fil de l'éperon sur 30 m puis traverser à droite de l'éperon par une vire/gradins ; revenir dès que possible sur le fil (pour ne pas gâcher 5 m de 3b, rocher correct mais sans plus), qui vient mourir au pied d'un ensemble de dalles orange (1 piton dans le bas des dalles).

### L5 -- 5a

Repérer un grand dièdre/cheminée qui s'évase vers le haut et rejoignant le nouveau fil : **ne pas s'y engager**. On devine alors, à gauche du dièdre-cheminée, une rampe en ascendance à droite qui rejoint le haut du dièdre-cheminée. Traverser facilement en ascendance à gauche sur 15-20 m pour rejoindre la base de cette rampe, dalleuse et compacte sur 15 m, en ascendance à droite (3 pitons). Celle-ci permet de rejoindre la sortie du dièdre/cheminée (que l'on n'escalade à aucun moment) puis le nouveau fil de l'éperon.

R5 sur 1 piton + friends.

### L6 -- 4b, corde tendue

Suivre le fil jusqu'au pied du ressaut terminal (3b). Repérer sur la droite une vire en éboulis qui permet de traverser en versant nord sur 10 m, se rétablir (2 pitons) sur une vire en ascendance à gauche ramenant sur le fil, et atteindre le sommet par un petit ressaut-cheminée (4b, 1 piton).

## Descente (3 h 30 min)

De façon générale, le retour demande beaucoup d'attention : on cotoie sans cesse le vide et l'itinéraire est très exposé.

Repérer versant sud, 5 m en contrebas, un relais sur cordelettes (évident à l'arrivée au sommet) ; le rejoindre en désescalade. Faire un rappel de 50 m sur le fil de l'arête SE jusqu'à son pied ; relais intermédiaire possible à 25 m (à consolider, ne pas faire confiance, préférer une désescalade facile versant Vénéon).

Suivre l'arête en très bon rocher jusqu'au col de Pié Bérarde (passages de 2b) : le plus facile et le plus rapide est de suivre le fil et non d'emprunter des vires improbables versant sud. Peu avant le col, l'arête présente un passage très effilé ; repérer en contrebas une vire évidente en versant sud (main droite) que l'on rejoint et suit facilement, et descendre au plus facile sur le pierrier au pied de l'arête.

Descendre facilement dans la combe suspendue en tirant plutôt rive gauche (cairns discrets). À la naissance de la gorge de la Ruinette, viser une croupe herbeuse en rive gauche (cairns). Suivre le sentier serpentant entre les barres (logique et facile mais toujours exposé au-dessus des barres). À la fin, le chemin disparaît dans la végétation et les éboulis, qu'il faut descendre au mieux pour rejoindre le chemin au fond du vallon du Haut Vénéon.
