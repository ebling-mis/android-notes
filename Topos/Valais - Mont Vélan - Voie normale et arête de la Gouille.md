---
created: 2023-07-06T21:33:00+02:00
modified: 2023-07-06T23:31:59+02:00
---

# Valais - Mont Vélan - Voie normale et arête de la Gouille

## Cotations

Cotation globale : PD  
Engagement (échelle C2C) : II  
Équipement en place (échelle C2C) : P3

## Ambiance

Premier parcours partiel de l'arête de la Gouille (haut de l'arête à la descente) : 3 septembre 1872, par Hans Baumann, Daniel Bich, H. J. et Walter Leaf.

## Itinéraire -- environ 5h

Départ entre 4h30 et 5h30 du matin.

De la cabane du Vélan, s'élever sur la moraine ouest du glacier de Tseudet (suivre le tuyau d'amenée d'eau de la cabane, cairns), puis prendre
pied sur le glacier peu avant 2700m et s'élever de 100m supplémentaires avant d'effectuer la traversée en direction du mont de la Gouille.
Attention, il est important de rester au plus bas, car les chutes de séracs de la face nord du Vélan projettent des débris sur une distance
impressionnante.

Remonter la rive droite du glacier sur des éboulis instables en direction du col de la Gouille (3150m). Le passage vers le col se situe à droite
des 2 couloirs d'éboulis issus du mont de la Gouille. Le passage est équipé d'une échelle, puis de chaînes fixes.

Du col, deux options :

 * **Voie normale** (2h, P1): Monter le glacier rive gauche en prenant soin d'éviter de s'exposer aux séracs proches de l'altitude 3180m, puis
   accéder au plateau supérieur. De là, il est judicieux de se diriger vers l'arête de la Gouille par une courte traversée sur la droite entre
   2 systèmes de crevasses. Puis, soit prendre l'arête de la Gouille vers 3500m (plus intéressant et plus court), soit tirer plein sud et contourner
   le rognon sous le sommet.
   
 * **Arête de la Gouille** (3h, P3): Suivre l'arête de rochers brisés et instables, d'abord peu redressée et s'affinant peu à peu. Rester au maximum
   sur le fil où les rochers sont le moins instables. Atteindre une brèche (belle vue sur la paroi nord) par deux séries de pas de désescalade, le
   dernier équipé d'une chaîne, puis rejoindre des rochers de la bosse rocheuse (3494m) d'où part un éperon en direction du glacier de Valsorey.
   L'arête se termine peu après, on arrive sur des pentes de neige vers 3580m. Remonter ensuite sur une pente facile (glacier, quelques crevasses)
   jusqu'à environ 3640m, rejoindre l'arête enneigée nord-nord-ouest du Vélan (légèrement dessinée) et la suivre jusqu'au sommet.

## Descente -- 3 à 4h

Par la voie normale. Du sommet, partir bien à droite sur l'arête sur 200m avant de descendre facilement sur le glacier.

Pour remonter sur le col, repérer un gros cairn à gauche du glacier et une chaîne bien visible plus haut, la suivre (plus facile que de l'autre côté).
