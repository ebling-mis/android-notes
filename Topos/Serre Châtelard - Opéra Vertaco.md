---
created: 2022-07-09T18:31:00+02:00
modified: 2022-11-12T17:37:49+01:00
---

# Serre Châtelard - Opéra Vertaco

## Cotations

Cotation globale : TD  
Cotation escalade : 6b (6a obligatoire)  
Engagement (échelle C2C) : II  
Équipement en place (échelle C2C) : P1

## Ambiance

La dernière voie équipée à Serre Châtelard : une ouverture en solitaire riche en
sensations. La voie se faufile au mieux dans le dévers en suivant le meilleur
rocher. Mais nous sommes à Serre Châtelard, et le rocher reserve ses surprises...

Prévoir au minimum 14 dégaines, dont quelques longues pour gérer le tirage dans
L1 et L5.

Ouverture : juin 2012, par Christophe Guier.

## Approche -- Par le haut -- 1h30

Prendre la D2 qui part de St-Laurent-en-Royans et monte sur le Vercors pendant
7 km jusqu'à l'intersection **finale** avec la route forestière de Serre-Mouchard,
vers 1000 m d'altitude. Descendre
sur quelques mètres la route forestière et se garer à gauche dans une épingle juste avant
la barrière.

De là, suivre la route forestière de Serre-Mouchard (la petite sente en pointillés sur la carte qui permet de couper les détours
de la route forestière est un peu bartasse). Passer la deuxième barrière et continuer la route
forestière encore 200 m jusqu'à la borne jaune "Serre Mouchard 925m" (environ 20
à 30 min de marche).

On voit à gauche de cette borne un chemin forestier (GR rouge et blanc). Prendre
ce GR vers l'ouest sur 10 m seulement ! Prendre à gauche, entre deux troncs
morts, une sente marquée de quelques cairns, que l'on en traversée à droite pendant
10 minutes, pour arriver au bord de la falaise. Longer la falaise à gauche sur
environ 20 à 60 m et on tombe sur un cairn et un bâton indiquant la ligne de rappel.

Effectuer 3 rappels :

 * Rp1 : 25 m - Tout droit (on arrive légèrement en fil d'araignée)
 * Rp2 : 35 m - Légèrement décalé à droite (en regardant le rocher)
 * Rp3 : 35 m - Jusqu'en bas.

Possibilité de coupler Rp1+Rp2 avec une corde 2×60m.

Au pied, longer longuement (15 min) la paroi vers la gauche (en regardant le rocher), la sente descend dans les buis, puis remonte au pied du rocher qu'on longe encore. La voie se situe bien après la voie _Newton_ (au moins aussi loin que la distance entre les rappels et _Newton_), et avant la _Voie du dièdre_.
Le départ de la voie est assez peu visible et on peut facilement passer à côté. Il se situe après une zone de rocher jaune-orange déversante au pied duquel le sentier forme une large plate-forme. Au moment où la plate-forme redevient sentier et descend très légèrement, repérer un cairn soigneusement construit contre la falaise. Contourner un rosier pour trouver le pied de la voie, où un gros galet a été collé avec le nom _Vertaco_ dessus.
La voie est équipée sur plaquettes (4 plaquettes visibles depuis le départ).

## Voie

### L1 -- 6b+

Un départ 6a fin en légère traversée vers la droite, puis une suite physique pour arriver à une traversée vers la droite (crux) qui amène dans un petit dièdre que l'on remonte jusqu'en haut de l'éperon (rocher moins bon sur le côté droit du dièdre, mais on reste plutôt à gauche).
C'est la longueur la plus difficile, mais bien protégée et A0 possible sur tout le pas physique et la traversée.

R1 confortable au sommet de l'éperon.

### L2 -- 6a

On commence par du 4 dans du rocher réactif (rester bien en-dessous des points), puis une jolie fissure-dièdre où il faudra un peu de détermination (équipement plus aéré). Possibilité de compléter l'équipement par des friends C4 #1.

R2 peu confortable (plein gaz) au niveau d'une étroiture de la fissure-dièdre.

### L3 -- 6b (1 pas)

On quitte rapidement la fissure-dièdre qui devient plus difficile pour partir à droite dans du beau gris. Un pas un peu bloc fait la cotation (possibilité de A0), puis on remonte rejoindre l'écaille bien visible à droite (un pas obligatoire, ça engage un peu).

R3 confortable.

### L4 -- 6a+

Tout droit au-dessus du relais, puis un pas pas facile légèrement déversant et peu prisu (passer à gauche). Sortie magnifique sur gouttes d'eau dans du rocher gris sculpté, longueur plutôt courte.

L4 confortable.

### L5 -- 6b

Tout droit sur du crépi avant la traversée (1 pas fin) dans du beau rocher orange. Traverser un peu au-delà du dernier point et sortir plutôt sur la gauche des points.

Sortie sur gouttes d'eau superbe.

R6 juste sous la crête.

## Descente

Le retour se fait à pied par les crêtes, où une sente a été taillée le long du bord de la falaise (15 à 20 min). On retrouve le sentier de l'approche.
