---
created: 2024-03-23T19:16:00+01:00
modified: 2024-03-25T07:02:00+01:00
---

# Archiane - A Thor et en travers

## Cotations

Cotation globale : D+  
Cotation escalade : 5c A0 (5c A0 obligatoire)  
Engagement (échelle C2C) : II  
Équipement en place (échelle C2C) : P2

## Ambiance

Très longue traversée (400m à l'horizontale pour 70m vers le haut !) qui
exploite la ligne de faiblesse de la falaise surplombant Archiane.

Prévoir une dizaine de dégaines, dont au moins 5 rallongeables, un jeu de
friends (0.4-2 minimum) et des câblés. Quelques sangles pourront être utiles,
notamment dans la longueur de sortie en A0. Le matériel de remontée sur corde
est indispensable en cas de chute du leader autant que du second.

Ouverture : printemps 1998, par le club Densité (Dominique Benard, Christophe
Raillon, Didier Allabert, André Tardieu et Manu Ibarra).

## Approche

De Bénévise, suivre le GR direction Archiane. À la bifurcation, prendre le chemin
montant au belvédère (45 min).

Le belvédère domine une gorge. Aller chercher le sommet de la gorge à droite, et
y descendre par un couloir terreux instable (cairns ?). Plus bas, une corde fixe
permet de franchir un ressaut. Au pied de ce ressaut, suivre une vire rive droite
qui longe la falaise.

Après un passage étroit entre la falaise et les buis, on arrive sur une plate-forme
plus large et confortable : s'encorder ici.

## Voie

La voie se compose de 9 longueurs horizontales, suivies de 2 verticales.

### L1 -- 3c, 45m, 1pt

Très facile, mi-escalade mi-ramping. Relais sur deux points reliés par une cordelette.

### L2 -- 5c, 45m, 5pts

Gros aplats pour les mains et pas grand-chose pour les pieds. Longueur magnifique !

Relais sur deux points reliés, 2-3m plus bas.

### L3 -- 4c, 40m, 0pt

Un pas au début puis très facile. Relais relié au niveau d'un pilier.

### L4 -- 3c, 40m, 1pt

Très facile, corde tendue conseillée. On perd un peu le fil des longueurs et des relais.

### L5 -- 3c, 45m, 1pt

Toujours très facile, corde tendue conseillée.

### L6 -- 5a, 40m, 5pts

La difficulté n'augmente pas significativement, on peut rester corde tendue.

Relais au niveau d'un renfoncement avant de franchir un pilier impressionnant.

### L7 -- 5a, 40m, 3pts

Un pas expo au début pour contourner le pilier, puis plus facile. Relais sur
3 points non-reliés.

### L8 -- 4a, 40m, 2pts

Facile vers les arbres. Relais relié au niveau des buis.

### L9 -- 5a, 40m, 2pts

Début végétal, puis quelques mouvements qui s'enchaînent bien (pour franchir le
fil d'un pilier, monter un peu pour redescendre juste après).

Relais dans un renfoncement, assez exigü, juste avant l'aplomb d'un couloir.

### L10 -- 5c A0, 40m, 5pts

Partir en ascendance à gauche (1 piton visible du relais) et remonter une sorte
de couloir-cheminée en passant plutôt à gauche que dans la fissure refermée de
droite (1 piton visible au dernier moment, friends). On arrive sous un verrou
un peu déversant (1 corde sur arbre, 1 plaquette à droite du verrou). Franchir
le verrou en artif (pédale sur la plaquette, chasse d'eau au-dessus) et se
rétablir sur une dalle inclinée très peu prisue. Remonter le couloir encore une
quinzaine de mètres (très expo : pas de points, impossible à protéger) pour
trouver R10 à gauche.

### L11 -- 4c, 30m, 1pt

Remonter le couloir qui se couche et sortir à droite dans le cirque, ambiance
jardinage. Relais à construire sur des buis.

## Retour

Jusqu'à R8, possibilité de regagner le bas en un grand rappel. De là, longer la
paroi jusqu'à la gorge que l'on remonte. Les longueurs en traversée se déroulent
sous un bombé (pas trop mouillées) mais les 2 longueurs de sortie peuvent être
bien humides après la pluie : si vous ne pouvez pas passer il faudra refaire la
dernière traversée en sens inverse pour faire les rappels.

Du sommet, on rejoint le sentier en traçant son chemin dans la forêt, pentes
d'éboulis. Traverser le haut de la gorge et rejoindre le belvédère.
