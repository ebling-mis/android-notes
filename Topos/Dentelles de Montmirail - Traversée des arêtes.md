---
created: 2022-09-30T16:13:00+02:00
modified: 2022-10-03T20:19:19+02:00
---

# Dentelles de Montmirail - Traversée des arêtes

## Cotations

Cotation globale : D-  
Cotation escalade : 5b (5a obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1 - P3

## Ambiance

Parcours de la crête sommitale de la chaîne de Gigondas dans le sens ouest-est, en enchaînant les _Florets_, le _Turc_ et la _Pousterle_.

Première traversée : 1928, par B. Favret, Faye, Jacques Lagarde.

## Approche -- 1h

Attention, la piste d'accès au parking du Cayron est fermée du 1er mai au 30 septembre, il faudra se garer plus bas et prévoir 15min d'approche en plus.

Du parking du Cayron, suivre la piste principale vers l'ouest en direction du col d'Alsau. Lorsque celle-ci tourne vers le sud (tables de pique-nique en bois, table d'orientation plus haut), prendre à gauche une sente balisée de points bleus qui monte vers la crête.

Suivre cette sente en direction sommet de Tête Vieille (sans forcément y monter), puis longer l'arête vers l'ouest. L'attaque se situe au niveau d'un col débonnaire.

## Traversée des _Florets_ -- 3h à 5h

Belle escalade, très bien équipée (P1). On franchit successivement les 6 ressauts de la crête des Florets.

### L1 -- 5b, 30m

Du départ, on voit un gendarme 15m plus haut, entouré d'un câble et d'un maillon : c'est le premier relais de rappel.  
Droit vers ce gendarme dans de petits blocs, puis un pas en dalle.

Rappel de 15m en face sud (sur la droite et avec un anneau de métal rond passé dans le double câble).

### L2 -- 5a, 20m

Monter dans une grande fissure peu raide, belle longueur. Relais chaîné.

### L3 -- 4c, 25m

Suivre l'arête qui se couche, jusqu'à un relais chaîné un peu en contrebas.  
Possibilité de coupler L2 et L3 en 50m, relais sur deux plaquettes avant de redescendre.

Rappel de 10m. Attention à ne pas descendre en rappel dans l'arche en-dessous, sinon on rate le gendarme suivant !

### L4 -- 4c, 15m

Passer en versant sud sur quelques mètres, puis droit dans une fissure. Relais chaîné derrière la pointe.

Rappel de 10m.

### L5 -- 3b, 30m

Dalle et fissure. À la grande terrasse sommitale, passer sous les arbres pour trouver un relais chaîné à droite de la "fenêtre".

Rappel de 20m.

### L6 -- 5b+, 15m

Dalle avec fissure à sa droite. Départ un peu fin, belle longueur. Relais chaîné de rappel, ou bien 4 plaquettes un peu sur la gauche.

Rappel de 20m.

### L7 -- 5a, 20m

Dalle inclinée à prendre plutôt sur le côté droit. Relais sur 2 plaquettes au sommet.

### L8 -- 4b, 50m

Suivre le fil de l'arête pour cette longueur plutôt descendante.  
Quelques points, possibilité de rajouter des sangles sur béquets.

Relais chaîné.

### L9 -- 3b, 30m

Descente un peu raide pour atteindre la brèche du Turc. Pas de point, rappel possible sur relais chaîné ou désescalade contre-assurée (25m).

On arrive à une première brèche. Poursuivez vers l'est par les sentiers en versant sud jusqu'à atteindre le pied de l'arête ouest du roc du Turc (non équipée).

### Échapatoire

La **brèche du Turc** est en réalité une succession de trois brèches bien marquées sur une trentaine de mètres. La plus à l'ouest correspond au pied de L9, elle est impraticable. Dans la brèche du milieu, possibilité de descendre en désescalade versant nord pour rejoindre les sentiers (option à préférer ?). Dans la brèche la plus à l'est, on peut tirer un petit rappel versant nord (10/15m) sur une énorme lunule patinée (sur la gauche en regardant côté nord) ; attention à ne pas coincer la corde !
On rejoint ainsi directement le sentier en face nord, que l'on suit tout d'abord vers la gauche (ouest), puis qui part sur la droite (est) en longeant le pied des falaises.

## Traversée du _Turc_

Course d'arrête moins équipée (P1+, parfois TA), moins de grimpe.

Accéder au sommet du **roc du Turc** :

 * soit en suivant l'arête ouest (fil) : 4c non-équipée, rocher correct, végétation.
 * soit en face sud, à droite de la _chambre du Turc_ : escalade facile équipée en 3c/4a
 * soit en face sud par la _chambre du Turc_ et sa chatière : randonnée pitoresque avec quelques plaquettes
 * soit en redescendant versant nord pour rejoindre le départ de la _Dülfer_ : caillou avec le nom de la voie au départ.

### _Dülfer_ L1 -- 5b+, 25m, P1

Départ dans une dalle, puis rejoindre un dièdre. Traverser un peu à gauche pour atteindre R1.

### _Dülfer_ L2 -- 5b, 25m, P1

Droit dans une belle fissure.


Du sommet du roc du Turc, suivre un sentier versant nord, sur une vire juste sous la crête, jusqu'à son extrémité (relais de sortie du _Marchand de cailloux_, 3 long. 5b+ P1+).  
Par un petit pas en traversée, gagner une brèche et monter facilement au sommet suivant, le **roc du Trou**.

Descendre facilement jusqu'à un large col, le **col de la Voie des Vires** : échapatoire possible par les vires versant sud, ou bien par un rappel de 50m versant nord, un peu à l'ouest du col à proprement parler.

Gravir un petit éperon en bon rocher (5a, goujons), descendre 1-2m versant sud pour suivre en descente un cheminement facile (broches) sous la crête jusqu'à une large dépression où l'on peut descendre en face sud facilement. Échapatoire possible versant nord sur les relais de _Rien ne va_ depuis la dépression.

Pour continuer :

 * soit on suit l'arête très effilée (goujons, pitons, chaînes)
 * soit on prend un couloir à droite.

Passer le sommet du _Dièdre des Parisiens_ (2 long. 5c P1), descendre face sud (rocher très moyen) jusqu'à une brèche avec une arche (relais de sortie de _Régina directe_). Remonter un peu puis contourner l'arête par une vire au sud (rocher très moyen) jusqu'à un gros arbre mort.

D'ici, deux solutions :

 * soit descendre un petit ressaut 5m au sud jusqu'à une chaîne (rappel de 25m), rejoindre le sentier bleu versant sud et le suivre jusqu'à la brèche de la Pousterle,
 * soit remonter sur l'arête (2 goujons, flèche bleue) et rejoindre la croix au sommet de la Pointe Claire. Descendre versant nord dans _Vibration_ (2 rappels, 50m + 35m).

## Traversée de la _Pousterle_

Course d'arête, équipement partiel (P3).

De la **brèche de la Pousterle**, suivre l'arête ouest par quelques pas de 5a, passer une première brèche puis accéder à une seconde brèche par une désescalade délicate (chaîne). Rappel en face sud dans la voie des Guides (30m).
