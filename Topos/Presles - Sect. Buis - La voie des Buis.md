# Presles - Sect. Buis - La voie des Buis

## Cotations

Cotation globale : D+  
Cotation escalade : 6a (5b obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1+

## Ambiance

La première voie ouverte sur les falaises de Presles. C'est LA grande classique historique.

Prévoir 12 dégaines (rallongeables pour la plupart), quelques sangles et éventuellement des friends moyens (camalot 0.5, 0.75 et 1) pour rassurer sur quelques passages.

Ouverture : 1953, par Pourtier et Taillefer. Rééquipement en 1991.

## Approche -- 30min

Depuis le parking du plateau de Presles, redescendre par la RD292 en direction de Pont-en-Royans.
Après le premier virage à droite et avant le tunnel, prendre un bon sentier à gauche
permettant de couper le virage des Roches du Nugues. Descendre 100m, traverser la route,
couper de nouveau le lacet par un raide sentier (corde). Prendre à droite sur la route
pendant 20 m pour rejoindre à gauche un sentier (panneau).

Suivre ce sentier sur 500 m. Il descend, remonte et se sépare en deux, à gauche du
pilier SW. Le sentier de droite descend en direction de Choranche. Prendre le sentier
de gauche qui remonte en direction de la paroi. Rejoindre la paroi juste avant la vire
aux chèvres.

Le départ de la voie est indiqué : _Les Buis_ sur une pierre à 4 m du sol (1er spit
peu visible).

## Voie

### L1 -- 4b

Petit mur en 4b (1 spit), puis traversée ascendante à gauche (3c). On arrive au relais d'une autre voie, monter encore en oblique quelques mètres puis suivre la vire à gauche sur 10m environ jusqu'au pied d'un dièdre (inscription bleue _R1 Buis_).

### L2 -- 4c+

Dièdre soutenu dans la cotation. Un pas délicat au départ, puis on remonte une cheminée
en 4 avec des points bien espacés.

Le relais se trouve 5m à droite de la sortie du dièdre, il faut vraiment traverser
horizontalement, plein gaz mais facile (inscription bleue _R2 buis_).

Le pilier dans l'axe du dièdre est en 6a, il s'agit de L3 de _Kit ou double_.

### L3 -- 5c

Dans un dièdre. Soutenu et patiné mais bien équipé (5c/A0 possible au départ, c'est le pas
dur, puis 5b). Relais à gauche sur un pilier à la sortie du dièdre. Très belle longueur,
patine pas gênante.

### L4 -- 4b

Monter jusqu'à la vire médiane (4b, 1 point). Suivre celle-ci à gauche sur environ 20 m (2a).
Croiser une voie peu après le début de la traversée, passer quelques buis (dont un gros) et
continuer encore jusqu'à un dièdre ouvert gris.

Relais sur 2 goujons + 1 vieux piton + encore un goujon, inscription bleue.

### L5 -- 5b

Dièdre soutenu mais bien équipé, petit crochet à droite vers le milieu, bonnes prises mais du placement. Belle longueur également !

Le relais est à gauche.

### L6 -- 5c+

Petit mur en 4, puis quelques gradins. Courte dalle fissurée (un friend de taille Camalot 0.75
permet de réduire l'exposition, mais un spit a été ajouté récemment). Remonter alors la
cheminée raide (écarts/ramonage, mais en prenant en ramonage c'est assez facile).

Relais sur la droite à la sortie de la cheminée.

### L7 -- 5c

Fissure soutenue en restant dans l'axe, les points sont parfois un peu loin. Départ sur la droite, puis revenir très vite dans l'axe.

Variante "montagne" à droite en plein dans les buis, 4c P2 avec des sangles.

Gradins facile pour sortir, relais sur deux points légèrement à droite.

### L8 -- 4b

Rejoindre au-dessus un couloir de buis puis le remonter (pas d'équipement mais nombreuses
traces de passage). Au bout du couloir, remonter en traversée ascendante à gauche (4 un peu
expo, 3 points). Quelques sangles sur les buis et becquets et des friends permettent de diminuer l'engagement.

Relais à gauche au niveau d'une plaque commémorative

### L9 

Sortir par une dernière longueur facile dans des gradins.

### Remarques

En cas de doute sur l'itinéraire, la patine est une bonne indication de la direction à suivre.

## Descente -- 15 min

Du sommet, prendre la ligne de faiblesse de la forêt et du rocher (assez évident) pour arriver à
une clairière où se situe un cairn. De toute façon, prendre une direction NNW ; on trouve diverses
sentes qui ramènent à une clairière et de là, à la D292.
