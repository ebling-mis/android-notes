---
created: 2021-07-30T21:27:38+02:00
modified: 2021-08-01T21:08:03+02:00
---

# Saou - Les Trois Becs - Arête des Débrouillards

## Cotations

Cotation globale : D-  
Cotation escalade : 5b (5a obligatoire)  
Engagement (échelle C2C) : II  
Équipement en place (échelle C2C) : P1+

## Ambiance

La grande voie la plus facile des Trois Becs. Elle présente un développement de près de 200m pour environ 100m de dénivelé. Sans être vraiment du Terrain d'Aventure, l'ambiance est montagne : le rocher est globalement correct, mais il faudra être prudent aux blocs branlants. D'une manière générale, il faut vérifier chaque prise avant de l'empoigner. Le rocher est excellent dans L7, qui est la plus belle longueur de la voie.

La voie est partiellement équipée, l'équipement en place pourra cependant être complété dans la plupart des longueurs pour réduire l'exposition. Dans la partie moins verticale de l'itinéraire (L3 à L6), il faudra ajouter quelques sangles et protections amovibles.

Attention à ne pas faire tomber de blocs sur les grimpeurs situés en-dessous. Si des cordées sont
au-dessus, la plus grande prudence est de mise.

Ouverture : 4 octobre 1998, par Manu Ibarra et André Tardieu.

## Approche -- 1h

Du parking sous le hameau des Auberts, descendre la route sur 20 m, prendre à
gauche la piste (GR 9). Après 10 minutes, quitter la piste horizontale pour
prendre le sentier qui s'élève sur la gauche (toujours le GR 9), marcher pendant
40 min environ jusqu'à atteindre un pierrier (sur la droite, longeant la paroi),
le traverser au niveau d'un gros cairn et descendre jusqu'à un accès évident à
la paroi (cairn, goujon à ~8m du sol), proche du pied de l'arête (la partie la plus
basse est un peu végétale et donc l'attaque n'est pas tout à fait au pied).

## Voie

### L1 -- 4c, 25m, pitons

S'élever sur le fil de l'arête en suivant les pitons, avec passage côté Nord de
l'arête.

**Attention** aux rochers qui bougent !

R1 sur 1 piton + 1 goujon.

### L2 -- 4c, 25m, pitons

Idem L1.

R2 sur 1 piton + 1 goujon.

### L3 -- 3c, 45m, rares pitons

L'arête devient moins verticale, elle est pauvre en pitons.

R3 sur 1 piton + 1 goujon.

### L4 -- 3c, 35m, rares pitons

Poursuivre sur l'arête pauvre en pitons.

R4 sur 1 piton + 1 goujon.

### L5 -- 4c, 35m, rares pitons

Poursuivre sur l'arête pauvre en pitons.

R5 problématique : plusieurs topos parlent d'un arbre mort avec une sangle, non-trouvé sur place... On peut improviser un relais confortable après 40-45m sur un becquet.

### L6 -- 4c, 25m, pitons

Poursuivre sur et sous l'arête pauvre en pitons, plutôt côté Sud.

R6 sur 1 piton + 1 goujon au pied de la dalle finale.

### L7 -- 5b, 20m, pitons

Dalle à trous inclinée bien équipée en pitons. La progression se fait en diagonale vers la droite, attention à ce que l'on touche à la sortie de la dalle.

R7 sur 2 pitons reliés pour le rappel.

## Descente

Rappel de 25m environ depuis R7 côté Sud, ou bien désescalade facile côté Sud.

Au pied du rappel, traverser en haut du pierrier pour rejoindre le sentier sous le pas de la Motte.
