---
created: 2022-07-04T05:39:00+02:00
modified: 2022-07-05T06:48:51+02:00
---

# Aiguilles de l'Argentière - Aiguille de la Combe - Déboires et Convictions

## Cotations

Cotation globale : D+  
Cotation escalade : 6a (5c obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1

## Ambiance

Une voie très agréable où le pas le plus dur est tout en bas. L'équipement est abondant sur goujons et tous les relais sont chaînés (!).

Ouverture : 15 septembre 2005, par Eric Aubert et Marc Malvotti.

## Approche -- 2h

Du col du Glandon, partir à l'ouest puis bifurquer rapidement à gauche pour contourner la bosse du Carrelet par le sud et rejoindre la ruine qui précède le chalet de la Combe. Quitter alors le sentier qui continue vers le lac de la Combe pour remonter la croupe herbeuse formant la rive droite du vallon qui mène au col de la Combe. Remonter au mieux cette croupe (cairns) qui permet d'éviter le fond du vallon et son pierrier malcommode.

Arrivé au sommet du vallon, traverser le pierrier sous le col de la Combe. La voie remonte un pilier menant à une antécime de l'aiguille de la Combe, première pointe à l'Est du col de la Combe. L'attaque de la voie se situe dans du rocher pourpre (petits surplombs), à gauche d'une grande dalle claire inclinée, au pied d'un couloir un peu herbeux issu de la brèche de la Combe, au nord-est du col de la Combe.

Un cairn permet de repérer l'attaque de la voie, la confusion étant possible avec la voie _Les visiteurs du soir_ (TD 6c>6a I P1), située 20m plus en amont.

## Voie

### L1 -- 6a, 45m

Mur raide avec aplats et prises fuyantes, assez physique. Une traversée fine vers la droite sur les pieds, puis des gradins faciles.

### L2 -- 5b+, 50m (12 dégaines dont 2 rallongeables)

Très grande longueur, en traversée vers la droite, toujours bien raide. Un petit crochet vers la gauche permet de trouver le relais sur une plate-forme. Attention au tirage !

### L3 -- 5c, 50m

Un pas pour franchir un ressaut à gauche de R2, puis on remonte un petit pilier pour rejoindre le fil de l'arête (facile). On vient buter contre un mur rouge : contourner le surplomb par la gauche (5c). Attention au tirage !

### L4 -- 5b, 25m

Droit vers un mur lichéneux puis une fissure raide (un pas en 5c ?). Relais peu confort sur un replat.

### L5 -- 4c, 30m

Droit sur le fil du pilier jusqu'au sommet (lichéneux, mais grosses prises). Quelques blocs sont instables.

## Descente (3 h 30 min)

La voie aboutit à une antécime (2730m) de l'aiguille de la Combe.

Le retour se fait par la face ouest (voie normale) par une descente un peu exposée, mais facile, à condition que le col de la Combe soit déneigé. Rejoindre l'arête ouest de l'Aiguille de la Combe et la descendre au mieux par des gradins jusqu'à un cairn (sangles en place pour un rappel, pas nécessaire). Franchir la petite brèche formée par le cairn et descendre par des pas de désescalade facile, jusqu'à une rampe descendante vers la droite. Au bout de la rampe, remonter légèrement pour atteindre la brèche de la Combe.

Descendre le couloir sud-est de la brèche de la Combe, comportant quelques petits ressauts faciles et des éboulis instables sur 50m. Rejoindre le pied de la voie, puis le col du Glandon.

Il est également possible de descendre en rappel dans la voie ; tous les relais sont équipés de chaîne et maillon.
