---
created: 2023-08-18T13:16:00+02:00
modified: 2023-08-19T16:36:29+02:00
---

# Vanoise - Pointe de Labby - Arête du Soleil (N)

## Cotations

Cotation globale : AD  
Cotation escalade : 4c (4b obligatoire)  
Engagement (échelle C2C) : II  
Équipement en place (échelle C2C) : P1

## Ambiance

Très joli itinéraire sur un rocher excellent, accessible et entièrement équipé (55 spits). La vue du sommet de la pointe de Labby est magnifique.

On peut, au choix, tout faire corde tendue, se protéger sur coinceurs et même tirer des longueurs pour ceux qui veulent, les relais sont en place. Prévoir 5 dégaines pour tirer des longueurs, sinon une dizaine pour avancer corde tendue.

Premier parcours : 29 août 1901, par J.-A. Favre, S. Gromier et Henri Mettrier.

## Approche -- 3h

Depuis le refuge de la Dent Parrachée (ou celui de Fond d'Aussois), rejoindre le **lac du Génépy**, que l'on contourne par la gauche.

Prendre pied sur le glacier de Labby et le remonter en visant une pointe non nommée située entre le col de Labby et le col du Moine. Le col de Labby n'est visible que très tardivement.

Arrivé sous des barres rocheuses, obliquer à gauche sur une langue de neige entre les rochers (environ 40° sur 20m).
Lorsque le passage est sec, c'est un dièdre facile équipé d'une corde fixe et marqué de points blanc/jaune.
Traverser ensuite vers la gauche au-dessus d'une barre rocheuse jusqu'au **col de Labby**.

Du col de Labby, faire un mouvement circulaire ascendant en frôlant la pointe de Labby. Gagner par une traversée ascendante dans une pente raide le **passage de Rosoire** (environ 40° sur 50m).
Lorsque le passage est sec, ce sont des gradins faciles équipés de cordes fixes dans les passages plus délicats.

## Arête du Soleil -- 2h à 3h30

Du passage de Rosoire, suivre au mieux le fil rocheux de l'arête vers le sud sur 100m et traverser un petit col, éventuellement neigeux, pour atteindre le début des difficultés.

Attaquer par un mur raide de 5m (4b), qui dépose sur une vire ocre versant est, **ne pas la suivre** mais traverser immédiatement le fil et suivre au mieux le fil versant ouest.

Au pied du ressaut suivant, suivre une vire versant est pour gravir une fissure large sur 10m (4b).
Poursuivre par une courte dalle versant ouest. Rejoindre le fil et une courte vire versant est (anneau de corde rose pale).
De là, on distingue 20m plus loin sur l'arête un mur délité jaunâtre versant ouest, témoignant d'un éboulement que l'on évite.
Suivre le fil de l'arête sur 10m seulement, jusqu'à un nouveau relais versant est (2 plaquettes non reliées avec 1 maillon et un anneau de corde).
Faire un **rappel de 25m** versant est jusqu'à une dalle ocre/rouille déversée (relais, 10m au sud de l'aplomb du rappel).
Traverser cette dalle à l'horizontale sur 30m (4 plaquettes, mais premier point invisible du relais).
Traverser un court dièdre pour rejoindre la base d'un couloir (relais). Le remonter par des dalles puis des éboulis jusqu'à la brèche au pied de la première pointe.

Traverser de quelques mètres versant ouest, gravir la première pointe par une belle cheminée (10m, 4c) et sortir sur le fil que l'on poursuit jusqu'à un **rappel (20m)**, qui dépose à la brèche au pied de la deuxième pointe. La première pointe peut s'éviter par le versant ouest.

Traverser horizontalement versant ouest sur 25 m pour attaquer la deuxième pointe par une dalle (10m, 4b) et passer une brèche à droite, 5m sous le sommet du ressaut (relais).
Désescalader versant est (3m, corde à nœuds), poursuivre sur la vire, repasser versant ouest par une petite brèche jusqu'à un **rappel de 10m** (ou désescalade).
Continuer sur le fil, passer une pseudo boîte aux lettres puis désescalader sur 3m. Reprendre le fil et **rappel de 10m**.

Après un dernier passage en traversée (4b) dans une brèche, atteindre facilement le sommet de la pointe de Labby.

## Descente

La descente se fera par la voie normale de montée, c'est-à-dire l'arête sud-ouest de la pointe de Labby.

Prendre une sente versant ouest qui débute quelques mètres au nord du sommet et rejoindre l'antécime sud-ouest.

Contourner cette antécime par la droite et emprunter sa croupe sud-est jusqu'au col de Labby.
