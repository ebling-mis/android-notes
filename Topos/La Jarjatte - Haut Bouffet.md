---
created: 2021-09-24T19:07:38+02:00
modified: 2021-09-24T19:07:38+02:00
---

# La Jarjatte - L'Aiguille ou le Haut Bouffet

## Cotations

Cotation globale : D-  
Cotation escalade : 5b (5a obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1

## Ambiance

Voie d'escalade sans prétention, mais agréable et dans une cadre absolument magnifique.

Elle se situe sur la droite (Sud) de l'Aiguille du Haut Bouffet, remontant le pilier à
droite d'un couloir bien visible du bas. Sur les photographies aériennes, cette arête
se détache nettement du versant Sud du sommet.

Ouverture : dans les années 2000, par Yves Gaillard.

## Approche -- 1h30

Du parking situé au haut du domaine skiable de la Jarjatte, monter en direction du col
des Aiguilles, soit en prenant le GR94, soit en coupant par les pistes de ski.  
Quitter le GR à la sortie de la forêt, au niveau d'un cairn caractéristique situé à
côté d'un gros bloc ([photo](https://media.camptocamp.org/c2corg-active/1626887152_1089207369BI.jpg)).
Poursuivre en face en direction d'un pierrier, qu'une sente de chamois permet de
traverser, d'abord Sud, puis Sud-Ouest. On arrive bientôt sous un couloir ;
contourner les marnes par la langue herbeuse à droite, puis rejoindre une terrasse
rocheuse-herbeuse au sommet des marnes. On trouve un premier spit en haut de la
terrasse, qui marque le départ de la voie.

L'approche finale est raide et il faut être attentif (pentes herbeuses, accès à la terrasse
un peu exposé).

## Voie

### L1 -- 5a

Une dalle raide et un peu humide à laquelle succède une cheminée souvent humide. La suite
est plus fine, dans du 3 herbeux. Belle escalade.

R1 sur 1 spit avec sangle.

Petit rappel de 15 m (un peu suspendu à la fin) pour redescendre de l'autre côté.

### L2 -- 4b

On suit le fil, le rocher est un peu délité au début. De moins en moins raide, pour
finir à plat sur 10m.

R2 sur 2 points sur la droite, un peu plus bas.

### L3 -- 5b

Belle dalle bien équipée, puis de nouveau un peu d'arête facile.

R3 sur 2 points environ 10m avant le pied du pilier final.

### L4 -- 5a

Les points ne sont pas forcément visibles tout de suite. Poursuivre vers le haut
en restant sur le fil du pilier. 1 point (très) à gauche, puis la suite plutôt
légèrement sur la droite du fil, enfin revenir sur la gauche du fil pour trouver
R4, un peu suspendu et étriqué (2 personnes max). Attention au tirage !

R4 sur 1 spit avec sangle, un peu suspendu.

### L5 -- 5a

Poursuivre jusqu'en haut du pilier.

R5 environ 5-6m après la sortie, sur 2 spits.

### L6 -- 2

Sur l'arête, horizontal ; puis sortie herbeuse. Continuer dans les éboulis, puis
dans l'herbe vers le sommet.

## Descente

Du replat herbeux sous le sommet, descendre vers l'Est sur la croupe herbeuse
qui se rétrécit et devient petite arête. Ne pas descendre directement vers le
vallon des Aiguilles à gauche (Nord), il y a des à-pics ! Descendre dans les
gradins à droite et rejoindre un petit collet.

De là, descendre par une petite sente en lacets sur le vallon des Aiguilles.
Rejoindre le col des Aiguilles, puis le parking par le sentier.
