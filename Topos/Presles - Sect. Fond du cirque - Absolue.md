---
created: 2021-11-11T08:47:06+01:00
modified: 2021-11-12T18:46:10+01:00
---

# Presles - Sect. Fond du cirque - Absolue

## Cotations

Cotation globale : TD  
Cotation escalade : 6b (6a obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1

## Ambiance

Escalade soutenue dans la plupart des longueurs, principalement en fissure et cheminée
sauf dans la première longueur. La voie étant récente, le rocher n'est pas patiné.

L'équipement est bien rapproché. La voie étant relativement soutenue, un petit niveau
6a est conseillé même si les pas obligatoires sont peut-être seulement dans le 6a.

Photo du secteur : https://www.camptocamp.org/routes/56224/fr/presles-fond-du-cirque-absolue#swipe-gallery

Ouverture : 2005, par Bruno Fara.

## Approche -- Par le haut, rappels de Torquemada, depuis le Charmeil -- 2h

Depuis le Charmeil, suivre la route et se rendre à la Ferme Pernon (panneau Adrien à
droite). Se garer sur le parking aménagé juste avant la maison du gîte _Entre Ciel et
Pierres_ au bout de la route. Sur le sentier, deux enclos à passer : un avec un passage
canadien, l'autre sans rien du tout. Passer la borne météo et le chalet sur la droite.
Peu après, suivre, à gauche, la direction de _Torquemada_ et _Tatachat_ (triangle rouge,
cairn et panneau).

Le chemin monte dans une clairière parsemée d'arbustes, puis se sépare en deux (à droite
_Tatachat_, à gauche _Torquemada_). Prendre à gauche et continuer à suivre les triangles
rouges. Le chemin remonte à nouveau dans une clairière pour gagner la forêt. Arrivé à une
clôture avec barbelés, la suivre et la franchir lorsqu'elle fait un angle droit (passage
aménagé). Remonter alors entre des mélèzes. Au niveau d'un arbre brisé, prendre sur la
droite une sente descendante.

On arrive sous une petite barre rocheuse. Au niveau d'un éperon, 5m en contrebas, on trouve
le premier rappel sur un gros arbre (gros anneau métallique).

Descendre en 4 rappels dans l'axe (les relais sont légèrement sur la gauche pour les deuxième et troisième rappels), dont deux partiellement en fil d'araignée (attention
aux chutes de pierres sur les deux derniers rappels !).

Descendre ensuite le long de la paroi (à droite en regardant celle-ci) pendant 20m. Le nom de
la voie est écrit en bleu clair. La première longueur est commune avec _Torquemada_ sur
quelques mètres, et la laisse à droite pour revenir sur le relais de L1 commun aux deux voies.

## Voie

### L1 -- 6a

Belle dalle compacte, escalade soutenue. Relativement difficile à lire.

Variante à droite dans le dièdre : 5b (L1 de _Torquemada_).

R1 sur deux plaquettes, commun à _Torquemada_ et _Desirée_.

### L2 -- 5c+

Ne pas traverser à gauche _(Torquemada)_, ni à droite sous le toit _(Désirée)_, monter tout
droit, fissure bien prisue puis cheminée.

R2 sur trois plaquettes, deux d'entre elles sont reliées par une cordelette.

### L3 -- 6a

Fissure, et passage de 2 petits bombés. Longueur relativement courte.

R3 sur deux plaquettes.

### L4 -- 6b

Un dévers en 6b en début de la longueur (passe en A0), suivi de fissures en 6a.

R4 sur deux plaquettes.

### L5 -- 6b

Début de longueur en fissure déversante (6b conti, passe en A0), puis fissure (6a),
puis encore un peu de dévers pour finir (6a+).

R5 au niveau d'un arbre, sur plaquettes.

### L6 -- 6a

Large cheminée de 30m, tout en coincement. Tous les points sont sur la gauche, certains ne se voient qu'apres coup...  
Petite traversée fine à la fin sur la gauche, passe en A0.

Longueur déroutante, pas cohérente avec le reste de la voie.

R6 sur plaquettes, sur la gauche à la sortie.

### L7 -- 4

Facile. On arrive à 10m des rappels de _Torquemada_.

## Descente

À pied vers le Charmeil.
