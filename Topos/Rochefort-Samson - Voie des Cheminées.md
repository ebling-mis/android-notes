---
created: 2023-05-04T15:57:00+02:00
modified: 2023-05-04T15:57:00+02:00
---

# Rochefort-Samson - Voie des Cheminées

## Cotations

Cotation globale : D  
Cotation escalade : 6a (5b A0+ obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P2

## Ambiance

Voie de 8 longueurs, démarrant dans les gorges de la Combe d'Oyans, dans un style déroutant très montagne : 3 longueurs en cheminée (L1, L2 et L7), équipement très aéré, rocher parfois délité... bref, du Rochefort !

Première traversée : 1975, par R. Chèze, A. Parat et JM. Parat.

## Approche -- 10min

Du parking des Ducs, gagner l'entrée des gorges. Remonter les gorges, jusqu'à atteindre une grande cheminée caractéristique, avec une grotte au pied. Une petite plaque en métal indique le nom de la voie.

## Voie

### L1 -- 5a, 25m

Gravir la cheminée par son côté droit (1 plaquette, puis 1 piton). Il est conseillé de tomber le sac pour ramoner.  
Puis passer sur le pilier à gauche (2 plaquettes).  

R1 confortable, chaîné dans une grotte (attention aux chauves-souris !).

### L2 -- 5c, 25m

Gravir la cheminée verticale (il est vivement conseillé de tomber le sac pour la ramoner), bons points espacés. Sortir sur la gauche et remonter des gradins.

R2 chainé sur un mini pilastre.

### L3 -- 5b, 40m

Départ à droite (1 spit + 1 piton), puis au mieux dans l'axe pour rallier un piton à 20 m. Un passage bien raide en 5b/5c protégé par 2 pitons, puis des gradins instables (rocher interactif et végétation)

R3 inconfortable à établir sur une petite vire en complétant le spit avec maillon par un friend, ou bien sur le spit d'après avec les buis.

### L4 -- 5a, 3c, 35m

Gravir des gradins qui aboutissent à un sentier que l'on suit au mieux dans l'axe. Rocher interactif et végétation.

R4 chaîné dans une conque au pied de la paroi.

### L5 -- 3c, 40m

Partir sur la gauche sur une vire, puis revenir sur la droite par un large mouvement tournant. Points discrets, rocher interactif et végétation.

R5, chainé, est à peu près à la verticale de R4, sur une plate-forme.

### L6 -- 5a, 30m

Départ en dalle en ascendance à droite, puis dans l'axe avant de revenir à gauche. Rocher toujours moyen, végétation.

R6 chaîné sur une plate-forme bien visible au pied de la cheminée de L7.

### L7 -- 6a > 5b A0+, 45m

Longueur-clé. Gravir la grande cheminée à gauche, cheminement évident mais exposé, 5c.  
Lorsqu'on vient buter sous un toit, sortir à gauche (2 plaquettes, 3 pitons). Le passage est impressionnant, mais passe en A0.

Sortie dans du rocher interactif et des gradins terreux à gauche. Attention au tirage !

R7 chainé sur petite vire.

### L8 -- 5a, 30m

Facile dans des blocs fragiles, fissure puis belle dalle. Une longueur magnifique, mais trop courte !

R8 chaîné au sommet.

## Échapatoires

Descente en rappel dans la voie possible jusqu'à R2.

Depuis R6, possibilité d'éviter la cheminée de L7 et de rejoindre le secteur _Cathédrale_ en partant sur la droite, 4c/5a (?) à protéger.

## Descente

À pied, suivre la sente qui descend puis part à gauche en longeant le rocher, on arrive au sommet du _Pillier des Anciens_. Suivre la sente vers la droite, elle descend raide en forêt et mène à l'entrée des gorges.

Alternativement, depuis le sommet, partir à droite et rejoindre le haut de la _Voie Françoise_ pour une descente en rappel, ou bien gagner le secteur _Cathédrale_ et emprunter la Rampe.