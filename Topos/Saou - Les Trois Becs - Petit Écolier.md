---
created: 2022-06-20T20:19:00+02:00
modified: 2022-06-21T20:31:32+02:00
---

# Saou - Les Trois Becs - Petit Écolier

## Cotations

Cotation globale : TD-  
Cotation escalade : 6a (5c obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1+

## Ambiance

Voie magnifique, originale et variée, sur du calcaire gréseux et des silex. Raide et athlétique pour le niveau, avec un équipement plutôt aérien (mélange de spits et de pitons solides peu visibles) qu'il n'est pas toujours possible de bien compléter.

Ouverture : 2001, par Ibarra & Raillon. L7 et L8 par Ibarra & Nouailletas en août 2005.

## Approche (1h)

Suivre le sentier balisé qui monte au pas de la Siara. Après un grand cairn sur une plateforme, il monte un peu, puis part franchement à gauche pour traverser un couloir. Peu après, il pénètre en forêt. Repérer le départ d'une sente sur la droite, au niveau d'un arbre qui pousse contre un gros rocher dressé, au milieu du chemin (cairn).

Suivre la sente qui monte pour rejoindre le pied de la falaise. On longe alors la falaise jusqu'à une boîte fixée à la paroi en hommage à des alpinistes décédés. Immédiatement après, remonter des gradins herbeux dans un petit amphithéâtre pour accéder à la vire de départ (escalade facile 3c avec 1 piton et un relais en haut). Il est aussi possible d'atteindre la vire en contournant nettement son socle par la droite et en la récupérant ensuite.

La voie du _Petit Écolier_, comme celle de _Mr Brun_, utilise le pilier à l'est du sommet. La voie démarre à l'aplomb gauche du pilier (1 piton à anneau et un spit visibles).

## Voie

### L1 -- 5a, 30m

Remonter droit sur le pilier, en naviguant un peu pour passer au plus facile sur du bon rocher.

R1 confortable sur une petite vire.

### L2 -- 5c/6a

Tout droit (1 pas délicat), puis un petit crochet vers la gauche pour rejoindre un dièdre qu'on suit avant d'aborder une traversée impressionnante de gauche à droite (6a, 2 goujons). Arrivé sous un grand surplomb, le franchir directement par la petite faiblesse du toit (5c, physique, un goujon sous le toit).

Très belle longueur pas facile à négocier, louvoyante et soutenue : indéniablement le crux de la voie, surtout pour le moral ! Le passage en A0 n'est pas possible dans la section dure, qui ne se complète pas non plus : il faudra veiller à placer ses friends avant de se lancer dans la traversée (éventuellement #2 en haut du dièdre, puis #0.5 dans réglette de départ de la traversée). La sortie plein gaz est superbe !

R2 confortable sur une large vire, juste au-dessus du surplomb.

### L3 -- 5b/c

Départ en ascendance vers la droite (2 pitons), petit mur, revenir à gauche, petite terrasse (1 piton), monter droit au-dessus et franchir un petit toit (délicat, 1 spit visible au dernier moment le protège bien).

R3 droit au-dessus.

### L4 -- 5b

Succession de strates magnifiques, bien prisues, mais raides et un peu athlétiques. On arrive au niveau de la grande vire qui coupe le piler en deux. Faire relais sous la vire pour éviter les chutes de pierres si la corde frotte contre les gradins rocheux/herbeux de la sortie.

Rejoindre la vire et faire relais sur la vire, un peu sur la droite, sous une sorte de baume (surplombs) : une plaquette + friends, arbre non loin également.

### L5 -- 5b, 45m

Du spit, partir en ascendance à gauche, monter droit puis franchir un bombé par la gauche. Rejoindre une immense dalle à silex, magnifique, raide mais bien pourvue en prises. La fin de la longueur est peu protégée, mais possibilité de compléter avec des friends. Sortir plutôt sur la droite, en direction d'un dièdre évident.

R5 sur la droite au-dessus des surplombs.

Longueur vraiment majeure, dans un océan de silex de toutes les tailles, formes, coupants ou non... un régal !

### L6 -- 5a

Poursuivre dans la dalle à silex, en traversée vers la droite, pour rejoindre une cheminée-dièdre que l'on remonte. On rejoint la voie _Mr Brun_ ; la sortie est en terrain à chamoix (ressauts herbeux), impossible à protéger.

R6 dans une sorte de niche à droite du pilier, sous un dièdre raide.

### L7 -- 5c, 30m

Franchir droit au-dessus un mur raide, puis un dièdre. Attention, une grosse écaille jaune bouge bien aux environs du crux, il faudra s'en passer...

R7 avec deux pitons sur une plateforme, protégé des chutes de pierres de L8.

### L8 -- 5c, 30m

Ne pas prendre la traversée à gauche, mais continuer dans un dièdre athlétique (5c), plaquettes visibles. Basculer à gauche sur le fil du pilier. Le rocher se dégrade vers la sortie, peu protégée (4b) jusqu'au sommet de l'éperon.

R8 sur 2 goujons au sommet de l'éperon.

## Descente

Descendre légèrement vers le collet qui sépare le sommet du piler du Veyou, en direction d'une prairie suspendue raide (~50m).

Descendre cette prairie suspendue en traversée vers la gauche pour rejoindre, par une vire ascendante peu marquée, le sentier en versant sud du Veyou.

Suivre le sentier jusqu'au sommet du Veyou, puis redescendre par le pas de Siara.

## Remarques

En cas de réchappe en rappel depuis R2, il y a un relais de descente à la verticale de R2. Inutile donc de galérer à retrouver R1 qui est décalé horizontalement et en profondeur (car L2 comporte une traversée et passe un toit).

La sortie de la voie (prairie suspendue) est très exposée et ne pourra être envisagée que par temps sec et en l'absence de neige. Sinon, il faudra prévoir piolet et/ou crampons...
