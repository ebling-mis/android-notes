---
created: 2022-08-04T18:20:00+02:00
modified: 2022-08-04T18:45:00+02:00
---

# Rocher de l'Épenet - Tourniol - La Seigneur Valençot

## Cotations

Cotation globale : D+  
Cotation escalade : 6c (5c obligatoire)  
Engagement (échelle C2C) : I  
Équipement en place (échelle C2C) : P1

## Ambiance

Voie historique typique du Rocher du Roi Gros-Nez : le rocher est tout à la fois compact
et fragile !

Ouverture : 23 avril 1967, par Y. Seigneur et F. Valençot. Rééquipée en 1993.

## Approche -- 15min

Depuis la dernière épingle de la D149 qui monte au col du Tourniol, emprunter un sentier
qui monte assez directement, en traversant plusieurs pierriers. À mi-chemin environ, à une
bifurcation peu marquée dans un pierrier, prendre la branche de droite qui amène au pied
de la falaise. Longer le rocher vers la gauche (nord) et passer _Un siècle et demi_ et
son départ abominable. Une plaque marque le départ de _La Seigneur-Valençot_.

## Voie

### L1 -- 6c

Départ un peu exposé (mais protégeable : friends + lunule), puis on arrive à un premier
surplomb (rocher douteux) qu'on peut franchir en A0. Poursuivre en ascendance légèrement
vers la droite pour buter contre un bombé lisse, qui constitue le crux de la longueur et
de la voie : 6c en libre, ou A0+ en faisant une pédale.

Poursuivre l'escalade légèrement vers la gauche ; on rejoint bientôt des gradins faciles
mais en rocher pourri qu'on gravit tout droit en direction d'une petite grotte. Une fois
à la hauteur de la grotte, traverser vers la droite pour faire relais sur un piton et une
plaquette.

Attention au tirage dans la longueur.

**Ne pas utiliser le relais chaîné situé dans la grotte**, il est trop à l'écart pour L2.

### L2 -- 5c

Départ droit au-dessus du relais. Le rocher, bon au début de la longueur, devient très vite
friable alors qu'il faut passer un premier bombé (A0 possible avec une dégaine panic).
Poursuivre jusqu'à une zone où un piton a été plié et une plaquette dévissée : la voie part
alors franchement sur la gauche, en suivant une rampe ascendante parfois délicate (A0
possible). Une fois sorti, on ne voit plus de point : ne pas prendre les gradins à gauche
mais grimper légèrement sur la droite dans du rocher correct (un piton gris dans le haut,
difficile à voir).

R2 chaîné au niveau d'une vire, sous une fissure-cheminée. Attention, le rocher au niveau
de la vire est très friable et ne demande qu'à partir !

### L3 -- 5b

Remonter la fissure-cheminée, escalade à l'ancienne mais longueur assez courte.

R3 quatre étoiles au sommet de la fissure.

### L4 -- 4b

Partir sur la gauche du relais (1 piton visible sur la tranche), et remonter tout droit
un couloir envahi de buis. Sur le haut de la longueur, prendre une vire à gauche pour sortir
du couloir.

R4 chaîné sur la vire.

### L5 -- 5a

Tout droit vers le sommet, peu de pieds mais de bonnes mains. Attention au rocher, friable
sur le haut.

## Descente

Suivre l'arête vers le sud jusqu'à un petit col. Basculer versant ouest pour rejoindre le sentier
de montée après avoir desservi les voies du pilier sud.
