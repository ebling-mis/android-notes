---
created: 2021-02-19T19:24:00+01:00
modified: 2021-07-30T21:32:05+02:00
---

# Saou - Aiguille de la Tour - Arête Sud

## Cotations

Cotation globale : D+  
Cotation escalade : 5c (5b obligatoire)  
Engagement (échelle C2C) : II  
Équipement en place (échelle C2C) : P2

## Ambiance

Une grande voie longue, peu équipée sauf L1/2/3, qui appartient clairement au
domaine du Terrain d'Aventure. Tous les styles de grimpe sont présents (cheminée,
fissure, Dülfer, dalle...).

Le rocher surprend dans L2/3 mais l'équipement est bon et les longueurs sont
purgées. L4/5 nécessitent des coinceurs et friends et un bon mental (longueurs
de 45m chacune). La sortie jusqu'au sommet, facultative, se fait dans un rocher
délité et pourri, avec une végétation abondante.

Crux dans L5, avec un passage en 5c+ (d'après moi) bien vertical et avec peu de
pieds, un peu avant le relais. L6 côtée 5c, mais si on a encore du jus, ça passe
bien : plusieurs pitons, beau rocher fin mais peu patiné.

Ouverture : 1967, par Fresafond, Grasset et Parat. Rééquipée en août 1987 par
Jean-Marc Belle, Denis Benoit et Frédéric Prot.

## Approche

Du parking, prendre le chemin qui mène au pied de l'aiguille de la Tour. Passer
devant le secteur Vitamine, continuer vers l'ouest, on tombe devant un cable et
un bloc où est écrit _Arête S_ en gros caractères rouges.

## Voie

### L1 -- 3b

Suivre le cable jusqu’à arriver au pied d'une cheminée s'élevant sur la gauche
avant que la corde fixe ne redescende. Facile, ne nécessite pas d'encordement.

Faire un relais sur les goujons du cable, à gauche ou à droite de cette cheminée.

### L2 -- 5a, 25m, équipée (nombreux points)

Remonter la cheminée en renfougne. Un bon 5a qui surprend un peu, chute pas
conseillée. Relais chaîné à la sortie, au niveau d'un arbre.

### L3 -- 5b, équipée (nombreux points)

Un pas au départ, bien traverser à gauche environ 2m, puis remonter (facile
et court). Déboucher au sommet de l'écaille, désescalader derrière pour trouver
un relais chaîné.

### L4 -- 5b, 4 pitons et 1 goujon + friends

L'engagement commence ; le départ n'est pas évident. Repérer une ligne de fissure
qui remonte entre les voies _Saharaoui oui_ et _Vent de Sable_ (non nommées sur
la falaise, mais repérer tant bien que mal les lignes de spits). Un piton auquel
pend un anneau de cordelette marque le départ.

Rejoindre la ligne de fissure à droite pour trouver, 5m plus haut, un piton.
Suivre les fissures qui tirent légèrement à droite (second piton 15m plus haut).
On croise un point de _Vent de Sable_ avant de passer à travers des arbustes.
Plusieurs relais sur pitons sont possibles (au moins 2), continuer droit vers
le haut pour venir trouver une terrasse au pied d'un système de fissures.

R4 pas évident :
 - soit sur plaquette et arbre bien à droite,
 - soit sur le relais de _Vent de Sable_, bien à gauche (2 spits reliés par un
   anneau de corde verte),
 - idéalement, confectionner un relais avec des coinceurs et des friends entre les
   deux, pile dans l'axe de l'arête.

### L5 -- 5c, 45m, pitons + friends

Rejoindre l'axe de l'arête et un système de fissures bien verticales dans du rocher
jaune. Coinceurs utiles. Un piton plus haut, puis un mur de quelques mètres en 5c
assez teigneux...

Attention, ça passe bien par les fissures à gauche et non pas par l'arbre mort qu'il
faut laisser à droite.

R5 à la fin des fissures :
 - soit 1 goujon + arbre,
 - soit sur 2 goujons chaînés un peu plus à droite, derrière l'arbre (tirage).

### L6 -- 5c, 25m, 5 pitons

Remonter tout droit une magnifique dalle à réglettes, un peu courte. Déboucher sur
un jardin suspendu.

R6 sur un arbre.

La suite n'est pas conseillée : le rocher pourri et la végétation abondante qu'il
faut sans cesse contourner voire traverser rend l'itinéraire désagréable et plus
exposé (mix de terrain à chamois terreux et de rocher délité).

### L7 -- 2

Traverser le jardin suspendu, puis emprunter une rampe ascendante vers la droite,
qui se termine par une traversée. Aucune protection en place, rocher pourri,
attention à tout ce que l'on touche !

R7 sur 3 plaquettes reliées par des anneaux de cordelettes.

### L8 -- 4c, 2 pitons

Contourner le fil du pilier par la droite (c'est le plus facile), puis remonter
légèrement vers la gauche. 2 pitons. Bon rocher dans la première partie de la
longueur, le reste pourri.

R8 sur un arbre avec sangles.

### L9 -- 4c

Remonter au mieux vers le sommet. Progression en corde tendue. Une fois au sommet,
revenir quelques mètres en arrière et suivre une sente encombrée de végétation
passant sous l'arête versant est. On rejoint une brèche où se trouve le 1er rappel
du versant W, sur un arbre.

## Descente

Par le versant ouest : depuis la brèche, enchaîner 3 rappels (45m + 25m + 25m)
sur des arbres. Attention à la végétation, qui peut coincer la corde.

Dans _Vitamine C_ : à la fin de L6, redescendre en rappel jusqu'au relais chaîné R5,
qui est aussi le dernier relai (R4) de _Vitamine C_ (2 goujons + chaîne). Puis
descendre dans _Vitamine C_ en 3 ou 4 rappels selon la longueur de la corde.

**Attention :** en dépit des indications données dans certains topos, il n'est
pas possible de sortir de la voie en rappel entre R6 et le sommet : le rocher est
pourri (grimpeurs en-dessous), la végétation est abondante et les rappels ne sont
pas verticaux. Une fois passé R6, la seule sortie possible est par le haut.
