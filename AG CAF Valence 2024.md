---
created: 2024-11-18T21:00:59+01:00
modified: 2024-11-18T21:03:08+01:00
---

# AG CAF Valence 2024

## Escalade

SAE :

 * 3 nouveaux initiateurs SAE : Gabriel, Nordine et Diane.
 * l'Entente (CAF/Densité/VUC + ville de Valence) - merci à Patrick L.
 * renouvellements de voies (sept/déc/avril) par Jean
 * énorme engagement de tous les encadrants autour des Ateliers du Polygone (14) : révisions manips, perf. technique de grimpe, échanges de pratiques en escalade, en GV, en alpi et même en ski alpinisme !

Mais n’oublions pas : but de l'escalade = mettre le nez dehors, lézarder au soleil, découvrir des nouveaux sites, nouveau caillou.

Pas beaucoup de sorties cet année, deux blessés : Théo (moniteur GV !) et moi. JNo diplômé SNE.

QQ belles journées :

 * dans les sites locaux (Omblèze, Saou, Valcroissant…)
 * lors des camps et week-end multi-activités.

En particulier
 * parcours sous la neige de À Thor et en Travers à Archiane
 * nombreuses via ferrata, aussi bien à la Motte-du-Caire qu’en Ardèche ou dans le Vercors : merci à Louis-Michel, André et Patrick pour leur encadrement.

Sujet Omblèze : pas beaucoup d'avancées.  
Rôle de surveillance des voies, d’entretien des accès et d’interface entre les grimpeurs, la commune et les propriétaires.  
QQ sorties à prévoir + besoin encadrants & adhérents

Formation : besoin d'encadrants  
Suivre les cursus d'initiateurs = formidable opportunité d’apprendre et d’être curieux  
Frais pris en charge par le club !

Bravo et merci à tous les encadrants qui font vivre l’activité !


## Effectifs

Nous avons clôturé l’exercice 2023/2024 avec un total de 444 adhérents (+3% par rapport à 2022/2023).  
Nous avons accueilli cette année 134 primo-adhérents, contre 109 l’an dernier !  
Pour l’exercice 2023/2024, on dénombre parmi nos adhérents 186 femmes (42%) et 46 mineurs.

Ce soir, nous comptons déjà au club 424 adhérents dont 130 nouveaux, ce qui laisse présager une belle année pour nos effectifs !

## Cotisations - Assurances

 * Assu. personne  (77% adhérents) : 22 -> 24€ (+9%)
 * Assu. renforcée (9 adhérents)   : 43 -> 48€ (+11,6%)
 * Ext. monde (4 adhérents)        : 118 -> 160€ (+36%)

Assureur Willis Towers Watson (WTW) Montagne  
Ex. Gras Savoie

Seul à répondre à l'appel d'offre de la FFCAM

Le coût des frais de secours en Europe et hors Europe a fortement augmenté depuis le Covid.

La fréquence des sinistres, notamment dans certains pays comme la Suisse, l’Italie et le Népal augmente.

La situation géopolitique a également un impact sur les coûts des rapatriements (territoires interdits de survol...)

## Cotisations - Évolution

 * +0,10 € pour les enfants (catégories E1, E2, J1 et J2),
 * +0,20 € pour les adultes (catégories A1, C1, P1 et T1).